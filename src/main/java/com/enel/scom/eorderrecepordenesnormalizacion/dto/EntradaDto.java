package com.enel.scom.eorderrecepordenesnormalizacion.dto;

import lombok.Data;

@Data
public class EntradaDto {
    private String nombreProceso;
    private String empresa;
    // private String conexion;
    private String codigoUsuario;
    private String nroODT;


}
