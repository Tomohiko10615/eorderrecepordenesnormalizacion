package com.enel.scom.eorderrecepordenesnormalizacion.dto;

import lombok.Data;

//INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA

@Data
public class MedidoresXML {
    private String accionMedidor;
    private String medidor;
    private String factorMedidor;   
    private String modeloMedidor;
    private String marcaMedidor;   
    
}


//FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA