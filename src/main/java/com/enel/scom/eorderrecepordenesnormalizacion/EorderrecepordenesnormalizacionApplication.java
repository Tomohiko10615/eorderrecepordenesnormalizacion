package com.enel.scom.eorderrecepordenesnormalizacion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.scom.eorderrecepordenesnormalizacion.dto.EntradaDto;
import com.enel.scom.eorderrecepordenesnormalizacion.service.ProbarService;
import com.enel.scom.eorderrecepordenesnormalizacion.service.RecepcionOrdenNormalizacionService;
import com.enel.scom.eorderrecepordenesnormalizacion.service.RecepcionResultadoService;

@SpringBootApplication
public class EorderrecepordenesnormalizacionApplication implements CommandLineRunner {
	private static final Logger logger = LogManager.getLogger(EorderrecepordenesnormalizacionApplication.class);
	@Autowired
	RecepcionOrdenNormalizacionService recepcionOrdenNormalizacionService;
	// @Autowired
	// RecepcionResultadoService recepcionResultadoService;

	@Autowired
	ProbarService probarService;

	public static void main(String[] args) {
		SpringApplication.run(EorderrecepordenesnormalizacionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// /binarios/ejecutables/eorder/perdidas/EOrderRecepOrdenesNormalizacion.exe
		// EDEL dbeorder@radl2e pe02850937 $orden >> ${archivo}

		// borrar/comentar/
/*
		 args = new String[3];
		 args[0] = "EDEL";
		 args[1] = "pe02850937";
		 args[2] = "20230313203817";
	 */
		logger.info("Inicio.");
		
		/*
		boolean result = recepcionOrdenNormalizacionService.validarFactor("3156", "8CA03", "SNX", "200");
		if (!result) {
			String vObservacion = "Error: El valor del factor de facturación del medidor no es válido.";
			String vCodOperacion = "ASY045";
			logger.info("Validando : {}", vObservacion);
			;
		} else {
			logger.info("Result : {}", result);
		}
		
		System.exit(0);
		*/

		if (args.length != 3) {
			logger.error("Error en parametros");
			logger.error("Uso:");
			// logger.error("java -jar EOrderRecepOrdenesNormalizacion.jar <Empresa>
			// <Conexion> <Cod_Usuario> <nroODT>");
			logger.error("java -jar EOrderRecepOrdenesNormalizacion.jar <Empresa> <Cod_Usuario> <nroODT>");
			System.exit(1);
		}

		EntradaDto entradaDto = new EntradaDto();
		entradaDto.setNombreProceso("EOrderRecepOrdenesNormalizacion.jar");
		entradaDto.setEmpresa(args[0]);
		// entradaDto.setConexion(args[1]);
		entradaDto.setCodigoUsuario(args[1]);
		entradaDto.setNroODT(args[2]);

		logger.info("Inicio proceso.");
		logger.info("Parametros ingresados.");

		logger.info("NombreProceso: {}.", entradaDto.getNombreProceso());
		logger.info("Empresa: {}.", entradaDto.getEmpresa());
		// logger.info("Conexion: {}.", entradaDto.getConexion());
		logger.info("CodigoUsuario: {}.", entradaDto.getCodigoUsuario());
		logger.info("NroODT: {}.", entradaDto.getNroODT());

		if (!recepcionOrdenNormalizacionService.recepcionarOrdenNormalizacion(entradaDto)) {
			logger.error("Finaliza con error en el proceso.");
			logger.info("Ko.");
			System.exit(1);
		}

		// probarService.accion1();

		// if (!recepcionResultadoService.recepcionar(entradaDto)) {
		// logger.error("Finaliza con error en el proceso.");
		// logger.info("Ko.");
		// System.exit(1);
		// }

		// try {
		// recepcionOrdenNormalizacionService.recepcionarOrdenNormalizacion(entradaDto);
		// } catch (Exception e) {
		// logger.error(e.getMessage());
		// logger.error("Finaliza con error en el proceso.");
		// logger.info("Ko.");
		// System.exit(1);
		// }
		logger.error("Finaliza de forma existosa.");
		logger.info("Ok.");
		System.exit(0);
	}

}
