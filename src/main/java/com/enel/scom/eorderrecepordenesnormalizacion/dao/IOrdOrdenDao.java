package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAbBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenBean;

public interface IOrdOrdenDao {
    // OrdOrdenBean findByNroOrden(String nroOrden);
    // OrdOrdenBean findByNroOrdenAndIdEmpresa(String nroOrden, Long idEmpresa);
    int updateFechaFinalizacionById(Long id, String fechaFinalizacion); // Linea 3741
    OrdOrdenBean findByIdOrdenAndDiscriminador(Long idOrden, String discriminador); // Linea 4101
    OrdOrdenAaBean findAaByNroOrden(String nroOrden); // Linea 1984
    OrdOrdenAbBean findAbByNroOrden(String nroOrden, Long idEmpresa); // Linea 2011,  2075

}
