package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorOrdTransferBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorOrdTransferDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.EorOrdTransferMapper;

@Repository
public class EorOrdTransferDaoImpl implements IEorOrdTransferDao {
	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(EorOrdTransferDaoImpl.class);

	@Override
	public List<EorOrdTransferBean> findByCodEstadoOrdenAndCodTipoOrdenEorder(Long cod_estado_orden,
			String cod_tipo_orden_eorder) {
		logger.info("findByCodEstadoOrdenAndCodTipoOrdenEorder({}, {}).", cod_estado_orden,
				cod_tipo_orden_eorder);
		String sqlString = EorOrdTransferMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFER_BY_ESTADOORDEN_TIPOEORDER;
		List<EorOrdTransferBean> objects = new ArrayList<>();
		try {
			objects = jdbcTemplate.query(sqlString, new EorOrdTransferMapper(), cod_estado_orden,
					cod_tipo_orden_eorder);
			logger.info("Cantidad de ordenes encontradas: {}.", objects.size());
			logger.info("Ordenes encontradas: {}", objects);

		} catch (EmptyResultDataAccessException e) {
			logger.info("Error: Resultado vacio.");
		} catch (DataAccessException de) {
			logger.info("Error al obtener ordenes.");
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return objects;
	}

	@Override
	public int update(String vCodOperacion, String Tdc_Creada_Campo, Long NumeroOrden, String vCodErrASY000,
			Long vCodEstRecep, Long vCodEstRecepError, String vObservacion, String cNroCuenta, Long vCodEstPendRecep,
			String vCodTipoTDC, Long vIdOrdTransfer) {

		String sqlString = ""
				+ "UPDATE\n"
				+ "    eor_ord_transfer\n"
				+ "SET\n"
				+ "      fec_operacion = current_timestamp\n"
				+ "    , cod_operacion = ?\n"
				+ "    , NRO_ORDEN_LEGACY = (CASE WHEN ? ='1' THEN ? ELSE NRO_ORDEN_LEGACY END)\n"
				+ "    , cod_estado_orden = (CASE WHEN ? = ? THEN ? ELSE ? END)\n"
				+ "    , OBSERVACIONES= ?\n"
				+ "    , COD_TIPO_ORDEN_LEGACY= (CASE WHEN ? ='1' THEN 'NORM' ELSE COD_TIPO_ORDEN_LEGACY END)\n"
				+ "    , NRO_CUENTA = ?\n"
				+ "WHERE\n"
				+ "    cod_estado_orden = ?\n"
				+ "    AND cod_tipo_orden_eorder = ?\n"
				+ "    AND id_ord_transfer =  ?\n"
				+ ";";

		int rows = 0;
		try {
			rows = jdbcTemplate.update(sqlString, vCodOperacion, Tdc_Creada_Campo, NumeroOrden, vCodOperacion,
					vCodErrASY000, vCodEstRecep, vCodEstRecepError, vObservacion, Tdc_Creada_Campo, cNroCuenta,
					vCodEstPendRecep, vCodTipoTDC, vIdOrdTransfer);
			logger.info("Método update, trasnfer, filas afectadas: {}", rows);

		} catch (Exception ex) {
			logger.info("Error al actualizar el estado Recepcionado con errores");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
	}

	@Override
	public EorOrdTransferBean findByNroOrdenLegacy(String nroOrdenLegacy) {
		logger.info("findByNroOrdenLegacy({})", nroOrdenLegacy);
		EorOrdTransferBean objecto = null;
		String sqlScript = EorOrdTransferMapper.SQLPOSTGRESQL_SELECT;

		try {
			objecto = jdbcTemplate.queryForObject(sqlScript, new EorOrdTransferMapper(), nroOrdenLegacy);
		} catch (EmptyResultDataAccessException ee) {
			logger.error("EmptyResultDataAccessException: {}.", ee.getMessage());

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {}.", de.getMessage());
			throw de;
		}
		return objecto;
	}

	@Override
	public int update(EorOrdTransferBean eorOrdTransferBean) {
		logger.info("update({})", eorOrdTransferBean);
		String sqlString = ""
				+ "UPDATE\n"
				+ "      EOR_ORD_TRANSFER\n"
				+ "SET\n"
				+ "      FEC_OPERACION = current_timestamp\n"
				+ "    , COD_OPERACION = ?\n"
				+ "    , NRO_ORDEN_LEGACY = ?\n"
				+ "    , COD_ESTADO_ORDEN = ?\n"
				+ "    , OBSERVACIONES = ?\n"
				+ "    , COD_TIPO_ORDEN_LEGACY = ?\n"
				+ "    , NRO_CUENTA = ?\n"
				+ "WHERE 1 = 1\n"
				+ "    AND COD_ESTADO_ORDEN = ?\n"
				+ "    AND COD_TIPO_ORDEN_EORDER = ?\n"
				+ "    AND ID_ORD_TRANSFER = ?\n";

		int rows = 0;
		try {
			rows = jdbcTemplate.update(sqlString,
					new Object[] { eorOrdTransferBean.getCodOperacion(), eorOrdTransferBean.getNroOrdenLegacy(),
							eorOrdTransferBean.getCodEstadoOrden(), eorOrdTransferBean.getObservaciones(),
							eorOrdTransferBean.getCodTipoOrdenLegacy(), eorOrdTransferBean.getNroCuenta(),
							eorOrdTransferBean.getCodEstadoOrden(),
							eorOrdTransferBean.getCodTipoOrdenEorder(), eorOrdTransferBean.getIdOrdTransfer() });
			logger.info("Método update, trasnfer, filas afectadas: {}", rows);

		} catch (Exception ex) {
			logger.info("Error al actualizar el estado Recepcionado con errores");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
	}

	@Override
	public int updateTranferProcesoRecepcionaOrdenNormalizacion(EorOrdTransferBean eorOrdTransferBean) {

		logger.info("updateTranferProcesoRecepcionaOrdenNormalizacion({})", eorOrdTransferBean);
		String sqlString = ""
				+ "UPDATE\n"
				+ "      EOR_ORD_TRANSFER\n"
				+ "SET\n"
				+ "      FEC_OPERACION = current_timestamp\n"
				+ "    , COD_OPERACION = ?\n"
				+ "    , NRO_ORDEN_LEGACY = CASE WHEN ? = '1' THEN ? ELSE NRO_ORDEN_LEGACY END\n"
				+ "    , COD_ESTADO_ORDEN = ?\n"
				+ "    , OBSERVACIONES = ?\n"
				+ "    , COD_TIPO_ORDEN_LEGACY = CASE WHEN ? = '1' THEN ? ELSE COD_TIPO_ORDEN_LEGACY END\n"
				// + "    , NRO_CUENTA = ?\n"
				+ "    , NRO_CUENTA = coalesce(?, NRO_CUENTA)\n" 
				+ "WHERE 1 = 1\n"
				+ "    AND COD_ESTADO_ORDEN = ?\n"
				+ "    AND COD_TIPO_ORDEN_EORDER = ?\n"
				+ "    AND ID_ORD_TRANSFER = ?\n";

		int rows = 0;
		try {
			rows = jdbcTemplate.update(sqlString,
					eorOrdTransferBean.getCodOperacion(),
					eorOrdTransferBean.getNroOrdenEorder(),
					eorOrdTransferBean.getNroOrdenLegacy(),
					eorOrdTransferBean.getCodEstadoOrden(),
					eorOrdTransferBean.getObservaciones(),
					eorOrdTransferBean.getNroOrdenEorder(),
					eorOrdTransferBean.getCodTipoOrdenLegacy(),
					eorOrdTransferBean.getNroCuenta(),
					eorOrdTransferBean.getCodEstadoOrdenAnt(),
					eorOrdTransferBean.getCodTipoOrdenEorder(),
					eorOrdTransferBean.getIdOrdTransfer());
			logger.info("Método update, trasnfer, filas afectadas: {}", rows);

		} catch (Exception ex) {
			logger.info("Error al actualizar el estado Recepcionado con errores");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
	}

}
