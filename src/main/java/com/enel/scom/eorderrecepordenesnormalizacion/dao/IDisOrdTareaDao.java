package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisOrdTareaBean;

public interface IDisOrdTareaDao {
    Long getIdOrdTareaByIdEmpresaAndIdOrdenAndIdIrregularidadAndIdTarea(Long idOrden, Long idAnormalidad, Long idTarea); // Linea 1593
    int insert(DisOrdTareaBean disOrdTareaBean); // Linea 1613
    int updateTareaEstadoByIdOrdTarea(Long idOrdTarea); // Linea 1654
    int updateTareaEstadoByIdOrden(Long idOrden); // Linea 3887
    Long nextVal(); // Linea 1608


}
