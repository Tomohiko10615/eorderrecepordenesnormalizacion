package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdMediCambioBean;

public interface IOrdMediCambioDao {
    int insert(OrdMediCambioBean ordMediCambioBean); // Linea 3073, 3596
}
