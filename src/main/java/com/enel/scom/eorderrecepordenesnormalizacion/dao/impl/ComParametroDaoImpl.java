package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComParametroBean;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.ComParametroMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IComParametroDao;

@Repository
public class ComParametroDaoImpl implements IComParametroDao {
	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(ComParametroDaoImpl.class);

	@Override
	public ComParametroBean findBySistemaAndEntidadAndCodigo(String sistema, String entidad, String codigo) {
		ComParametroBean oParametro = null;
		String sqlScript = ComParametroMapper.SQLPOSTGRESQL_SELECT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO;
		try {
			oParametro = jdbcTemplate.queryForObject(sqlScript, new ComParametroMapper(), sistema, entidad, codigo);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No se encuentra configurado el Parametro Sistema: {}, Entidad: {}, Codigo: {}", sistema,
					entidad, codigo);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return oParametro;
	}

	@Override
	public ComParametroBean findBySistemaAndEntidadAndCodigoAndActivo(String sistema, String entidad, String codigo,
			String activo) {

		ComParametroBean oParametro = null;
		String sqlScript = ComParametroMapper.SQLPOSTGRESQL_SELECT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO_ACTIVO;
		try {
			oParametro = jdbcTemplate.queryForObject(sqlScript, new ComParametroMapper(), sistema, entidad, codigo,
					activo);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No se encuentra configurado el Parametro Sistema: {}, Entidad: {}, Codigo: {}, Activo: {}.",
					sistema,
					entidad, codigo, activo);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return oParametro;
	}

	@Override
	public Long getCountBySistemaAndEntidadAndCodigoAndValorAlf(String sistema, String entidad, String codigo,
			String valorAlf) {
		Long cantidad = 0L;
		String sqlScript = ComParametroMapper.SQLPOSTGRESQL_COUNT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO_VALORALF;
		try {
			cantidad = jdbcTemplate.queryForObject(sqlScript, Long.class, sistema, entidad, codigo, valorAlf);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No se encuentra configurado el Parametro Sistema: {}, Entidad: {}, Codigo: {}, Activo: {}.",
					sistema,
					entidad, codigo, valorAlf);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return cantidad;
	}
}
