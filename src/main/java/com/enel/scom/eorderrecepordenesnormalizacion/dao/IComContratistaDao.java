package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IComContratistaDao {
    // ComContratistaBean findByCodContratista(String cCod_Contratista);
    Long getCountByCodContratista(String codContratista); // Linea 4363
}
