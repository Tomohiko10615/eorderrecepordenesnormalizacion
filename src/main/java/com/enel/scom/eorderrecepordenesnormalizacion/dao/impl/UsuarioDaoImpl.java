package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.UsuarioBean;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.UsuarioMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IUsuarioDao;

@Repository
public class UsuarioDaoImpl implements IUsuarioDao {
	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(UsuarioDaoImpl.class);

	@Override
	public UsuarioBean findByUsername(String username) {
		UsuarioBean objecto = null;
		String sqlScript = UsuarioMapper.SQLPOSTGRESQL_SELECT_FOR_USUARIO_BY_USERNAME;
		try {
			objecto = jdbcTemplate.queryForObject(sqlScript, new UsuarioMapper(), username);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Error: No existe ID_USUARIO para el parametro <Cod_Usuario> ingresado.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
		}

		return objecto;
	}

}
