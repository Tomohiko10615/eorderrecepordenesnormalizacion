package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorOrdTransferBean;


public interface IEorOrdTransferDao {
	List<EorOrdTransferBean> findByCodEstadoOrdenAndCodTipoOrdenEorder(Long codEstadoOrden, String comParametrosCodigo); // Linea 792
	int update(String vCodOperacion, String Tdc_Creada_Campo, Long NumeroOrden, String vCodErrASY000, Long vCodEstRecep, Long vCodEstRecepError, String vObservacion, String cNroCuenta, Long vCodEstPendRecep, String vCodTipoTDC, Long vIdOrdTransfer); // Linea 1735
	int update(EorOrdTransferBean eorOrdTransferBean); // Linea 1735
	int updateTranferProcesoRecepcionaOrdenNormalizacion(EorOrdTransferBean eorOrdTransferBean); // Linea 1735
	EorOrdTransferBean findByNroOrdenLegacy(String nroOrdenLegacy); // Linea 1956
	// int getCodEstadoOrdenByNroOrdenLegacy(String nroOrdenLegacy);
}
