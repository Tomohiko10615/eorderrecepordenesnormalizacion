package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComParametroBean;

public interface IComParametroDao {
	// No requiere ubicar las lineas en el PROC. Estos metodos ya estan implementados
	ComParametroBean findBySistemaAndEntidadAndCodigo(String sistema, String entidad, String codigo); 
	ComParametroBean findBySistemaAndEntidadAndCodigoAndActivo(String sistema, String entidad, String codigo, String activo);
	Long getCountBySistemaAndEntidadAndCodigoAndValorAlf(String sistema, String entidad, String codigo, String valorAlf); // Linea 1911, 1933
}
