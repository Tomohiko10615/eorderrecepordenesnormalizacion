package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IMedModeloDao {
    Long getIdModelByCodModelAndCodMarca(String codModelo, String codMarca); // Linea 3261
}
