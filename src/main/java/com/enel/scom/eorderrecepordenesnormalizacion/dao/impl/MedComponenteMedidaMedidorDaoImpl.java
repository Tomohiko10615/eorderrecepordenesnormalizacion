package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedComponenteMedidaMedidorDao;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Repository
public class MedComponenteMedidaMedidorDaoImpl implements IMedComponenteMedidaMedidorDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplateBdScomUsrScom;
	
	@Override
	public void deleteFromMedidaMedidorByIdComponente(Long dIdComponente_i, List<Integer> idMedidas) {
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplateBdScomUsrScom);
		try {
			log.info("Eliminando medidas...");
			log.info("Medidas que no se eliminarán {}", idMedidas);
			log.info("idComponente: {}", dIdComponente_i);
	        String deleteQuery = "DELETE FROM med_medida_medidor WHERE id_componente = :idComponente AND id_medida NOT IN (:idMedidas)";
	        
	        Map<String, Object> paramMap = new HashMap<>();
	        paramMap.put("idComponente", dIdComponente_i);
	        paramMap.put("idMedidas", idMedidas);

	        namedParameterJdbcTemplate.update(deleteQuery, paramMap);
	        log.info("Se eliminaron las medidas...");
	    } catch (Exception e) {
	        log.error("Error al eliminar medidas de med_medida_medidor: {}", e.getMessage());
	    }
		
	}
}
