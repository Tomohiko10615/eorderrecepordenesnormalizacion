package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.WkfWorkflowBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IWkfWorkflowDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.WkfWorkflowMapper;

@Repository
public class WkfWorkflowDaoImpl implements IWkfWorkflowDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(WkfWorkflowDaoImpl.class);

    @Override
    public int updateIdStateByIdWorkflow(Long idWorkflow, String idState) {
        String sqlString = ""
        + "UPDATE \n"
        + "    WKF_WORKFLOW\n"
        + "SET \n"
        + "     ID_OLD_STATE = ID_STATE\n"
        + "    , ID_STATE = ?\n"
        + "WHERE 1 = 1\n"
        + "    AND ID = ?\n"
        + ";"
        ;

        int rows = 0;

        try {
            rows = jdbcTemplate.update(sqlString, idState, idWorkflow);
            logger.info("Metodo updateIdStateByIdWorkflow. Filas afectadas: {}.", rows);

        } catch (Exception ex) {
            logger.error("Error al actualizar.");
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return rows;
    }

    @Override
    public int updateIdStateByIdWorkflowAndIdState(Long idWorkflow, String idState, String newIdState) {
        String sqlString = ""
        + "UPDATE \n"
        + "    WKF_WORKFLOW\n"
        + "SET \n"
        + "      ID_OLD_STATE = ID_STATE\n"
        + "    , ID_STATE = ?\n"
        + "WHERE 1 = 1\n"
        + "    AND ID = ?\n"
        + "    AND ID_STATE = ?\n"
        + ";";

        int rows = 0;

        try {
            rows = jdbcTemplate.update(sqlString, newIdState, idWorkflow, idState);
            logger.info("Metodo updateIdStateByIdWorkflowAndIdState. Filas afectadas: {}.", rows);

        } catch (Exception ex) {
            logger.error("Error al actualizar.");
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return rows;
    }

    @Override
    public WkfWorkflowBean findById(Long idWorkflow) {
        String sqlString = ""
                + "SELECT \n"
                + "      ID\n"
                + "    , ID_DESCRIPTOR\n"
                + "    , VERSION\n"
                + "    , ID_STATE\n"
                + "    , HAS_PARENT\n"
                + "    , ID_OLD_STATE\n"
                + "    , PARENT_TYPE\n"
                + "    , FECHA_ESTADO\n"
                + "FROM\n"
                + "    WKF_WORKFLOW\n"
                + "WHERE 1 = 1\n"
                + "    AND ID = ?";
        WkfWorkflowBean object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, new WkfWorkflowMapper(), idWorkflow);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion para el dato {}.", idWorkflow);
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage(), ee);
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage(), de);
        }
        return object;
    }

}
