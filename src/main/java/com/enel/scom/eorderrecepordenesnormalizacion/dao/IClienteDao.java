package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ClienteBean;

public interface IClienteDao {
    ClienteBean findByNroServicio(Long nroServicio); // Linea 4154
}
