package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.SrvActComercialAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.SrvActComercialBean;

public interface ISrvActComercialDao {
    SrvActComercialBean findByCodActComercialAndIdEmpresa(String codActComercial, Long idEmpresa); // Linea 2167
    // SrvActComercialAaBean findAaByCodActComercialAndIdEmpresa(String codActComercial, Long idEmpresa);
    
}
