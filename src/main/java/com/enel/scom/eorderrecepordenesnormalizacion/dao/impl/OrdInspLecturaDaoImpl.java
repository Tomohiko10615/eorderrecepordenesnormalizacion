package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdInspLecturaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdInspLecturaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdInspLecturaMapper;
@Repository
public class OrdInspLecturaDaoImpl implements IOrdInspLecturaDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(OrdInspLecturaDaoImpl.class);
	
    @Override
    public int insert(OrdInspLecturaBean ordInspLecturaBean) {
    	String sqlScript = OrdInspLecturaMapper.SQLPOSTGRESQL_INSERT_ORD_INSP_LECTURA;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript,
					ordInspLecturaBean.getIdEmpresa(),
					ordInspLecturaBean.getIdMediInsp(),
					ordInspLecturaBean.getIdLectura()
					);
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("No se pudo insertar los datos en la tabla ORD_INSP_LECTURA.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public Long nextVal() {
    	String sqlScript = OrdInspLecturaMapper.SQLPOSTGRESQL_SELECT_FOR_SQINSPECCIONMEDIDORLECTURA;
		Long idOrdInspLectura = null;
		try {
			idOrdInspLectura = jdbcTemplate.queryForObject(sqlScript, Long.class);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No pudo obtenerse el idOrdInspLectura.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
		}catch (Exception ex)
		{
			logger.info("No pudo obtenerse el idOrdInspLectura.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return idOrdInspLectura;
    }
}
