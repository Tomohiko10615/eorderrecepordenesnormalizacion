package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisResultadoInspAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisResultadoInspDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DisResultadoInspMapper;
@Repository
public class DisResultadoInspDaoImpl implements IDisResultadoInspDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(DisResultadoInspDaoImpl.class);
	
    @Override
    public DisResultadoInspAaBean findByCodResultadoAndIdEmpresa(String codResultado) {
    	DisResultadoInspAaBean oResultado = null;
		String sqlScript = DisResultadoInspMapper.SQLPOSTGRESQL_SELECT_FOR_DIS_RESULTADO_INSP;
		try {
			oResultado = jdbcTemplate.queryForObject(sqlScript, new DisResultadoInspMapper(), codResultado);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Resultado no existe-Codigo Causal . ({})", codResultado);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return oResultado;
    }
   
}
