package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IComParentescoDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.ComParentescoMapper;

@Repository
public class ComParentescoDaoImpl implements IComParentescoDao {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(ComParentescoDaoImpl.class);
	
	@Override
	public Long findByCodParentescoAndIdEmpresa(String codParentesco, Long idEmpresa) {
		Long idParentesco = null;
		String sqlScript = ComParentescoMapper.SQLPOSTGRESQL_SELECT_FOR_PARENTESCO;
		try {
			idParentesco = jdbcTemplate.queryForObject(sqlScript, Long.class, codParentesco, idEmpresa);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Parentesco no existe. ({})", codParentesco);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idParentesco;
	}

}
