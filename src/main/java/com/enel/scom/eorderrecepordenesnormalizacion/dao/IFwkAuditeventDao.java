package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.FwkAuditeventBean;

public interface IFwkAuditeventDao {
    // int insert(Long NumeroOrden, Long id_fk, String specific_auditevent, long id_user);
    int insert(FwkAuditeventBean fwkAuditeventBean); // Linea 3853
}
