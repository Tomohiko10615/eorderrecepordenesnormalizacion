package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdObservacionBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdObservacionDao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class OrdObservacionDaoImpl implements IOrdObservacionDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(OrdObservacionDaoImpl.class);

    @Override
    public Long nextVal() {
        String sqlString = "SELECT NEXTVAL('SQOBSERVACIONORDEN')";
        Long object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, Long.class);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }

    @Override
    public int insert(OrdObservacionBean ordObservacionBean) {
        logger.info("insert({})", ordObservacionBean);
        String sqlString = ""
                + "INSERT INTO ORD_OBSERVACION\n"
                + "(\n"
                + "      ID_OBSERVACION\n"
                + "    , ID_ORDEN\n"
                + "    , TEXTO\n"
                + "    , FECHA_OBSERVACION\n"
                + "    , ID_USUARIO\n"
                + "    , STATE_NAME\n"
                + "    , DISCRIMINATOR\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "      ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + ")"
                ;
        int rows = 0;
        try {
            rows = jdbcTemplate.update(sqlString, ordObservacionBean.getIdObservacion(),
                    ordObservacionBean.getIdOrden(), ordObservacionBean.getTexto(),
                    ordObservacionBean.getFechaObservacion(), ordObservacionBean.getIdUsuario(),
                    ordObservacionBean.getStateName(), ordObservacionBean.getDiscriminator());
            logger.info("Insert. Filas afectadas: {}", rows);
        } catch (DataAccessException de) {
            logger.info("Error al insertar.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return rows;
    }
}
