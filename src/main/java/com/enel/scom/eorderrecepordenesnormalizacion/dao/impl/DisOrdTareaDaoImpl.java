package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisOrdTareaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisOrdTareaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DisOrdTareaMapper;
@Repository
public class DisOrdTareaDaoImpl implements IDisOrdTareaDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(DisOrdTareaDaoImpl.class);
	
    @Override
    public Long getIdOrdTareaByIdEmpresaAndIdOrdenAndIdIrregularidadAndIdTarea(Long idOrden,
            Long idAnormalidad, Long idTarea) {
    	Long idOrdTarea = null;
		String sqlScript = DisOrdTareaMapper.SQLPOSTGRESQL_SELECT_FOR_ID_ORD_TAREA;
		try {
			idOrdTarea = jdbcTemplate.queryForObject(sqlScript, Long.class, idOrden, idAnormalidad, idTarea);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No pudo recuperar ID de la tarea");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idOrdTarea;
    }

    @Override
    public int insert(DisOrdTareaBean disOrdTareaBean) {
    	String sqlScript = DisOrdTareaMapper.SQLPOSTGRESQL_INSERT_INTO_DIS_ORD_TAREA;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript,
					disOrdTareaBean.getId(),
					disOrdTareaBean.getIdOrden(),
					disOrdTareaBean.getIdAnormalidad(),
					disOrdTareaBean.getIdTarea());
					
			logger.info("Método insertDisOrdTarea ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("No pudo Insertar la tarea");
			logger.error("Exception: {}", ex.getMessage());
			throw ex;
		}
		return rows;	
    }

    @Override
    public int updateTareaEstadoByIdOrdTarea(Long idOrdTarea) {
    	String sqlScript = DisOrdTareaMapper.SQLPOSTGRESQL_UPDATE_ESTADO_BY_ID_ORDEN_TAREA;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					idOrdTarea);
			logger.info("Método updateTareaEstadoByIdOrdTarea ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Al actualizar estado de la tarea");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }

    @Override
    public int updateTareaEstadoByIdOrden(Long idOrden) {
    	String sqlScript = DisOrdTareaMapper.SQLPOSTGRESQL_UPDATE_ESTADO_BY_ID_ORDEN;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					idOrden);
			logger.info("Método updateTareaEstadoByIdOrden ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error al momento de desactivar Tareas");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public Long nextVal() {
    	String sqlScript = DisOrdTareaMapper.SQLPOSTGRESQL_SELECT_NEXTVAL_SQTAREAORDEN;
		Long idOrdTarea = null;
		try {
			idOrdTarea = jdbcTemplate.queryForObject(sqlScript, Long.class);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No pudo obtenerse el idOrdTarea.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
		}catch (Exception ex)
		{
			logger.info("No pudo obtenerse el idOrdTarea.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return idOrdTarea;
    }
   


}
