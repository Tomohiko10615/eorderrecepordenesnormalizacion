package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedPropiedadBean;

public interface IMedPropiedadDao {
    MedPropiedadBean findByCodPropiedadAndIdEmpresa(String codPropiedad, Long idEmpresa); // Linea 3310
}
