package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IOrdTipoOrdenDao {
    Long getCountByCodInternoAndCodTipoOrden(String codInterno, String codTipoOrden); // Linea 2059
}
