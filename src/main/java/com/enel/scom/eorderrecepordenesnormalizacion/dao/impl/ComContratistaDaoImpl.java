package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IComContratistaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.ComContratistaMapper;

@Repository
public class ComContratistaDaoImpl implements IComContratistaDao {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(ComContratistaDaoImpl.class);
	
	@Override
	public Long getCountByCodContratista(String codContratista) {
		String sqlScript = ComContratistaMapper.SQLPOSTGRESQL_COUNT_COM_CONTRATISTA;
		Long count;
		try {
			count = jdbcTemplate.queryForObject(sqlScript, Long.class, codContratista);
			if (count == 0L) {
				logger.info("El contratista {} no esta configurado en COM_CONTRATISTA", codContratista);
			}
			return count;
		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
	}

}
