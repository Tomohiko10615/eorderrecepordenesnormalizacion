package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedHisComponenteDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.MedHisComponenteMapper;

@Repository
public class MedHisComponenteDaoImpl implements IMedHisComponenteDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplateBdScomUsrScom;
	
	@Autowired
	@Qualifier("oracleJdbcTemplate")
	private JdbcTemplate jdbcTemplateEdscprodUsrEorder;

	private static final Logger logger = LogManager.getLogger(MedHisComponenteDaoImpl.class);
	
    @Override
    public int updateFecHastaByIdComponete(Long idComponente) {
    	String sqlScript = MedHisComponenteMapper.SQLPOSTGRESQL_UPDATE_FEC_HASTA_MED_HIS_COMPONENTE;
		int rows = 0;
		try {
			rows = jdbcTemplateBdScomUsrScom.update(
					sqlScript,
					idComponente);
					
			logger.info("Método update ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error al Update la tabla med_his_componente");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }

    @Override
    public int insertMedidorRetiro(Long idHisComponente, Long idOrden, Long idComponente, String propiedad, Long idServicio,
            Long idEjecutor) {
    	String sqlScript = MedHisComponenteMapper.SQLPOSTGRESQL_INSERT_MEDIDOR_RETIRO;
		int rows = 0;
		try {
			rows = jdbcTemplateBdScomUsrScom.update(
					sqlScript,
					idHisComponente,
					idOrden,
					idComponente,
					propiedad,
					idServicio,
					idEjecutor,
					propiedad);
					
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando med_his_componente.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public int insertMedidorInstalacion(Long idHisComponente, Long idOrden, Long idComponente, Long idServicio) {
    	String sqlScript = MedHisComponenteMapper.SQLPOSTGRESQL_INSERT_MEDIDOR_INSTALACION;
		int rows = 0;
		try {
			rows = jdbcTemplateBdScomUsrScom.update(
					sqlScript,
					idHisComponente,
					idOrden,
					idComponente,
					idServicio);
					
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando med_his_componente.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public Long nextVal() {
        //String sqlScript = "SELECT SQAUDITEVENT.NEXTVAL FROM DUAL";
		// Agregado R.I. Se obtiene el sequence desde el SCOM
		String sqlScript = "SELECT NEXTVAL('schscom.SQAUDITEVENT')";
        Long secuencia = null;
        try {
            //secuencia = jdbcTemplateEdscprodUsrEorder.queryForObject(sqlScript, Long.class);
        	// Agregado R.I. Se ejecuta la nueva query con la conexión hacia el SCOM
        	secuencia = jdbcTemplateBdScomUsrScom.queryForObject(sqlScript, Long.class);
        } catch (EmptyResultDataAccessException ee) {
            logger.info("No pudo obtenerse la secuencia SQAUDITEVENT.");
            logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
        } catch (Exception ex) {
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return secuencia;
    }
   
}
