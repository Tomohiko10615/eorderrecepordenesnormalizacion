package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdMediCambioBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdMediCambioDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdMediCambioMapper;
@Repository
public class OrdMediCambioDaoImpl implements IOrdMediCambioDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(OrdMediCambioDaoImpl.class);
	
    @Override
    public int insert(OrdMediCambioBean ordMediCambioBean) {
    	String sqlScript = OrdMediCambioMapper.SQLPOSTGRESQL_INSERT_ORD_MEDI_CAMBIO;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript,
					ordMediCambioBean.getIdOrden(),
					ordMediCambioBean.getIdComponente(),
					// Agregado R.I. Se agrega accion
					ordMediCambioBean.getAccion()
					);
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("No se pudo insertar los datos en la tabla ORD_MEDI_CAMBIO.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }
}
