package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.AnormalidadXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IAnormalidadXmlDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;



import com.enel.scom.eorderrecepordenesnormalizacion.mapper.AnormalidadXmlMapper;
@Repository
public class AnormalidadXmlDaoImpl implements IAnormalidadXmlDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(AnormalidadXmlDaoImpl.class);

    @Override
    public List<AnormalidadXmlBean> findByIdOrdenInsp(Long idOrden) {
        logger.info("findByIdOrdenInsp({})", idOrden);
        String sqlString = AnormalidadXmlMapper.SQLPOSTGRESQL_SELECT;
        List<AnormalidadXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new AnormalidadXmlMapper(), idOrden);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage(), ee);
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage(), de);
        }

        return lista;
    }
}
