package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.TareaXmlBean;

public interface ITareaXmlDao {
    List<TareaXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long nroEvento); // Linea 1517
}
