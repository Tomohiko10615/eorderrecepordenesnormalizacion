package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorXmlBean;

public interface IMedidorXmlDao {
    List<MedidorXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones); // Linea 981
    
  //INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA
    void actualizarMedidores(String medidorXml, String modeloXml, String marcaXml, String factorXml);
    
    String selectMedidorEncontrado(String v_modelo, String v_marca, String v_componente);

	int obtenerCountFactoresValidos(String cnumeromedidor, String cmodelomedidor, String cmarcamedidor, String cfactor);

  //FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA
}
