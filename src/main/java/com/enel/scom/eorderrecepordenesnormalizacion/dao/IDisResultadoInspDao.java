package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisResultadoInspAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisResultadoInspBean;

public interface IDisResultadoInspDao {
	DisResultadoInspAaBean findByCodResultadoAndIdEmpresa(String codResultado); // Linea 2204
    // DisResultadoInspAaBean findAaByCodResultadoAndIdEmpresa(String codResultado, Long idEmpresa);
}
