package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisOrdNormBean;

public interface IDisOrdNormDao {
    int update(DisOrdNormBean disOrdNorm); // Linea 4033
    // Long getIdOrdInspByIdOrden(Long idOrden);
    Long findByIdOrden(Long idOrden); // Linea 4086
}
