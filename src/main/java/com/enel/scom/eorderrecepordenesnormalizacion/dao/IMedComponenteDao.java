package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedComponenteBean;

public interface IMedComponenteDao {
    // int update(MedComponenteBean medComponeteBean);
    int updateComponenteFuenteCliente(Long idComponente, Long idUbicacion); // Linea 3027
    int updateComponenteFuenteEjecutor(Long idComponente, Long idEjecutor); // Linea 3052
    int updateComponenteInstalar(Long idComponente, String fechaSuministro, Long idPropiedad, Long idUbicacion); // Linea 3574
    Long getIdComponenteElectricoByIdServicioAndNroComponenteAndMarcaMedidorAndModeloMedidor(Long idUbicacion, String nroComponente, String codMarca, String codModelo); // Linea 2307, 2743
    Long getIdComponenteDisponibleByNroComponenteAndIdModeloAndIdEmpresa(String nroComponente, Long idModelo); // Linea 3282
}
