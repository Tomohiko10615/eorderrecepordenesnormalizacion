package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisTareaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DisTareaMapper;
@Repository
public class DisTareaDaoImpl implements IDisTareaDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(DisTareaDaoImpl.class);
	
    @Override
    public Long findByCodTareaAndIdEmpresa(String codTarea) {
    	Long idTarea = null;
		String sqlScript = DisTareaMapper.SQLPOSTGRESQL_SELECT_FOR_ID_TAREA;
		try {
			idTarea = jdbcTemplate.queryForObject(sqlScript, Long.class, codTarea);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Tarea no existe ({}).", codTarea);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idTarea;
    }
}
