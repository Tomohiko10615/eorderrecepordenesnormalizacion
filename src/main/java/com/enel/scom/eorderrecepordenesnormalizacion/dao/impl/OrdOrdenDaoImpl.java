package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAbBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdOrdenDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenAaMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenAbMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class OrdOrdenDaoImpl implements IOrdOrdenDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(OrdOrdenDaoImpl.class);

    @Override
    public int updateFechaFinalizacionById(Long id, String fechaFinalizacion) {
        logger.info("updateFechaFinalizacionById({}, {})", id, fechaFinalizacion);
        String sqlString = ""
        + "UPDATE \n"
        + "    ORD_ORDEN\n"
        + "SET \n"
        + "      FECHA_FINALIZACION = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss')\n"
        + "    , FECHA_INGRESO_ESTADO_ACTUAL = NOW()\n"
        + "WHERE 1 = 1\n"
        + "    AND ID = ?\n"
        ;

        int rows = 0;

        try {
            rows = jdbcTemplate.update(sqlString, fechaFinalizacion, id);
            logger.info("Update. Filas afectadas: {}.", rows);

        } catch (Exception ex) {
            logger.error("Error al actualizar.");
            logger.error("Exception: {}", ex.getMessage());
            throw ex;
        }
        return rows;
    }

    @Override
    public OrdOrdenBean findByIdOrdenAndDiscriminador(Long idOrden, String discriminador) {
        logger.info("findByIdOrdenAndDiscriminador({}, {})", idOrden, discriminador);
        String sqlString = OrdOrdenMapper.SQLPOSTGRESQL_SELECT_ALL_FROM_ORDORDEN_BY_ID_AND_DISCRIMINADOR;
        OrdOrdenBean object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, new OrdOrdenMapper(), idOrden, discriminador);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }

    @Override
    public OrdOrdenAaBean findAaByNroOrden(String nroOrden) {
        logger.info("findAaByNroOrden({})", nroOrden);
        String sqlString = OrdOrdenAaMapper.SQLPOSTGRESQL_SELECT;
        OrdOrdenAaBean object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, new OrdOrdenAaMapper(), nroOrden);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }

    @Override
    public OrdOrdenAbBean findAbByNroOrden(String nroOrden, Long idEmpresa) {
        logger.info("findAbByNroOrden({}, {})", nroOrden,  idEmpresa);
        String sqlString = OrdOrdenAbMapper.SQLPOSTGRESQL_SELECT;
        OrdOrdenAbBean object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, new OrdOrdenAbMapper(), nroOrden);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }


}
