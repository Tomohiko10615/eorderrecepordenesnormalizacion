package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface INucClienteDao {
    Long getIdClienteByIdServicio(Long idServicio);
}
