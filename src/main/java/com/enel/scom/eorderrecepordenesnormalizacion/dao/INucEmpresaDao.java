package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.NucEmpresaBean;

public interface INucEmpresaDao {
    NucEmpresaBean findByCodPartition(String codPartition); // Linea 592
}
