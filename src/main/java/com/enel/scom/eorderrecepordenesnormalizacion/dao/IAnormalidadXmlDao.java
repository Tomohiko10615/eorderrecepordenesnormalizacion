package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.AnormalidadXmlBean;

public interface IAnormalidadXmlDao {
    List<AnormalidadXmlBean> findByIdOrdenInsp(Long idOrden); // Linea 1454
}
