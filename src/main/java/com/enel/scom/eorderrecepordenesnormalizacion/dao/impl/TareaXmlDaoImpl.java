package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.TareaXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.ITareaXmlDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.TareaXmlMapper;
@Repository
public class TareaXmlDaoImpl implements ITareaXmlDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(TareaXmlDaoImpl.class);

    @Override
    public List<TareaXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long nroEvento) {
        String sqlString = TareaXmlMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR;
        List<TareaXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new TareaXmlMapper(), vIdOrdTransfer, nroEvento);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion para los datos {} {}.", vIdOrdTransfer, nroEvento);
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage(), ee);
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage(), de);
        }

        return lista;
    }
}
