package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedComponenteMedidaMedidorBean;

public interface IMedComponenteMedidaMedidorDao {

	void deleteFromMedidaMedidorByIdComponente(Long dIdComponente_i, List<Integer> idMedidas);
    // MedComponenteMedidaMedidorBean getByIdCompoente(Long idComponente);
}
