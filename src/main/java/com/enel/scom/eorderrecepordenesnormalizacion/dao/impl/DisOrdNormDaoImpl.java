package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisOrdNormBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisOrdNormDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DisOrdNormMapper;
@Repository
public class DisOrdNormDaoImpl implements IDisOrdNormDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(DisOrdNormDaoImpl.class);
	
    @Override
    public int update(DisOrdNormBean disOrdNorm) {
		logger.info("disOrdNorm: {}", disOrdNorm);
    	String sqlScript = DisOrdNormMapper.SQLPOSTGRESQL_UPDATE_DIS_ORD_NORM;
		// String sqlScript = "";
		// sqlScript += "update dis_ord_norm\n";
		// sqlScript += "set\n";
		// sqlScript += "ID_EJECUTOR_eje = ?\n";
		// sqlScript += ", FEC_hora_INI_EJE = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss')\n";
		// sqlScript += ", FEC_hora_FIN_EJE = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss')\n";
		// sqlScript += ", ID_RESULTADO = ?\n";
		// if (disOrdNorm.getNroOtFisica() != null)
		// 	sqlScript += ", nro_ot_fisica ?\n";
		// if (disOrdNorm.getFecNormalizacion() != null)
		// 	sqlScript += ", fec_normalizacion = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss')\n";
		// if (disOrdNorm.getIdActComNotif() != null)
		// 	sqlScript += ", ID_ACT_COM_NOTIF = ?\n";
		// if (disOrdNorm.getLugarNotificado() != null || !disOrdNorm.getLugarNotificado().isEmpty())
		// 	sqlScript += ", LUGAR_NOTIFICADO = ?\n";
		// sqlScript += "where\n";
		// sqlScript += "id_orden = ?\n";

		

		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					disOrdNorm.getIdEjecutorEje(), 
					disOrdNorm.getFecHoraIniEje(),
					disOrdNorm.getFecHoraFinEje(),
					disOrdNorm.getIdResultado(),
					disOrdNorm.getNroOtFisica(),
					disOrdNorm.getFecNormalizacion(),
					disOrdNorm.getIdActComNotif(),
					disOrdNorm.getLugarNotificado(),
					disOrdNorm.getIdOrden());
			logger.info("Método updateDisOrdNorm ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando orden Norm(Gestion de Perdidas).");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }

    @Override
    public Long findByIdOrden(Long idOrden) {
    	Long idOrdInsp = null;
		String sqlScript = DisOrdNormMapper.SQLPOSTGRESQL_SELECT_FOR_ID_ORD_INSP;
		try {
			idOrdInsp = jdbcTemplate.queryForObject(sqlScript, Long.class, idOrden);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No pudo obtenerse el ID de Orden de Inspeccion.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idOrdInsp;
    }
   
}
