package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IComParentescoDao {
    Long findByCodParentescoAndIdEmpresa(String codParentesco, Long idEmpresa); // Linea 2186
    // ComParentescoAaBean findAaByCodParentescoAndIdEmpresa(String codParentesco, Long idEmpresa); 
}
