package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorInterfazCnrBean;

public interface IEorInterfazCnrDao {
    int getCountByNumeroInspeccionAndTipoOrden(Long numeroInspeccion); // Linea 4119
    int insert(EorInterfazCnrBean eorInterfazCnrDao); // Linea 4197
    Long nextVal(); // Linea 4185

}
