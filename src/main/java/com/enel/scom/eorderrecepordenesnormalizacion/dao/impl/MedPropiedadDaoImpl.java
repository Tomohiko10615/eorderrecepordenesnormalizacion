package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedPropiedadBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedPropiedadDao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class MedPropiedadDaoImpl implements IMedPropiedadDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(MedPropiedadDaoImpl.class);

    @Override
    public MedPropiedadBean findByCodPropiedadAndIdEmpresa(String codPropiedad, Long idEmpresa) {
        logger.info("findByCodPropiedadAndIdEmpresa({}, {})", codPropiedad, idEmpresa);
        String sqlString = ""
                + "SELECT ID, COD_PROPIEDAD, DES_PROPIEDAD, ACTIVO, COD_INTERNO\n"
                + "FROM SCHSCOM.MED_PROPIEDAD\n"
                + "WHERE COD_PROPIEDAD = ?\n";
        MedPropiedadBean object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, (rs, rowNum) -> MedPropiedadBean.builder()
                    .id(rs.getLong("ID"))
                    .codPropiedad(rs.getString("COD_PROPIEDAD"))
                    .desPropiedad(rs.getString("DES_PROPIEDAD"))
                    .activo(rs.getString("ACTIVO"))
                    .codInterno(rs.getString("COD_INTERNO"))
                    .build(), codPropiedad);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }

}
