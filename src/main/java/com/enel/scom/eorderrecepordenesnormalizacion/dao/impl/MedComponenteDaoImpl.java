package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedComponenteDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.MedComponenteMapper;
@Repository
public class MedComponenteDaoImpl implements IMedComponenteDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(MedComponenteDaoImpl.class);
	
    @Override
    public int updateComponenteFuenteCliente(Long idComponente, Long idUbicacion) {
    	String sqlScript = MedComponenteMapper.SQLPOSTGRESQL_UPDATE_COMPONENTE_FUENTE_CLIENTE;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					idUbicacion,
					idComponente);
			logger.info("Método updateComponenteFuenteCliente ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando MedComponente");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }

    @Override
    public int updateComponenteFuenteEjecutor(Long idComponente, Long idEjecutor) {
    	String sqlScript = MedComponenteMapper.SQLPOSTGRESQL_UPDATE_COMPONENTE_FUENTE_EJECUTOR;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					idEjecutor,
					idComponente);
			logger.info("Método updateComponenteFuenteEjecutor ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando MedComponente");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public int updateComponenteInstalar(Long idComponente, String fechaSuministro, Long idPropiedad, Long idUbicacion) {
    	String sqlScript = MedComponenteMapper.SQLPOSTGRESQL_UPDATE_COMPONENTE_INSTALAR;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					idUbicacion,
					idPropiedad,
					fechaSuministro,
					idComponente);
			logger.info("Método updateComponenteInstalar ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error: Actualizando MedComponente");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public Long getIdComponenteElectricoByIdServicioAndNroComponenteAndMarcaMedidorAndModeloMedidor(Long idUbicacion,
            String nroComponente, String codMarca, String codModelo) {
    	Long idComponenteElectrico = null;
		String sqlScript = MedComponenteMapper.SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_ELECTRICO;
		try {
			idComponenteElectrico = jdbcTemplate.queryForObject(sqlScript, Long.class,
					idUbicacion,
					nroComponente,
					codMarca,
					codModelo);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No existe idComponenteElectrico");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idComponenteElectrico;
    }

    @Override
    public Long getIdComponenteDisponibleByNroComponenteAndIdModeloAndIdEmpresa(String nroComponente, Long idModelo) {
    	Long idComponenteDisponible = null;
		String sqlScript = MedComponenteMapper.SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_DISPONIBLE;
		try {
			idComponenteDisponible = jdbcTemplate.queryForObject(sqlScript, Long.class,
					nroComponente,
					idModelo);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No existe idComponenteDisponible");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idComponenteDisponible;
    }
    
}
