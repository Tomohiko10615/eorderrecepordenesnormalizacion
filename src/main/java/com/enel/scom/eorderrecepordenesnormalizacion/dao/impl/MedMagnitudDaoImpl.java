package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;


import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedMagnitudBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedMagnitudDao;

@Repository
public class MedMagnitudDaoImpl implements IMedMagnitudDao {
    @Autowired
    @Qualifier("oracleJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(MedMagnitudDaoImpl.class);

    @Override
    public int insertMagnitudLectura(MedMagnitudBean medMagnitudBean) {
        logger.info("insertMagnitudLectura({})", medMagnitudBean);

        String sqlString = ""
                + "INSERT INTO MED_MAGNITUD\n"
                + "(\n"
                + "      ID_MAGNITUD\n"
                + "    , ID_EMPRESA\n"
                + "    , ID_COMPONENTE\n"
                + "    , ID_SERVICIO\n"
                + "    , ID_MEDIDA\n"
                + "    , ENTEROS\n"
                + "    , DECIMALES\n"
                + "    , FACTOR\n"
                + "    , VALOR\n"
                + "    , TIPO\n"
                + "    , ID_TIP_MAGNITUD\n"
                + "    , OBSERVACIONES\n"
                + "    , FEC_LEC_TERRENO\n"
                + "    , SEC_MAGNITUD\n"
                + "    , ID_EST_MAGNITUD\n"
                + "    , ID_TIP_CALCULO\n"
                + "    , ID_TIPO_CONS\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "      ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + "    , ?\n"
                + ")\n";
        int rows = 0;
        try {
            rows = jdbcTemplate.update(sqlString, medMagnitudBean.getIdMagnitud(), medMagnitudBean.getIdEmpresa(),
                    medMagnitudBean.getIdComponente(), medMagnitudBean.getIdServicio(), medMagnitudBean.getIdMedida(),
                    medMagnitudBean.getEnteros(), medMagnitudBean.getDecimales(), medMagnitudBean.getFactor(),
                    medMagnitudBean.getValor(), medMagnitudBean.getTipo(), medMagnitudBean.getIdTipMagnitud(),
                    medMagnitudBean.getObservaciones(), medMagnitudBean.getFecLecTerreno(),
                    medMagnitudBean.getSecMagnitud(), medMagnitudBean.getIdEstMagnitud(),
                    medMagnitudBean.getIdTipCalculo(), medMagnitudBean.getIdTipoCons());
            logger.info("Insert. Filas afectadas: {}", rows);
        } catch (DataAccessException de) {
            logger.info("Error al insertar.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return rows;
    }

    @Override
    public Long nextVal() {
        String sqlScript = "SELECT SQMEDMAGNITUD.NEXTVAL FROM DUAL";
        Long secuencia = null;
        try {
            secuencia = jdbcTemplate.queryForObject(sqlScript, Long.class);
        } catch (EmptyResultDataAccessException ee) {
            logger.info("No pudo obtenerse la secuencia.");
            logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
        } catch (Exception ex) {
            logger.info("No pudo obtenerse el idOrdTarea.");
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return secuencia;
    }

}
