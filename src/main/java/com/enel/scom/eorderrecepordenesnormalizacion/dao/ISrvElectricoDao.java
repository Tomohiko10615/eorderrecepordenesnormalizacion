package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface ISrvElectricoDao {
    int updateSecMagnitudByIdServicio(Long idServicio, Long secMagnitud); // Linea 4000
    Long getSecMagnitudByIdServicio(Long idServicio); // Linea 3951
}
