package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.NucEmpresaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.NucEmpresaMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.INucEmpresaDao;

@Repository
public class NucEmpresaDaoImpl implements INucEmpresaDao {
	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(NucEmpresaDaoImpl.class);

	@Override
	public NucEmpresaBean findByCodPartition(String codPartition) {
		NucEmpresaBean objecto = null;
		String sqlScript = NucEmpresaMapper.SQLPOSTGRESQL_SELECT_FOR_NUCEMPRESA_BY_CODPARTITION;
		try {
			objecto = jdbcTemplate.queryForObject(sqlScript, new NucEmpresaMapper(), codPartition);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Error: No existe ID_EMPRESA para el parametro <Empresa> ingresado.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return objecto;
	}

}
