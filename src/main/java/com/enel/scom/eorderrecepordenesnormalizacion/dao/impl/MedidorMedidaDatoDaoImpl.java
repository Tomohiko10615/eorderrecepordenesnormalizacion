package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorMedidaDatoBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedidorMedidaDatoDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.MedidorMedidaDatoMapper;
@Repository
public class MedidorMedidaDatoDaoImpl implements IMedidorMedidaDatoDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(MedidorMedidaDatoDaoImpl.class);
	
    @Override
    public MedidorMedidaDatoBean findByIdComponenteRetirar(Long idComponente) {
		logger.info("findByIdComponenteRetirar({})", idComponente);
    	MedidorMedidaDatoBean medidorMedidaDatoBean = null;
		String sqlScript = MedidorMedidaDatoMapper.SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_RETIRAR;
		try {
			medidorMedidaDatoBean = jdbcTemplate.queryForObject(sqlScript, new MedidorMedidaDatoMapper(),
					idComponente);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No existe ComponenteRetirar");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return medidorMedidaDatoBean;
    }

    @Override
    public MedidorMedidaDatoBean findByIdComponenteInstalar(Long idComponente) {
		logger.info("findByIdComponenteInstalar({})", idComponente);
    	MedidorMedidaDatoBean medidorMedidaDatoBean = null;
		String sqlScript = MedidorMedidaDatoMapper.SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_INSTALAR;
		try {
			medidorMedidaDatoBean = jdbcTemplate.queryForObject(sqlScript, new MedidorMedidaDatoMapper(),
					idComponente);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No existe ComponenteInstalar");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return medidorMedidaDatoBean;
    }

}
