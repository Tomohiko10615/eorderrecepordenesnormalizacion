package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedMagnitudBean;

public interface IMedMagnitudDao {
    int insertMagnitudLectura(MedMagnitudBean medMagnitudBean); // Linea 2511
    Long nextVal(); // Linea 2492
    
}
