package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.LecturaXmlBean;

public interface ILecturaXmlDao {
    List<LecturaXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long nroEvento, String vNroMedidor, String vAccionMedidor); // Linea 1145
}
