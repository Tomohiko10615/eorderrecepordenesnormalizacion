package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.Medidor4jBean;

public interface IMedidor4jDao {
    // List<Medidor4jBean> findByIdServicio(Double dIdServicio);
    List<Medidor4jBean> findByIdUbicacion(Long idUbicacion); // Linea 2255
    
}
