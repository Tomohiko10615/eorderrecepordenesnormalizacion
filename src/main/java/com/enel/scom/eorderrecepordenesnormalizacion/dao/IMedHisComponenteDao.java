package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IMedHisComponenteDao {
    int updateFecHastaByIdComponete(Long idComponente); // Linea 3101, 3625
    int insertMedidorRetiro(Long idHisComponente, Long idOrden, Long idComponente, String propiedad, Long idServicio, Long idEjecutor); // Linea 3119
    int insertMedidorInstalacion(Long idHisComponente, Long idOrden, Long idComponente, Long idServicio); // Linea 3644
    Long nextVal();
}
