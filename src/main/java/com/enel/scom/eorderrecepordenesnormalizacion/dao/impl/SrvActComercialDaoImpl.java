package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.SrvActComercialBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.ISrvActComercialDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.enel.scom.eorderrecepordenesnormalizacion.mapper.SrvActComercialMapper;
@Repository
public class SrvActComercialDaoImpl implements ISrvActComercialDao{
	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(SrvActComercialDaoImpl.class);
    @Override
    public SrvActComercialBean findByCodActComercialAndIdEmpresa(String codActComercial, Long idEmpresa) {
        SrvActComercialBean objecto = null;
		String sqlScript = SrvActComercialMapper.SQLPOSTGRESQL_SELECT_ALL;
		try {
			objecto = jdbcTemplate.queryForObject(sqlScript, new SrvActComercialMapper(), codActComercial, idEmpresa);
		} catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion para los datos {} {}.", codActComercial, idEmpresa);
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }

		return objecto;
    }
    
}
