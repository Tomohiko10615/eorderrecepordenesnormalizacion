package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorMedidaDatoBean;

public interface IMedidorMedidaDatoDao {
    MedidorMedidaDatoBean findByIdComponenteRetirar(Long idComponente); // Linea 2776
    MedidorMedidaDatoBean findByIdComponenteInstalar(Long idComponente); // Linea 3328
}
