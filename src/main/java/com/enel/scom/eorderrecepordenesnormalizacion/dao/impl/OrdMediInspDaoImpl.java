package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdMediInspBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdMediInspDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdMediInspMapper;
@Repository
public class OrdMediInspDaoImpl implements IOrdMediInspDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(OrdMediInspDaoImpl.class);
	
    @Override
    public int insert(OrdMediInspBean ordMediInspBean) {
    	String sqlScript = OrdMediInspMapper.SQLPOSTGRESQL_INSERT_ORD_MEDI_INSP;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript,
					ordMediInspBean.getIdOrden(),
					ordMediInspBean.getIdComponente(),
					ordMediInspBean.getIdEmpresa(),
					ordMediInspBean.getIdMediInsp()
					);
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("No se pudo insertar los datos en la tabla ORD_MEDI_INSP.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;
    }

    @Override
    public Long nextVal() {
    	String sqlScript = OrdMediInspMapper.SQLPOSTGRESQL_SELECT_FOR_SQINSPECCIONMEDIDORESORDEN;
		Long idMediInsp = null;
		try {
			idMediInsp = jdbcTemplate.queryForObject(sqlScript, Long.class);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No pudo obtenerse el idMediInsp.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
		}catch (Exception ex)
		{
			logger.info("No pudo obtenerse el idMediInsp.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return idMediInsp;
    }


}
