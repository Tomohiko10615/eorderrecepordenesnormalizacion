package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComCuadrillaAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IComCuadrillaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.ComCuadrillaMapper;

@Repository
public class ComCuadrillaDaoImpl implements IComCuadrillaDao {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(ComCuadrillaDaoImpl.class);
	
	@Override
	public Long getCountByCodCuadrillaAndIdEmpresa(String codCuadrilla) {
		String sqlScript = ComCuadrillaMapper.SQLPOSTGRESQL_SELECT_COUNT_CUADRILLA;
		Long count;
		try {
			count = jdbcTemplate.queryForObject(sqlScript, Long.class, codCuadrilla);
			if (count == 0) {
				logger.info("Cuadrilla no existe ({})", codCuadrilla);
			}
			return count;
		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
	}

	@Override
	public ComCuadrillaAaBean findAaByCodCuadrillaAndIdEmpresa(String codCuadrilla) {
		ComCuadrillaAaBean oCuadrilla = null;
		String sqlScript = ComCuadrillaMapper.SQLPOSTGRESQL_SELECT_FOR_CUADRILLA;
		try {
			oCuadrilla = jdbcTemplate.queryForObject(sqlScript, new ComCuadrillaMapper(), codCuadrilla);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Cuadrilla no encontrada. {}", codCuadrilla);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);

		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return oCuadrilla;
	}

}
