package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisAnormalidadDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DisAnormalidadMapper;
@Repository
public class DisAnormalidadDaoImpl implements IDisAnormalidadDao {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(DisAnormalidadDaoImpl.class);
	
    @Override
    public Long findByCodAnormalidadAndIdEmpresa(String codAnormalidad) {
    	Long idAnormalidad = null;
		String sqlScript = DisAnormalidadMapper.SQLPOSTGRESQL_SELECT_FOR_ID_ANORMALIDAD;
		try {
			idAnormalidad = jdbcTemplate.queryForObject(sqlScript, Long.class, codAnormalidad);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("Irregularidad no existe ({}).", codAnormalidad);
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
			throw ee;
		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
		return idAnormalidad;
    }
    
}
