package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.DatoProcesoXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorOrdTransferDetDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.DatoProcesoMapper;

@Repository
public class EorOrdTransferDetDaoImpl implements IEorOrdTransferDetDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(EorOrdTransferDetDaoImpl.class);

    @Override
    public String getCodigoContratistaByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones) {
        logger.info("getCodigoContratistaByIdOrdTransferAndNroEvento({}, {})", vIdOrdTransfer, vNroRecepciones);
        String sqlString = ""
                + "SELECT\n"
                + "    (XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/Codigo_Contratista/text()', D.REG_XML))[1]::TEXT AS CODIGO_CONTRATISTA\n"
                + "FROM \n"
                + "    EOR_ORD_TRANSFER_DET D\n"
                + "WHERE 1 = 1\n"
                + "    AND D.ID_ORD_TRANSFER = ?\n"
                + "    AND D.ACCION ='RECEPCION'\n"
                + "    AND D.NRO_EVENTO = ?\n"
                + "    AND D.FEC_ACCION IN (SELECT C.FEC_OPERACION FROM EOR_ORD_TRANSFER C WHERE D.ID_ORD_TRANSFER =C.ID_ORD_TRANSFER)\n";
        String object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, String.class, vIdOrdTransfer, vNroRecepciones);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }

    @Override
    public List<DatoProcesoXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones) {
        logger.info("findByIdOrdTransferAndNroEvento({}, {})", vIdOrdTransfer, vNroRecepciones);
        String sqlString = DatoProcesoMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_DATOPROCESO;
        List<DatoProcesoXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new DatoProcesoMapper(), vIdOrdTransfer, vNroRecepciones);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return lista;
    }

    @Override
    public int updateTransferDetProcesoRecepcionaOrdenNormalizacion(String vCodOperacion, Long vIdOrdTransfer, Long vNroRecepciones) {
        String sqlString = ""
                + "UPDATE\n"
                + "    EOR_ORD_TRANSFER_DET D\n"
                + "SET\n"
                + "      COD_ERROR = ?\n"
                + "    , FEC_ACCION = (SELECT FEC_OPERACION FROM EOR_ORD_TRANSFER C WHERE C.ID_ORD_TRANSFER = D.ID_ORD_TRANSFER)\n"
                + "where 1 = 1\n"
                + "    and D.ID_ORD_TRANSFER = ?\n"
                + "    AND D.ACCION  = 'RECEPCION'\n"
                + "    AND D.NRO_EVENTO = ?\n";

        int rows = 0;
        try {
            rows = jdbcTemplate.update(sqlString, vCodOperacion, vIdOrdTransfer, vNroRecepciones);
            logger.info("Método update, trasnfer det, filas afectadas: {}", rows);

        } catch (Exception ex) {
            logger.info("Error al actualizar el estado Recepcionado con errores");
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return rows;
    }

}
