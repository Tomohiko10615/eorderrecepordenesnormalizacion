package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IDisAnormalidadDao {
    Long findByCodAnormalidadAndIdEmpresa(String codAnormalidad); // Linea 4258
    
}
