package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedModeloDao;


import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedPropiedadBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedPropiedadDao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAbBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdOrdenDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenAaMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenAbMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.OrdOrdenMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
@Repository
public class MedModeloDaoImpl implements IMedModeloDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(MedModeloDaoImpl.class);
    @Override
    public Long getIdModelByCodModelAndCodMarca(String codModelo, String codMarca) {
        logger.info("findByCodPropiedadAndIdEmpresa({}, {})", codModelo, codMarca);
        String sqlString = ""
        + "SELECT\n"
        + "    MM.ID\n"
        + "FROM\n"
        + "    MED_MODELO MM\n"
        + "    INNER JOIN MED_MARCA MAR ON MM.ID_MARCA = MAR.ID\n"
        + "WHERE 1 = 1\n"
        + "    -- AND MM.ID_EMPRESA = 1\n"
        + "    AND MM.COD_MODELO = ?\n"
        + "    AND MAR.COD_MARCA = ?\n"
                ;
        Long object = null;
        try {
            object = jdbcTemplate.queryForObject(sqlString, Long.class, codModelo, codMarca);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }
}
