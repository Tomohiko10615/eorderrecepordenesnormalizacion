package com.enel.scom.eorderrecepordenesnormalizacion.dao;

public interface IDisTareaDao {
    Long findByCodTareaAndIdEmpresa(String codTarea); // Linea 4243
}
