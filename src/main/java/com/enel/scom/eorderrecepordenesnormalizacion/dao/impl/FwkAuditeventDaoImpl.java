package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.FwkAuditeventBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IFwkAuditeventDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.FwkAuditeventMapper;
@Repository
public class FwkAuditeventDaoImpl implements IFwkAuditeventDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(FwkAuditeventDaoImpl.class);
	
    @Override
    public int insert(FwkAuditeventBean fwkAuditeventBean) {
    	String sqlScript = FwkAuditeventMapper.SQLPOSTGRESQL_INSERT_INTO_FWK_AUDITEVENT;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript,
					fwkAuditeventBean.getUsecase(),
					fwkAuditeventBean.getIdFk(),
					fwkAuditeventBean.getIdUser());
					
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("Error al insertar la auditoria");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }
}
