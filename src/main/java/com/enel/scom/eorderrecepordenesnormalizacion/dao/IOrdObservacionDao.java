package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdObservacionBean;

public interface IOrdObservacionDao {
    Long nextVal(); // Linea 3904
    int insert(OrdObservacionBean ordObservacionBean); // Linea 3909
}
