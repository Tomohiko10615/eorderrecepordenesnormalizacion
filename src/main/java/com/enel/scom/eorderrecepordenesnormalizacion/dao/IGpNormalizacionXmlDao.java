package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.GpNormalizacionXmlBean;

public interface IGpNormalizacionXmlDao {
    List<GpNormalizacionXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones); // Linea 1400
}
