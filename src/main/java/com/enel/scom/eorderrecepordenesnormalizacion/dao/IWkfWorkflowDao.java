package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.WkfWorkflowBean;

public interface IWkfWorkflowDao {
    int updateIdStateByIdWorkflow(Long idWorkflow, String idState); // Linea 3759
    int updateIdStateByIdWorkflowAndIdState(Long idWorkflow, String idState, String newIdState); // Linea 3806
    // int updateEstadoByIdAndIdState(Long id_workflow, String id_stare_whr, String id_state_upd);
    // String getIdState(Long id_workflow);
    WkfWorkflowBean findById(Long idWorkflow); // 3778
}
