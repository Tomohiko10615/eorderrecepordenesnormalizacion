package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComCuadrillaAaBean;

public interface IComCuadrillaDao {
    // ComCuadrillaBean findByCodCuadrillaAndIdEmpresa(String COD_CUADRILLA, Long ID_EMPRESA);
    Long getCountByCodCuadrillaAndIdEmpresa(String codCuadrilla); // Linea 2119
    ComCuadrillaAaBean findAaByCodCuadrillaAndIdEmpresa(String codCuadrilla); // Linea 2138

}