package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdInspLecturaBean;

public interface IOrdInspLecturaDao {
    int insert(OrdInspLecturaBean ordInspLecturaBean); // Linea 2630
    Long nextVal(); // Linea 2613
}
