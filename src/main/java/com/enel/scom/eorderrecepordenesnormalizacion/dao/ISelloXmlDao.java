package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.SelloXmlBean;

public interface ISelloXmlDao {
    List<SelloXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones); // Linea 1349
}
