package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorInterfazCnrBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorInterfazCnrDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.EorInterfazCnrMapper;
@Repository
public class EorInterfazCnrDaoImpl implements IEorInterfazCnrDao{

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(EorInterfazCnrDaoImpl.class);
	
    @Override
    public int getCountByNumeroInspeccionAndTipoOrden(Long numeroInspeccion) {
    	String sqlScript = EorInterfazCnrMapper.SQLPOSTGRESQL_SELECT_FOR_COUNT_EOR_INTERFAZ_CNR;
		int count;
		try {
			count = jdbcTemplate.queryForObject(sqlScript, Integer.class, numeroInspeccion);
			if (count == 0) {
				logger.info("No pudo obtenerse el ID de Resultado de Inspeccion.");
			}
			return count;
		} catch (DataAccessException de) {
			logger.error("DataAccessException: {} {}", de.getMessage(), de);
			throw de;
		}
    }

    @Override
    public int insert(EorInterfazCnrBean eorInterfazCnrBean) {
    	String sqlScript = EorInterfazCnrMapper.SQLPOSTGRESQL_INSERT_INTO_EOR_INTERFAZ_CNR;
		int rows = 0;
		try {
			rows = jdbcTemplate.update(
					sqlScript, 
					eorInterfazCnrBean.getIdOrdInterfcnr(),
					eorInterfazCnrBean.getNumeroServicio(),
					eorInterfazCnrBean.getNumeroInspeccion(),
					eorInterfazCnrBean.getNumeroCuenta(),
					eorInterfazCnrBean.getTipoOrden(),
					eorInterfazCnrBean.getMotivo(),
					eorInterfazCnrBean.getSubTipoOrden());
			logger.info("Método insert ejecutado, filas afectadas: {}", rows);
		}catch (Exception ex)
		{
			logger.info("No se pudo insertar los datos en la tabla EOR_INTERFAZ_CNR.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return rows;	
    }

    @Override
    public Long nextVal() {
    	String sqlScript = EorInterfazCnrMapper.SQLPOSTGRESQL_SELECT_NEXTVAL_SQORDINTERFCNR;
		Long idOrdInterfCnr = null;
		try {
			idOrdInterfCnr = jdbcTemplate.queryForObject(sqlScript, Long.class);
		} catch (EmptyResultDataAccessException ee) {
			logger.info("No se encuentra Secuencia para eor_interfaz_cnr.");
			logger.error("EmptyResultDataAccessException: {} {}", ee.getMessage(), ee);
		}catch (Exception ex)
		{
			logger.info("No se encuentra Secuencia para eor_interfaz_cnr.");
			logger.error("Exception: {}", ex.getMessage(), ex);
			throw ex;
		}
		return idOrdInterfCnr;
    }

}
