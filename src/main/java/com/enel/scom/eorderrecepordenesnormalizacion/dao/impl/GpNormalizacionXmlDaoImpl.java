package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.GpNormalizacionXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IGpNormalizacionXmlDao;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.ArrayList;

import com.enel.scom.eorderrecepordenesnormalizacion.mapper.GpNormalizacionXmlMapper;
@Repository
public class GpNormalizacionXmlDaoImpl implements IGpNormalizacionXmlDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(GpNormalizacionXmlDaoImpl.class);
    @Override
    public List<GpNormalizacionXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones) {
        logger.info("findByIdOrdTransferAndNroEvento({}, {})", vIdOrdTransfer, vNroRecepciones);
        String sqlString = GpNormalizacionXmlMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR;
        List<GpNormalizacionXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new GpNormalizacionXmlMapper(), vIdOrdTransfer, vNroRecepciones);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return lista;
    }
}
