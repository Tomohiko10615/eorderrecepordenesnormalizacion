package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.ArrayList;

import com.enel.scom.eorderrecepordenesnormalizacion.mapper.MedidorXmlMapper;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedidorXmlDao;

@Repository
public class MedidorXmlDaoImpl implements IMedidorXmlDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(MedidorXmlDaoImpl.class);

    @Override
    public List<MedidorXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long vNroRecepciones) {
        logger.info("findByIdOrdTransferAndNroEvento({}, {})", vIdOrdTransfer, vNroRecepciones);
        String sqlString = MedidorXmlMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR;
        List<MedidorXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new MedidorXmlMapper(), vIdOrdTransfer, vNroRecepciones);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return lista;
    }
    
    // INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA

    @Transactional
    @Override
    public void actualizarMedidores(String medidorXml, String modeloXml, String marcaXml, String factorXml) {
    	try {
			 String sqlString = MedidorXmlMapper.SQLPOSTGRESQL_UPDATE_MEDIDORES;
			 logger.debug("Executing query PrintPlanoMapper.SQLPOSTGRESQL_UPDATE_MEDIDORES");
			 jdbcTemplate.update(sqlString,  medidorXml, modeloXml, marcaXml,
					 	modeloXml, marcaXml, factorXml, 
					 	medidorXml, modeloXml, marcaXml, 
						modeloXml, marcaXml, factorXml);
		} catch (EmptyResultDataAccessException ee) {
          logger.info("Error en Executing query MAPPERS.QUERY.SQLPOSTGRESQL_UPDATE_MEDIDORES: {}",ee.getMessage());
		}
    }
    
    @Override
	public String selectMedidorEncontrado(String v_modelo, String v_marca, String v_componente) {
		String result = "";
		try {
			String sqlString = MedidorXmlMapper.SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO;
			logger.debug("Executing query SCOMMapper.SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO");
			result = jdbcTemplate.queryForObject(sqlString, String.class, v_modelo,v_marca,v_componente  );
		} catch (EmptyResultDataAccessException ee) {
			logger.error("Error en Executing query SCOMMapper.SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO: {}",
					ee.getMessage());
			throw ee;
		} catch (DataAccessException e) {
			logger.error("Error en Executing query SCOMMapper.SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO: {}",
					e.getMessage());
			throw e;
		}
		return result;
	}

	@Override
	public int obtenerCountFactoresValidos(String medidorXml, String modeloXml, String marcaXml,
			String cfactor) {
		int count = 0;
		try {
			 String sqlString = MedidorXmlMapper.SQLPOSTGRESQL_COUNT_MEDIDORES;
			 logger.debug("Executing query PrintPlanoMapper.SQLPOSTGRESQL_SELECT_COUNT_MEDIDORES");
			 count = jdbcTemplate.queryForObject(sqlString, Integer.class,  medidorXml, modeloXml, marcaXml,
					 	modeloXml, marcaXml, cfactor, 
					 	medidorXml, modeloXml, marcaXml, 
						modeloXml, marcaXml, cfactor);
		} catch (EmptyResultDataAccessException ee) {
         logger.info("Error en Executing query MAPPERS.QUERY.SQLPOSTGRESQL_SELEC_COUNT_MEDIDORES: {}",ee.getMessage());
		}
		return count;
	}
	
    //FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA
}
