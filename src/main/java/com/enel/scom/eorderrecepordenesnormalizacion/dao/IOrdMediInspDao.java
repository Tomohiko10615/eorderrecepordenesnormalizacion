package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdMediInspBean;

public interface IOrdMediInspDao {
    int insert(OrdMediInspBean ordMediInspBean); // Linea 2585
    Long nextVal(); // Linea 2567


}
