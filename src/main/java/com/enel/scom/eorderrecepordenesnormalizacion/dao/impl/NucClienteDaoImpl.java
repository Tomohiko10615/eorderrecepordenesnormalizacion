package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.INucClienteDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
@Repository
public class NucClienteDaoImpl implements INucClienteDao {
    @Autowired
    @Qualifier("oracleJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(NucClienteDaoImpl.class);

    @Override
    public Long getIdClienteByIdServicio(Long idServicio) {
        logger.info("getIdClienteByIdServicio({})", idServicio);
        Long idCliente = 0L;
        String sqlString = ""
                + "select cli.id_cliente\n"
                + "from nuc_cliente cli, nuc_cuenta nc , nuc_servicio sv\n"
                + "where cli.id_cliente = nc.id_cliente\n"
                + "and nc.id_cuenta = sv.id_cuenta\n"
                + "and sv.id_servicio = ?\n";

        try {
            idCliente = jdbcTemplate.queryForObject(sqlString, Long.class, idServicio);
            logger.info("Valor: {}.", idCliente);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return -1L;
        } catch (DataAccessException de) {
            logger.error("Error al obtener sec_magnitud.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return idCliente;
    }

}
