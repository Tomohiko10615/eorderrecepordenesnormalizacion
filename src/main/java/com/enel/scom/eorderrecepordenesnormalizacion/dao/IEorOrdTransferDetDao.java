package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import java.util.List;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DatoProcesoXmlBean;

public interface IEorOrdTransferDetDao {
	String getCodigoContratistaByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long nroEvento); // Linea 494
	List<DatoProcesoXmlBean> findByIdOrdTransferAndNroEvento(Long idOrdTransfer, Long nroEvento); // Linea 849
    int updateTransferDetProcesoRecepcionaOrdenNormalizacion(String codError, Long idOrdTransfer, Long nroEvento); // Linea 1761
}
