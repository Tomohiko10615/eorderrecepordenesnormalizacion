package com.enel.scom.eorderrecepordenesnormalizacion.dao;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.UsuarioBean;

public interface IUsuarioDao {
    UsuarioBean findByUsername(String username); // Linea 605
}
