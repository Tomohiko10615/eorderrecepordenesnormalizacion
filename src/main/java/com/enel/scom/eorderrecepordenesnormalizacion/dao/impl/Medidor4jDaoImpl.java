package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;
import org.springframework.stereotype.Repository;


import com.enel.scom.eorderrecepordenesnormalizacion.mapper.Medidor4jMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;


import com.enel.scom.eorderrecepordenesnormalizacion.bean.Medidor4jBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IMedidor4jDao;
@Repository
public class Medidor4jDaoImpl implements IMedidor4jDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(Medidor4jDaoImpl.class);
    @Override
    public List<Medidor4jBean> findByIdUbicacion(Long idUbicacion) {
        logger.info("findByNroServicio({})", idUbicacion);
        String sqlString = Medidor4jMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_OPERACION;
        List<Medidor4jBean> object = new ArrayList<>();
        try {
            object = jdbcTemplate.query(sqlString, new Medidor4jMapper(), idUbicacion);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }
}
