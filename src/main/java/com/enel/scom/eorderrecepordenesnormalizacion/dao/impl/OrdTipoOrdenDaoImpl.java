package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdTipoOrdenDao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;


@Repository
public class OrdTipoOrdenDaoImpl implements IOrdTipoOrdenDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(OrdTipoOrdenDaoImpl.class);

    @Override
    public Long getCountByCodInternoAndCodTipoOrden(String codInterno, String codTipoOrden) {
        logger.info("getCountByCodInternoAndCodTipoOrden({}, {})", codInterno, codTipoOrden);
        Long contador = 0L;
        String sqlString = ""
                + "SELECT COUNT(*)\n"
                + "FROM ORD_TIPO_ORDEN\n"
                + "WHERE 1 = 1\n"
                + "AND COD_INTERNO = ?\n"
                + "AND TRIM(COD_TIPO_ORDEN) = ?";

        try {
            contador = jdbcTemplate.queryForObject(sqlString, Long.class, codInterno, codTipoOrden);
            logger.info("Cantidad: {}.", contador);
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return contador;
    }
}
