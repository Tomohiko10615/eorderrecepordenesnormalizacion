package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.dao.ISrvElectricoDao;

@Repository
public class SrvElectricoDaoImpl implements ISrvElectricoDao {
    @Autowired
    @Qualifier("oracleJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(SrvElectricoDaoImpl.class);

    @Override
    public int updateSecMagnitudByIdServicio(Long idServicio, Long secMagnitud) {
        String sqlString = ""
                + "UPDATE \n"
                + "    SRV_ELECTRICO \n"
                + "SET \n"
                + "    SEC_MAGNITUD = ?\n"
                + "WHERE \n"
                + "    ID_SERVICIO = ?";

        int rows = 0;

        try {
            rows = jdbcTemplate.update(sqlString, secMagnitud, idServicio);
            logger.info("Metodo updateSecMagnitudByIdServicio. Filas afectadas: {}.", rows);

        } catch (Exception ex) {
            logger.error("Error al actualizar el campo srv_electrico.sec_magnitud.");
            logger.error("Exception: {}", ex.getMessage(), ex);
            throw ex;
        }
        return rows;
    }

    @Override
    public Long getSecMagnitudByIdServicio(Long idServicio) {
        logger.info("getSecMagnitudByIdServicio({})", idServicio);

        Long secMagnitud = 0L;
        String sqlString = ""
        		+ "SELECT \n"
               + " NVL(SE.SEC_MAGNITUD,0) AS SEC_MAGNITUD\n" //CAMBIO INC000109879963
               // + "SELECT \n" CAMBIO INC000109879963
             //   + "    SE.SEC_MAGNITUD\n" CAMBIO INC000109879963
                + "FROM \n"
                + "    SRV_ELECTRICO SE\n"
                + "WHERE 1 = 1\n"
                + "    AND ID_SERVICIO = ?\n";

        try {
            secMagnitud = jdbcTemplate.queryForObject(sqlString, Long.class, idServicio);
            logger.info("Sec magnitud: {}. ", secMagnitud);

        } catch (EmptyResultDataAccessException ee) {
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
            return -1L;
        } catch (DataAccessException de) {
            logger.error("Error al obtener sec_magnitud.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return secMagnitud;
    }

}
