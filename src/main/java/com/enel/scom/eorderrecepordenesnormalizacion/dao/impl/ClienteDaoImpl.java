package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.springframework.stereotype.Repository;





import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.ClienteBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IClienteDao;
@Repository
public class ClienteDaoImpl implements IClienteDao{
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LogManager.getLogger(ClienteDaoImpl.class);
    @Override
    public ClienteBean findByNroServicio(Long nroServicio) {
        logger.info("findByNroServicio({})", nroServicio);
        String sqlString = ""
        + "SELECT\n"
        + "      ID_SERVICIO\n"
        + "    , NRO_CUENTA\n"
        + "    , NRO_SERVICIO\n"
        + "    , NOMBRE\n"
        + "    , APELLIDO_PAT\n"
        + "    , APELLIDO_MAT\n"
        + "    , SRV_TIP_ELECT\n"
        + "    , COD_RUTA\n"
        + "    , TELEFONO\n"
        + "    , COD_TARIFA\n"
        + "    , LATITUD\n"
        + "    , LONGITUD\n"
        + "    , CODIGO_UBIGEO\n"
        + "    , COD_SED\n"
        + "    , TIPO_RED\n"
        + "    , SRV_POTENCIA_VALOR\n"
        + "    , SRV_TIPO_ACOMETIDA_CODIGO\n"
        + "    , COD_SET\n"
        + "    , COD_TENSION\n"
        + "    , COD_ALIMENTADOR\n"
        + "    , ESTADO_CONEXION\n"
        + "    , COD_FASE\n"
        + "    , ESTADO_CLIENTE\n"
        + "    , COD_SRV_TIP_MEDICION\n"
        + "    , CODIGO_CFT\n"
        + "    , CODIGO_AGUI\n"
        + "    , COD_CATEG_TARIFA\n"
        + "    , ID_DIRECCION\n"
        + "    , TEXTO_DIRECCION\n"
        + "    , LATITUD_SED\n"
        + "    , LONGITUD_SED\n"
        + "    , LLAVE\n"
        + "    , ID_SUC_EJECUTORA\n"
        + "    , DISTRITO\n"
        + "    , TIENE_LECTURA_REMOTA\n"
        + "    , COD_TIP_DOC_PERSONA\n"
        + "    , NRO_DOCTO_IDENT\n"
        + "    , TIPO_CLIENTE_SM\n"
        + "    , FECHA_INSERCION\n"
        + "    , USUARIO_INSERCION\n"
        + "    , FECHA_MODIFICACION\n"
        + "    , USUARIO_MODIFICACION\n"
        + "    , CAJ_TOMA\n"
        + "    , NOMBRE_SRV_ELE\n"
        + "    , ID_LISTA_VALOR_DIR\n"
        + "    , EMAIL\n"
        + "FROM\n"
        + "    SCHSCOM.CLIENTE\n"
        + "WHERE 1 = 1\n"
        + "    AND NRO_SERVICIO = ?\n"
        ;
        ClienteBean object = null;
        try {
            object = (ClienteBean) jdbcTemplate.queryForObject(sqlString, new BeanPropertyRowMapper(ClienteBean.class), nroServicio);
            logger.info("Objecto encontrado: {}.", object);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion.");
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage());
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage());
        }
        return object;
    }
}
