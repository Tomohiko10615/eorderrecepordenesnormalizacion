package com.enel.scom.eorderrecepordenesnormalizacion.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.LecturaXmlBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.ILecturaXmlDao;
import com.enel.scom.eorderrecepordenesnormalizacion.mapper.LecturaXmlMapper;

@Repository
public class LecturaXmlDaoImpl implements ILecturaXmlDao {
    @Autowired
    @Qualifier("postgresJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger logger = LogManager.getLogger(LecturaXmlDaoImpl.class);
    
    @Override
    public List<LecturaXmlBean> findByIdOrdTransferAndNroEvento(Long vIdOrdTransfer, Long nroEvento,
            String vNroMedidor, String vAccionMedidor) {
        String sqlString = LecturaXmlMapper.SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_OPERACION;
        sqlString = String.format(sqlString, vNroMedidor, vAccionMedidor);
        List<LecturaXmlBean> lista = new ArrayList<>();
        try {
            lista = jdbcTemplate.query(sqlString, new LecturaXmlMapper(), vIdOrdTransfer, nroEvento);
            logger.info("Cantidad encontrada: {}.", lista.size());
            logger.info("Datos encontrados: {}.", lista);
        } catch (EmptyResultDataAccessException ee) {
            logger.error("No se encontro informacion para los datos {} {} {} {}.", vIdOrdTransfer, nroEvento,
                    vNroMedidor, vAccionMedidor);
            logger.error("EmptyResultDataAccessException {}.", ee.getMessage(), ee);
            // return null;
        } catch (DataAccessException de) {
            logger.error("Error al obtener información.");
            logger.error("DataAccessException {}.", de.getMessage(), de);
        }

        return lista;
    }
}
