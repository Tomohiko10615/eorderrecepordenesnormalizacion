package com.enel.scom.eorderrecepordenesnormalizacion.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class ScomDataSourceJDBCConfiguration {
	
	public static final BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("POST");

	/*@Bean
	@Primary
	@ConfigurationProperties("postgres.datasource")
	public DataSourceProperties  postgresDataSourceProperties() {
		 return new DataSourceProperties();
	}	
		@Bean(name = "postgresJdbcTemplateAlternativo")
		@DependsOn("postgresDatasource")
		public JdbcTemplate jdbcTemplate(@Qualifier("postgresDatasource") DataSource dsPost) {
			return new JdbcTemplate(dsPost);
		}
	*/

	@Bean(name = "postgresDatasource") 
	@Primary
	//@ConfigurationProperties("postgres.datasource")
	public DataSource postgresDataSource() {
		
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
		 //return postgresDataSourceProperties().initializeDataSourceBuilder().build();
	}
		
	@Autowired
	DataSourceConfig dataSourcePost;
		
	@Bean(name = "postgresJdbcTemplate")
	@DependsOn("postgresDatasource")
	public JdbcTemplate jdbcTemplate(@Qualifier("connScom") Connection connScom) throws SQLException {
		connScom.setAutoCommit(false);
		return new JdbcTemplate(new SingleConnectionDataSource(connScom, true));
	}
	@Bean(name = "connScom")
	Connection registrarConnection(@Qualifier("postgresDatasource") DataSource postgresDatasource) throws SQLException {
		return postgresDatasource.getConnection();
	}
			
			
	/*@Bean
	@ConfigurationProperties("post.datasource")
	public DataSourceProperties  postgresDataSourceProperties2() {
		 return new DataSourceProperties();
	}

	@Bean(name = "postgresDatasource2") 
	@ConfigurationProperties("post.datasource")
	public DataSource postgresDataSource2() {
		 return postgresDataSourceProperties2().initializeDataSourceBuilder().build();
	}
	
	@Bean(name = "namedParameterJdbcTemplate")
	@DependsOn("postgresDatasource2")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("postgresDatasource2") DataSource dsOracle) {
		return new NamedParameterJdbcTemplate(dsOracle);
	}*/
	
	
}
