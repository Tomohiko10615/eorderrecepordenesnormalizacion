package com.enel.scom.eorderrecepordenesnormalizacion.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class SynergiaDataSourceJDBCConfiguration {

	public static final BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("ORCL");
	
/*	@Bean(name = "oracleDatasource")
	@ConfigurationProperties(prefix = "oracle.datasource")
	public DataSource oracleDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "oracleJdbcTemplate")
	public JdbcTemplate jdbcTemplate(@Qualifier("oracleDb") DataSource dsOracle) {
		return new JdbcTemplate(dsOracle);	
	} */

	
	@Bean(name = "oracleDatasource") 
	@Primary
	public DataSource postgresDataSource() {
		
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
	}
	
	@Autowired
	DataSourceConfig dataSourcePost;
	@Bean(name = "oracleJdbcTemplate")
	@DependsOn("oracleDatasource")
	public JdbcTemplate jdbcTemplate(@Qualifier("connSc4j") Connection connSc4j) throws SQLException {
		connSc4j.setAutoCommit(false);
		return new JdbcTemplate(new SingleConnectionDataSource(connSc4j, true));
	}

	@Bean(name = "connSc4j")
	Connection registrarConnection(@Qualifier("oracleDatasource") DataSource oracleDatasource) throws SQLException {
		return oracleDatasource.getConnection();
	}
	
}
