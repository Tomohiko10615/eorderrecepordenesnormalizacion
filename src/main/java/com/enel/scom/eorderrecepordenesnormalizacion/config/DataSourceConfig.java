package com.enel.scom.eorderrecepordenesnormalizacion.config;

import lombok.extern.slf4j.Slf4j;

import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;


@Slf4j
@Component
public class DataSourceConfig {

	private static String PREFIX_JAR_SCOM = "enel.wnetwork.configuration";
	private static String PREFIX_JAR_ORCL = "enel.4j.configuration";

	/*private static final String url = "jdbc:oracle:thin:@CL0APMSYND00.risorse.enel:1521:EDSCPRE";
	private static final String username = "usr_eorder";
	private static final String password = "transpre01$"; 
	private static final String driverClassName = "oracle.jdbc.OracleDriver";
	private static Connection conn;

	public static Connection getConnection() {
		if (conn != null) {
			return conn;
		}
		try {
			Class.forName(driverClassName);
			conn = DriverManager.getConnection(url, username, password);
			conn.setAutoCommit(false);
			return conn;
		} catch (ClassNotFoundException ex) {
			log.error("Error al registrar el driver de Orcl: " + ex.getMessage());
			System.exit(1);
		} catch (SQLException e) {
			log.error("Error al conectar con la base de datos de Orcl (" + url + "): " + e.getMessage());
			System.exit(1);
		}
		return null;
	}*/

	public static BeanDataSource getConnectionDatos(String tipoBaseDato)  {
		BeanDataSource deanDataSource=new BeanDataSource();
		ResourceBundle rb = ResourceBundle.getBundle("application");
		String pass="";
		Properties propiedades = new Properties();
		try {
			propiedades.load(new FileReader(rb.getString("spring.config.location")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Connection con = null;
		String prefixOldBundle = "";
		String propertyPrefix = "";
		try {
			if (tipoBaseDato.compareTo("POST") == 0) {
				 propertyPrefix = PREFIX_JAR_SCOM;
				prefixOldBundle = "InicioSesionPostgresql";
			} else if (tipoBaseDato.compareTo("ORCL") == 0) {
				 propertyPrefix = PREFIX_JAR_ORCL;
				prefixOldBundle = "InicioSesionOracle";
			}
			propertyPrefix = propertyPrefix + "." + prefixOldBundle + ".";
			Class.forName(propiedades.getProperty(propertyPrefix + "DB_DRIVER"));
			String url = propiedades.getProperty(propertyPrefix + "DB_URL");	
			pass=Desencriptar(propiedades.getProperty(propertyPrefix + "DB_PASSWORD"));
			con = DriverManager.getConnection(url, propiedades.getProperty(propertyPrefix + "DB_USER"),
					pass);
			// con.setAutoCommit(false);			
		} catch (ClassNotFoundException e) {
			log.error(String.format("Error al registrar el driver de %s: %s", tipoBaseDato, e.getMessage()));
			System.exit(1);
		} catch (SQLException e) {
			log.error(String.format("Error al conectar con la base de datos de %s: %s", tipoBaseDato, e.getMessage()));
			System.exit(1);
		} catch (Exception e) {
			log.error(String.format("Error general al conectar con la base de datos de %s: %s", tipoBaseDato, e.getMessage()));
			System.exit(1);
		}
		
		deanDataSource.setConexion(con);
		deanDataSource.setDbDriver(propiedades.getProperty(propertyPrefix + "DB_DRIVER"));
		deanDataSource.setDbURL(propiedades.getProperty(propertyPrefix + "DB_URL"));
		deanDataSource.setDbUser(propiedades.getProperty(propertyPrefix + "DB_USER"));
		deanDataSource.setDbPass(pass);
		deanDataSource.setDbMax(Integer.valueOf(propiedades.getProperty(propertyPrefix + "maximum-pool-size")));
		deanDataSource.setDbMin(Integer.valueOf(propiedades.getProperty(propertyPrefix + "minimumIdle")));
		
		return deanDataSource;
	}
	
    public static String Desencriptar(String textoEncriptado) throws Exception {
    	
    	String SECRET_KEY  = "enel";
    	String base64EncryptedString = "";

        try {
            byte[] message = Base64.getDecoder().decode(textoEncriptado.getBytes("utf-8"));        
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = cipher.doFinal(message);
            base64EncryptedString = new String(plainText, "UTF-8");
            
//			cipher.init(Cipher.ENCRYPT_MODE, key);
//			String cadenaCodificada = Base64.getEncoder().encodeToString(cipher
//					.doFinal(Base64.getDecoder().decode(Base64.getEncoder().encode("tBj!3e6VQd-5".getBytes("utf-8")))));
//			int w = 0;
        } catch (Exception ex) {
        	System.out.println(ex);
        }
        return base64EncryptedString;
    }	

}
