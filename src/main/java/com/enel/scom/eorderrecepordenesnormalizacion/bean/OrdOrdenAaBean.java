package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdOrdenAaBean {
    private String nroOrden; // varchar(240) NULL,
    private Long nroCuenta; // numeric(10) NULL,
}
