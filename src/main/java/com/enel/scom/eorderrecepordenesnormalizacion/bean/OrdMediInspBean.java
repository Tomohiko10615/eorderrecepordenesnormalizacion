package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdMediInspBean {
    private Long idOrden; // int8 NULL,
    private Long idComponente; // int8 NULL,
    private Long idEmpresa; // int8 NULL,
    private Long idContraste; // int8 NULL,
    private Long idMediInsp; // int8 NOT NULL,
    private Long idUbicacionMedidor; // int8 NULL,
    private BigDecimal cargaR; // numeric(10, 2) NULL,
    private BigDecimal cargaT; // numeric(10, 2) NULL,
    private BigDecimal cargaS; // numeric(10, 2) NULL,
}
