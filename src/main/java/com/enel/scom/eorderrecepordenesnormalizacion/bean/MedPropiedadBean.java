package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MedPropiedadBean {
    private Long id; // int8 NOT NULL,
	private String codPropiedad; // varchar(6) NULL,
	private String desPropiedad; // varchar(100) NULL,
	private String activo; // varchar(1) NULL,
	private String codInterno; // varchar(50) NULL,
}
