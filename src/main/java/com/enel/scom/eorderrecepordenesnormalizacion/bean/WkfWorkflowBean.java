package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WkfWorkflowBean {
	// CREATE TABLE schscom.wkf_workflow (
	private Long id; // int8 NOT NULL,
	private String idDescriptor; // varchar(40) NULL,
	private Long version; // numeric NULL,
	private String idState; // varchar(40) NULL,
	private String hasParent; // varchar(1) NULL,
	private String idOldState; // varchar(40) NULL,
	private String parentType; // varchar(250) NULL,
	private Date fechaEstado; // timestamp NULL,
}
