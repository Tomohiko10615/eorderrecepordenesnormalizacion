package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdMediCambioBean {
    private Long idOrden; // int8 NULL,
	private Long idComponente; // int8 NULL,
	private Long idEmpresa; // int8 NULL,
	private String accion; // varchar(50) NULL,
	private Long idContraste; // int8 NULL,
	private Long idMediCambio; // int8 NOT NULL,
}
