package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Data;

@Data
public class MedMarcaBean {
    private Long id; // int8 NOT NULL,
    private Long idTipComponente; // int8 NULL,
    private String codMarca; // varchar(15) NULL,
    private String desMarca; // varchar(100) NULL,
    private String activo; // varchar(1) NULL,
    private Long idUsuarioRegistro; // int8 NULL,
    private Date fecRegistro; // timestamp NULL,
    private Long idUsuarioModif; // int8 NULL,
    private Date fecModif; // timestamp NULL,
}
