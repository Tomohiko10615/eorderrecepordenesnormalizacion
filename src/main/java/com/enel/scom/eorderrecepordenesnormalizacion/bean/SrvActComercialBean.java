package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SrvActComercialBean {
    private Long idActComercial; // id_act_comercial int8 NOT NULL,
    private Long idEmpresa; // id_empresa int8 NULL,
    private String codActComercial; // cod_act_comercial varchar(6) NULL,
    private String desActComercial; // des_act_comercial varchar(100) NULL,
    private String activo; // activo varchar(1) NULL DEFAULT 'S'::character varying,
    private String codInterno; // cod_interno varchar(50) NULL,
}
