package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FwkAuditeventBean {
    private Long id; // int8 NOT NULL,
    private String usecase; // varchar(255) NULL,
    private String objectref; // varchar(500) NULL,
    private Long idFk; // int8 NULL,
    private Date fechaEjecucion; // timestamp NULL,
    private String specificAuditevent; // varchar(500) NULL,
    private Long idUser; // int8 NULL,
    private Long idEmpresa; // int8 NULL,
}
