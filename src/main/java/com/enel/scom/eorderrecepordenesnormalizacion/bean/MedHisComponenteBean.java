package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class MedHisComponenteBean {
    private Long idHisComponente; // int8 NOT NULL,
	private Long idComponente; // int8 NULL,
	private Long idEstComponente; // int8 NULL,
	private Date fecDesde; // timestamp NULL,
	private Date fecHasta; // timestamp NULL,
	private Long idUbicacion; // int8 NULL,
	private String typeUbicacion; // varchar(500) NULL,
	private Long idOrden; // int8 NULL,
}
