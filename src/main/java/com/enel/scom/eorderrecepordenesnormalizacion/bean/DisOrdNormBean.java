package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DisOrdNormBean {
	// CREATE TABLE schscom.dis_ord_norm (
	private Long idOrden; // int8 NOT NULL,
	private Long idOrdInsp; // int8 NULL,
	private Long idEjecutorAsig; // int8 NULL,
	private Date fecDevolucPrev; // timestamp NULL,
	private Date fecEnvio; // timestamp NULL,
	private Long idEjecutorEje; // int8 NULL,
	private Date fecHoraIniEje; // timestamp NULL,
	private Date fecHoraFinEje; // timestamp NULL,
	private Long idResultado; // int8 NULL,
	private Long nroOtFisica; // numeric(10) NULL,
	private Long idNotificado; // int8 NULL,
	private String lugarNotificado; // varchar(250) NULL,
	private Long idParentNotificado; // int8 NULL,
	private Long idGiroNotificado; // int8 NULL,
	private String subtipoOrd; // varchar(100) NULL,
	private Date fecNormalizacion; // timestamp NULL,
	private Long idActComNotif; // int8 NULL,
	private Date fechaEmision; // timestamp NULL,
	private Long idSubtipo; // int8 NULL,
}
