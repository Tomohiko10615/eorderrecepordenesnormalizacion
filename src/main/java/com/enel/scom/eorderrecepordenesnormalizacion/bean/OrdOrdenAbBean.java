package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdOrdenAbBean {
    private Long idOrden;
    private Long idWorkflow;
    private String idState;
    private Long idWorkflowSeg;
    private Long idServicio;
    private Long nroServicio;
    private Long idOrdInsp;
}
