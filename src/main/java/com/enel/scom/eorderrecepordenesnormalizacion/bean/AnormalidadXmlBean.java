package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AnormalidadXmlBean {
    private String CODIGOANORMALIDAD;
}
    