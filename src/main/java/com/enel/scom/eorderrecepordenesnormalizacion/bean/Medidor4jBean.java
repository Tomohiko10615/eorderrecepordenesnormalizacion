package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Medidor4jBean {
    private String nroComponente; //[1024];
	private String codMarca; //[1024];   /* ANL_20200909 */
	private String codModelo; //[1024];  /* ANL_20200909 */
}
