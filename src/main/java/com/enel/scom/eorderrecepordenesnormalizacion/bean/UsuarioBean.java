package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.sql.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UsuarioBean {
    private Long id; // int8 NOT NULL,
	private String username; // varchar(50) NULL,
	private Date fecha_alta; // timestamp NULL,
	private Date fecha_baja; // timestamp NULL,
	private Date fecha_ultimo_ingreso; // timestamp NULL,
	private Long id_perfil; // int8 NULL,
	private boolean activo; // bool NULL,
}
