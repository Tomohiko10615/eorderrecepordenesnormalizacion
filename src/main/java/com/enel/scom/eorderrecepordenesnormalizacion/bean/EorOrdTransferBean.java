package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EorOrdTransferBean {

	private Long idOrdTransfer; // int8 NOT NULL,
	private String codTipoOrdenLegacy; // varchar(6) NULL,
	private String nroOrdenLegacy; // varchar(240) NULL,
	private String codTipoOrdenEorder; // varchar(6) NULL,
	private Long nroOrdenEorder; // numeric(18) NULL,
	private Long codEstadoOrden; // numeric(2) NULL,
	private Long codEstadoOrdenAnt; // numeric(2) NULL,
	private Long nroEnvios; // numeric(3) NULL,
	private Long nroRecepciones; // numeric(3) NULL,
	private String suspendida; // varchar(1) NULL DEFAULT 'N'::character varying,
	private String nroOrdenLgcRelac; // varchar(240) NULL,
	private Long codCumplimentacion; // numeric(20) NULL,
	private Date fecOperacion; // timestamp NULL,
	private String codOperacion; // varchar(30) NULL,
	private Long nroAnulaciones; // numeric(3) NULL,
	private String tieneAnexos; // varchar(1) NULL,
	private Long codEstadoAnexo; // numeric(2) NULL,
	private String nomArchAnexo; // varchar(100) NULL,
	private String observaciones; // varchar(300) NULL,
	private Long flagEnvio; // numeric(1) NULL,
	private String generacion; // varchar(1) NULL DEFAULT 'A'::character varying,
	private Date fecEstado; // timestamp NULL,
	private Long nroCuenta; // numeric(10) NULL,
	private String creadaEorder; // varchar(1) NULL DEFAULT '0'::character varying,
	private Long idEmpresa; // int8 NULL,
	private Date fechaCreacion; // timestamp NULL DEFAULT CURRENTTIMESTAMP,
}
