package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EorInterfazCnrBean {
    private Long idOrdInterfcnr; // int8 NOT NULL,
    private Long numeroServicio; // numeric(10) NULL,
    private Long numeroCuenta; // numeric(10) NULL,
    private Long numeroExpediente; // numeric(10) NULL,
    private Long numeroInspeccion; // numeric(10) NULL,
    private String tipoOrden; // varchar(20) NULL,
    private String tipoRestriccion; // varchar(20) NULL,
    private String tareaInspeccion; // varchar(20) NULL,
    private String ficticio; // varchar(1) NULL,
    private Date fechaRegistro; // timestamp NULL,
    private Long codEstadoOrden; // numeric(2) NULL,
    private Long codEstadoOrdenAnt; // numeric(2) NULL,
    private Date fecEstado; // timestamp NULL,
    private String codOperacion; // varchar(30) NULL,
    private String motivo; // varchar(20) NULL,
    private String subTipoOrden; // varchar(20) NULL,
    private String observaciones; // varchar(300) NULL,
    private Date fecOperacion; // timestamp NULL
}
