package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Data;

@Data
public class ComCuadrillaBean {
    private Long idCuadrilla; // id_cuadrilla int8 NOT NULL,
    private String codCuadrilla; // cod_cuadrilla varchar(6) NULL,
    private String desCuadrilla; // des_cuadrilla varchar(100) NULL,
    private Long idContratista; // id_contratista int8 NULL,
    private Long idCoordCuadrilla; // id_coord_cuadrilla int8 NULL,
    private Long idActCuadrilla; // id_act_cuadrilla int8 NULL,
    private Long idGrupoCuadrilla; // id_grupo_cuadrilla int8 NULL,
    private Long cantMaxOp; // cant_max_op int8 NULL,
    private Date fechaCreac; // fecha_creac timestamp NULL,
    private String idNivelCri; // id_nivel_cri varchar(50) NULL,
    private String active; // active varchar(1) NULL DEFAULT 'S'::character varying,
    private Long idContratocnt; // id_contratocnt int8 NULL,
}
