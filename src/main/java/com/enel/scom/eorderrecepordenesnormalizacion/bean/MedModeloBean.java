package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.text.DecimalFormat;
import java.util.Date;

import lombok.Data;
@Data
public class MedModeloBean {
    private Long id; // int8 NOT NULL,
    private Long idMarca; // int8 NULL,
    private String codModelo; // varchar(6) NULL,
    private String desModelo; // varchar(100) NULL,
    private Long idTipModelo; // int8 NULL,
    private Date fecCreacion; // timestamp NULL,
    private Long cantAnosVida; // numeric(4) NULL,
    private Long cantAnosAlmacen; // numeric(4) NULL,
    private Long nroSellosCert; // numeric(4) NULL,
    private Long nroSellosInst; // numeric(4) NULL,
    private Long idFase; // int8 NULL,
    private Long idTipMedicion; // int8 NULL,
    private Long idVoltaje; // int8 NULL,
    private Long idTension; // int8 NULL,
    private Long idTecnologia; // int8 NULL,
    private Long idTipRegistrador; // int8 NULL,
    private Long nroRegistrador; // numeric(10) NULL,
    private String esReacondicionado; // varchar(1) NULL,
    private String esReseteado; // varchar(1) NULL,
    private String esPatron; // varchar(1) NULL,
    private String esTotalizador; // varchar(1) NULL,
    private Double exactitud; // numeric(3, 2) NULL,
    private Double constante; // numeric(10, 3) NULL,
    private Long idAmperaje; // int8 NULL,
    private Long idUnidadMedidaCte; // int8 NULL,
    private Long cantHilos; // numeric(10) NULL,
    private String activo; // varchar(1) NULL DEFAULT 'S'::character varying,
    private Long idDynamicobject; // numeric(18) NULL,
    private String discriminador; // varchar(20) NULL DEFAULT 'MODELOMEDIDOR'::character varying,
    private Long idClasePrecision; // numeric(18) NULL,
    private String perfilCarga; // varchar(1) NULL DEFAULT 'N'::character varying,
    private String relojIncorporado; // varchar(1) NULL DEFAULT 'N'::character varying,
    private String requiereProg; // varchar(1) NULL DEFAULT 'N'::character varying,
    private Long idUsuarioRegistro; // int8 NULL,
    private Date fecRegistro; // timestamp NULL,
    private Long idUsuarioModif; // int8 NULL,
    private Date fecModif; // timestamp NULL,
    private String nroSellosBornera; // varchar(50) NULL,
    private String nroSellosMedidor; // varchar(50) NULL,
    private String claseMedidor; // varchar(50) NULL,
}
