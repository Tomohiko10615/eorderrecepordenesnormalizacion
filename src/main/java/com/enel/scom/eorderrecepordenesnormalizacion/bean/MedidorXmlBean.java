package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MedidorXmlBean {
    private String NUMEROMEDIDOR; // [1024];
    private String MARCAMEDIDOR; // [1024];
    private String MODELOMEDIDOR; // [1024];
    private String NUMEROFABRICAMEDIDOR; // [1024];
    private String TIPOMEDIDOR; // [1024];
    private String TECNOLOGIAMEDIDOR; // [1024];
    private String FASEMEDIDOR; // [1024];
    private String FACTORMEDIDOR; // [1024];
    private String ACCIONMEDIDOR; // [1024];
    private String TIPOMEDICION; // [1024];
    private String UBICACIONMEDIDOR; // [1024];
    private String TELEMEDIDO; // [1024];
    private String ELEMENTOBORNEROPRUEBA; // [1024];
    private String ANOFABRICACION; // [1024];
    private String PROPIEDADMEDIDOR; // [50];
    private String fechaInstalacion; // [50];
}
