package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SelloXmlBean {
    private String NUMEROSELLO;
    private String UBICACIONSELLO;
    private String SERIESELLO;
    private String TIPOSELLO;
    private String COLORSELLO;
    private String ESTADOSELLO;
    private String ACCIONSELLO;
    private String DESTINOSELLO;
    private String MEDIDORASOCIADO;
}
