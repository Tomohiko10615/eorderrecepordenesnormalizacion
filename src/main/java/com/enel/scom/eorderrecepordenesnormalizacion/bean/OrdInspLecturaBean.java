package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdInspLecturaBean {
    private Long idInspLectura; // int8 NOT NULL,
	private Long idEmpresa; // int8 NULL,
	private Long idMediInsp; // int8 NULL,
	private Long idLectura; // int8 NULL,
}
