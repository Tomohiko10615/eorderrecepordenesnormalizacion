package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DisOrdTareaBean {
    private Long id; // int8 NOT NULL,
    private Long idOrden; // int8 NULL,
    private Long idAnormalidad; // int8 NULL,
    private Long idTarea; // int8 NULL,
    private String tareaEstado; // varchar(1) NULL,
    private Long idEmpresa; // varchar(1) NULL,
}
