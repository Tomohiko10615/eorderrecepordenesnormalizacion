package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class OrdObservacionBean {
    private Long idObservacion; // int8 NOT NULL,
    private Long idOrden; // int8 NULL,
    private String texto; // varchar(1200) NULL,
    private Date fechaObservacion; // timestamp NULL,
    private Long idUsuario; // int8 NULL,
    private String stateName; // varchar(20) NULL,
    private String discriminator; // varchar(20) NULL,
    private Long idEmpresa;
}
