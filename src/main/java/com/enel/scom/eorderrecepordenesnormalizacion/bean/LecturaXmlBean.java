package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LecturaXmlBean {
    private String TIPOLECTURA; //[1024];
    private String HORARIOLECTURA; //[50];
    private String FECHALECTURA; //[1024];
    private String ESTADOLEIDO; //[12];
}
