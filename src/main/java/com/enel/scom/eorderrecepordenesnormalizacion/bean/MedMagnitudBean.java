package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MedMagnitudBean {
    private Long idMagnitud; // NUMBER(18) NOT NULL,
    private Long idEmpresa; // NUMBER(18) NULL,
    private Long idComponente; // NUMBER(18) NULL,
    private Long idServicio; // NUMBER(18) NULL,
    private Long idMedida; // NUMBER(18) NULL,
    private Long enteros; // NUMBER(4) NULL,
    private Long decimales; // NUMBER(4) NULL,
    private BigDecimal factor; // NUMBER(10,3) NULL,
    private BigDecimal valor; // NUMBER(16,4) NULL,
    private String tipo; // VARCHAR2(50 BYTE) NULL,
    private Long idTipMagnitud; // NUMBER(18) NULL,
    private String observaciones; // VARCHAR2(100 BYTE) NULL,
    private String codLecturista; // VARCHAR2(6 BYTE) NULL,
    private String codObservacion; // VARCHAR2(6 BYTE) NULL,
    private String codSupervisor; // VARCHAR2(6 BYTE) NULL,
    private Long idSitTerInfo; // NUMBER(18) NULL,
    private Date fecLecTerreno; // DATE NULL,
    private Long secMagnitud; // NUMBER(10) NULL,
    private Long idEstMagnitud; // NUMBER(18) NULL,
    private Long idEstacionalidad; // NUMBER(18) NULL,
    private Long idTipCalculo; // NUMBER(18) NULL,
    private Date fechaLectIni; // DATE NULL,
    private String leidoDesde; // VARCHAR2(3 BYTE) NULL,
    private Long idTipoCons; // NUMBER(18) NULL,
    private Long idSupervisor; // NUMBER(18) NULL,
    private Long idLecturista; // NUMBER(18) NULL
}
