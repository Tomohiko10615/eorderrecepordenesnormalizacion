package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class MedidorMedidaDatoBean {
    private String tieneMedidaEnfp;
    private String tieneMedidaEnhp;
    private String tieneMedidaDmfp;
    private String tieneMedidaDmhp;
    private String tieneMedidaReac;
    private Long idMedida1;
    private Long cantEnteros1;
    private Long cantDecimales1;
    private BigDecimal valFactor1;
    private Long idTipCalculo1;
    private Long idMedida2;
    private Long cantEnteros2;
    private Long cantDecimales2;
    private BigDecimal valFactor2;
    private Long idTipCalculo2;
    private Long idMedida3;
    private Long cantEnteros3;
    private Long cantDecimales3;
    private BigDecimal valFactor3;
    private Long idTipCalculo3;
    private Long idMedida4;
    private Long cantEnteros4;
    private Long cantDecimales4;
    private BigDecimal valFactor4;
    private Long idTipCalculo4;
    private Long idMedida5;
    private Long cantEnteros5;
    private Long cantDecimales5;
    private BigDecimal valFactor5;
    private Long idTipCalculo5;
    private String propiedad;
}
