package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdOrdenBean {
	// CREATE TABLE schscom.ord_orden (
	private Long id; // int8 NOT NULL,
	private Long idOrdenSc4j; // int8 NULL,
	private Long idWorkflow; // int8 NULL,
	private Long idTipoOrden; // int8 NULL,
	private String nroOrden; // varchar(240) NULL,
	private Long idBuzon; // int8 NULL,
	private Date fechaCreacion; // timestamp NULL,
	private String discriminador; // varchar(20) NULL,
	private Long idUsuarioCreador; // int8 NULL,
	private Long idServicio; // int8 NULL,
	private Date fechaFinalizacion; // timestamp NULL,
	private String leido; // varchar(1) NULL,
	private Date fechaIngresoEstadoActual; // timestamp NULL,
	private Long idMotivoAnulacion; // int8 NULL,
	private Long idMotivo; // int8 NULL,
	private Date fechaVencimiento; // timestamp NULL,
	private Date fechaVencLegal; // timestamp NULL,
	private Long idUsuarioRegistro; // int8 NULL,
	private Date fecRegistro; // timestamp NULL,
	private Long idUsuarioModif; // int8 NULL,
	private Date fecModif; // timestamp NULL,
	private Boolean activo; // bool NULL,
}
