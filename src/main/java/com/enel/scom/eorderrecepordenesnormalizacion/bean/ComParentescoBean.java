package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Data;

@Data
public class ComParentescoBean {
    private Long idParentesco; // id_parentesco int8 NOT NULL,
    private Long idEmpresa; // id_empresa int8 NULL,
    private String codParentesco; // cod_parentesco varchar(20) NULL,
    private String desParentesco; // des_parentesco varchar(100) NULL,
    private String activo; // activo varchar(1) NULL DEFAULT 'S'::character varying,
}
