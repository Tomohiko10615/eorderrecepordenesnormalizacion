package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Data;

@Data
public class ComContratistaBean {
    private Long id; // int8 NOT NULL,
	private String cod_contratista; // varchar(6) NULL,
	private Long id_persona; // int8 NULL,
	private String activo; // varchar(1) NULL,
}
