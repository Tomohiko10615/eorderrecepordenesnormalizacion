package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DisResultadoInspBean {
    private Long id;
    private String codResultado;
    private String desResultado;
    private String activo;
    private String codInterno;
}
