package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OperacionXmlBean {
    private String CODIGO_OPERACION; //[1024];
    private String CODIGO_RESULTADO; //[1024];
    private String CODIGO_CAUSAL_RESULTADO; //[1024];
    private String CODIGO_NOTA_CODIFICADA; //[1024];
    private String INICIO_OPERACION; //[1024];
    private String FIN_OPERACION; //[1024];
    private String DURACION_OPERACION; //[1024];
    private String NOTAS_OPERACION; //[301]; /* TRC 06/04/2015 */
}
