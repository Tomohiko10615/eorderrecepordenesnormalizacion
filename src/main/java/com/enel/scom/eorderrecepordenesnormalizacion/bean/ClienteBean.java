package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class ClienteBean {
    // CREATE TABLE schscom.cliente (
        private Long idServicio; // int8 NOT NULL,
        private Long nroCuenta; // numeric(10) NULL,
        private Long nroServicio; // numeric(10) NULL,
        private String nombre; // varchar(255) NULL,
        private String apellidoPat; // varchar(50) NULL,
        private String apellidoMat; // varchar(50) NULL,
        private String srvTipElect; // varchar(1) NULL,
        private String codRuta; // varchar(100) NULL,
        private String telefono; // varchar(200) NULL,
        private String codTarifa; // varchar(10) NULL,
        private BigDecimal latitud; // numeric(18, 8) NULL,
        private BigDecimal longitud; // numeric(18, 8) NULL,
        private String codigoUbigeo; // varchar(6) NULL,
        private String codSed; // varchar(6) NULL,
        private String tipoRed; // varchar(15) NULL,
        private BigDecimal srvPotenciaValor; // numeric(16, 4) NULL,
        private String srvTipoAcometidaCodigo; // varchar(6) NULL,
        private String codSet; // varchar(6) NULL,
        private String codTension; // varchar(6) NULL,
        private String codAlimentador; // varchar(6) NULL,
        private String estadoConexion; // varchar(40) NULL,
        private String codFase; // varchar(6) NULL,
        private String estadoCliente; // varchar(50) NULL,
        private String codSrvTipMedicion; // varchar(20) NULL,
        private String codigoCft; // varchar(50) NULL,
        private String codigoAgui; // varchar(18) NULL,
        private String codCategTarifa; // varchar(100) NULL,
        private Long idDireccion; // int8 NULL,
        private String textoDireccion; // varchar(1000) NULL,
        private BigDecimal latitudSed; // numeric(18, 8) NULL,
        private BigDecimal longitudSed; // numeric(18, 8) NULL,
        private String llave; // varchar(50) NULL,
        private Long idSucEjecutora; // int8 NULL,
        private String distrito; // varchar(1000) NULL,
        private String tieneLecturaRemota; // varchar(1) NULL,
        private String codTipDocPersona; // varchar(6) NULL,
        private String nroDoctoIdent; // varchar(15) NULL,
        private String tipoClienteSm; // varchar(10) NULL,
        private Date fechaInsercion; // timestamp NULL,
        private String usuarioInsercion; // varchar(50) NULL,
        private Date fechaModificacion; // timestamp NULL,
        private String usuarioModificacion; // varchar(50) NULL,
        private String cajToma; // varchar(50) NULL,
        private String nombreSrvEle; // varchar(50) NULL,
        private Long idListaValorDir; // int8 NULL,
        private String email; // varchar(200) NULL
}
