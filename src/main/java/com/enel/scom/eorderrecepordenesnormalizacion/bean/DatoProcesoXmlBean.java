package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DatoProcesoXmlBean {
    private String LLAVE_SECRETA; //[1024];
    private String CODIGO_DISTRIBUIDORA; //[1024];
    private String CODIGO_SISTEMA_EXTERNO_ORIGEN; //[1024];
    private String CODIGO_TIPO_DE_TDC; //[1024];
    private String CODIGO_EXTERNO_DEL_TDC; //[1024];
    private String CODIGO_INTERNO_DEL_TDC; //[1024];
    private String CODIGO_PROCESO; //[1024];
    private String CODIGO_SUBPROCESO; //[1024];
    private String TDC_CREADO_EN_EORDER; //[1024];
    private String CODIGO_RESULTADO; //[1024];
    private String CODIGO_CAUSAL_RESULTADO; //[7];
    private String CODIGO_NOTA_CODIFICADA; //[1024];
    private String INICIO_TDC; //[21];
    private String FIN_TDC; //[21];
    private String DURACION_EJECUCION; //[1024];
    private String CODIGO_CUADRILLA; //[7];
    private String PLACA_VEHICULO; //[1024];
    private String NOMBRE_USUARIO; //[1024];
    private String APELLIDO_USUARIO; //[1024];
    private String MATRICULA_USUARIO; //[1024];
    private String LATITUD_MEDIDOR; //[1024];
    private String LONGITUD_MEDIDOR; //[1024];
    private String LATITUD_EMPALME; //[1024];
    private String LONGITUD_EMPALME; //[1024];
    private String FECHA_HORA_INICIO_CITA; //[1024];
    private String FECHA_HORA_FIN_CITA; //[1024];
    private String NOMBRE_PERSONA_CONTACTO_CITA; //[1024];
    private String TELEFONO_PERSONA_CONTACTO_CITA; //[1024];
}
