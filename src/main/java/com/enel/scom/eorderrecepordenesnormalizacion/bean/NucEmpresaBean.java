package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NucEmpresaBean {
    private Long id_empresa; // int8 NOT NULL,
	private String tiene_datos; // varchar(1) NULL,
	private String tiene_usuarios; // varchar(1) NULL,
	private String es_global; // varchar(1) NULL,
	private String cod_partition; // varchar(6) NULL,
	private String des_partition; // varchar(100) NULL,
	private String name_application; // varchar(50) NULL,
}
