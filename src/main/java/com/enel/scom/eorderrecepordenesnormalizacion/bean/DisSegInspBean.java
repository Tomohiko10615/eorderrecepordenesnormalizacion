package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Data;

@Data
public class DisSegInspBean {
    // CREATE TABLE schscom.dis_seg_insp (
	private Long id; // int8 NOT NULL,
	private Long idWkf; // int8 NULL,
}
