package com.enel.scom.eorderrecepordenesnormalizacion.bean;


import java.util.Date;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class ComParametroBean {
	




	private Long id_parametro; // int8 NOT NULL,
	private String sistema; // varchar(20) NULL,
	private String entidad; // varchar(20) NULL,
	private String codigo; // varchar(10) NULL,
	private String descripcion; // varchar(100) NULL,
	private Long valor_num; // numeric(18, 2) NULL,
	private String valor_alf; // varchar(250) NULL,
	private String activo; // varchar(1) NULL,
	private Date fecha_act; // timestamp NULL,
	private Date fecha_desac; // timestamp NULL,
}
