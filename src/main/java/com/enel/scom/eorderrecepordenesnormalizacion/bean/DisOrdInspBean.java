package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Data;

@Data
public class DisOrdInspBean {
    // CREATE TABLE schscom.dis_ord_insp (
    private Long id; // int8 NOT NULL,
    private Long idOrden; // int8 NULL,
    private Long idLibroInsp; // int8 NULL,
    private String tipInspeccion; // varchar(50) NULL,
    private String nomReferencia; // varchar(60) NULL,
    private String dirReferencia; // varchar(250) NULL,
    private String entreCalles; // varchar(200) NULL,
    private Long idParenContacto; // int8 NULL,
    private Long nroAtencionEmer; // numeric(15) NULL,
    private Date fecAtencionEmer; // timestamp NULL,
    private Long idProducto; // int8 NULL,
    private Long idJefeProducto; // int8 NULL,
    private Long idCoordCuadrilla; // int8 NULL,
    private Date fecDevolucPrev; // timestamp NULL,
    private Date fecEnvio; // timestamp NULL,
    private Long idCuadrilla; // int8 NULL,
    private Long idEjecutor; // int8 NULL,
    private Date fecIniEje; // timestamp NULL,
    private Date fecFinEje; // timestamp NULL,
    private Long idResultado; // int8 NULL,
    private Long idEstadoSeguimiento; // int8 NULL,
    private Long idComunaReferencia; // int8 NULL,
    private String procedeACobro; // varchar(1) NULL,
    private String conInfoCompleta; // varchar(1) NULL,
    private Long idSeginsp; // int8 NULL,
    private String autoGenerada; // varchar(1) NULL,
    private Long idSedAsoc; // int8 NULL,
    private String prioridad; // varchar(1) NULL,
    private Long idMotivoNoCobro; // int8 NULL,
    private Long idContratista; // int8 NULL,
    private Long nroNotificacion; // int8 NULL,
}
