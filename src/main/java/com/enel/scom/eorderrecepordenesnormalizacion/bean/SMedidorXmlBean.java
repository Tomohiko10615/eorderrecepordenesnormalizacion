package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SMedidorXmlBean {
    private String  cNUMEROMEDIDOR; //[1024];
    private String  cMARCAMEDIDOR; //[1024];
    private String  cMODELOMEDIDOR; //[1024];
    private String  cNUMEROFABRICAMEDIDOR; //[1024];
    private String  cTIPOMEDIDOR; //[1024];
    private String cFACTOR;
    
    
    
    private String  cACCIONMEDIDOR; //[1024];
    
    private String  cUBICACIONMEDIDOR; //[1024];
    
    
    
    private String  cPROPIEDADMEDIDOR; //[50];
    private String  cLectENFP; //[12];
    private String  cLectENHP; //[12];
    private String  cLectDMFP; //[12];
    private String  cLectDMHP; //[12];
    private String  cLectEnRe; //[12];
    private String fechaInstalacion; //[50];
}
