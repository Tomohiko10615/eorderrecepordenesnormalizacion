package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GpNormalizacionXmlBean {

    private String cCod_Contratista; //[20];
    private String NroOrdenInspeccion; //[20];
    private String TieneCambioMed; //[50];
    private String CodActComercial; //[50];
    private String DetalleInspeccion; //[50];
    private String NroNotificacion; //[20];
}
