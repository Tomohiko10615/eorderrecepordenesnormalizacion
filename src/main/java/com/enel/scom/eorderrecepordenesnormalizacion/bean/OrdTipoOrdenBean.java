package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import lombok.Data;

@Data
public class OrdTipoOrdenBean {

	// CREATE TABLE schscom.ord_tipo_orden (
	private Long id; // int8 NOT NULL,
	private String descriptor; // varchar(40) NULL,
	private Long idMotivoDefault; // int8 NULL,
	private String desTipoOrden; // varchar(50) NULL,
	private String codInterno; // varchar(50) NULL,
	private String discriminador; // varchar(20) NULL,
	private String codTipoOrden; // varchar(6) NULL,
	private String servicioAsociado; // varchar(1) NULL,
	private String ordenclassname; // varchar(150) NULL,
	private String codTipoProceso; // varchar(20) NULL,
}
