package com.enel.scom.eorderrecepordenesnormalizacion.bean;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EorOrdTransferDetBean {

	private Long id_ord_transfer_det; // int8 NOT NULL,
	private Long id_ord_transfer; // int8 NULL,
	private String accion; // varchar(10) NULL,
	private Long nro_evento; // numeric(3) NULL,
	// private String reg_xml; // xml NULL,
	private Date fec_accion; // timestamp NULL,
	private String cod_error; // varchar(30) NULL,
	private Long id_usuario; // int8 NULL,
	private Long id_empresa; // int8 NULL,
	
}
