package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class FwkAuditeventMapper {

	public static final String SQLPOSTGRESQL_INSERT_INTO_FWK_AUDITEVENT =
			"insert into fwk_auditevent "
			+ "(id "
			+ ",usecase "
			+ ",objectref "
			+ ",id_fk "
			+ ",fecha_ejecucion "
			+ ",specific_auditevent "
			+ ",id_user "
			+ ")"
			+ "values "
			+ "         (NEXTVAL('SQAUDITEVENT') "
			+ "         ,'RecepMasivaNormalizacion(SE:'|| ? || ')' "
			+ "         ,'com.synapsis.synergia.dis.domain.OrdenNormalizacion' "
			+ "         ,? "
			+ "         ,now() "
			+ "         ,'ORD' "
			+ "         ,?"
			+ "         )";
}
