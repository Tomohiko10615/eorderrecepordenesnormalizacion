package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.GpNormalizacionXmlBean;

public class GpNormalizacionXmlMapper implements RowMapper<GpNormalizacionXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR = ""
    + "SELECT\n"
    + "      UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/codigo_tdc_inspeccion_asociada/text()', D.REG_XML))::TEXT AS NROORDENINSPECCION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/se_cambio_el_medidor/text()', D.REG_XML))::TEXT AS TIENECAMBIOMEDIDOR\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/Giro_de_negocio_encontrado/text()', D.REG_XML))::TEXT AS CODACTCOMERCIAL\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/detalle_inspeccion/text()', D.REG_XML))::TEXT AS DETALLEINSPECCION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/nro_notificacion_inspeccion/text()', D.REG_XML))::TEXT AS NRONOTIFICACION\n"
    + "FROM\n"
    + "    EOR_ORD_TRANSFER_DET D\n"
    + "WHERE 1 = 1\n"
    + "    AND D.ID_ORD_TRANSFER = ?\n"
    + "    AND D.ACCION ='RECEPCION'\n"
    + "    AND D.NRO_EVENTO = ?\n"
    + "    AND D.FEC_ACCION IN (SELECT C.FEC_OPERACION FROM EOR_ORD_TRANSFER C WHERE D.ID_ORD_TRANSFER =C.ID_ORD_TRANSFER)\n"
    + ";";

    @Override
    @Nullable
    public GpNormalizacionXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        GpNormalizacionXmlBean object = GpNormalizacionXmlBean.builder().build();
        
        object.setNroOrdenInspeccion(rs.getString("NROORDENINSPECCION"));
        object.setTieneCambioMed(rs.getString("TIENECAMBIOMEDIDOR"));
        object.setCodActComercial(rs.getString("CODACTCOMERCIAL"));
        object.setDetalleInspeccion(rs.getString("DETALLEINSPECCION"));
        object.setNroNotificacion(rs.getString("NRONOTIFICACION"));
        

       
		return object;
    }
}
