package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class DisAnormalidadMapper {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_ANORMALIDAD =
			"SELECT id "
			+ "FROM dis_anormalidad "
			+ "WHERE cod_anormalidad = ?";
}
