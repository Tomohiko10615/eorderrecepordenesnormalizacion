package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComParametroBean;

public class ComParametroMapper implements RowMapper<ComParametroBean>{
	public static final String SQLPOSTGRESQL_SELECT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO = ""
	+ "SELECT \n"
	+ "      VALOR_ALF AS PARAMETRO_ALF\n"
	+ "    , VALOR_NUM AS PARAMETRO_NUM\n"
	+ "    , DESCRIPCION AS DESCRIPCION\n"
	+ "FROM\n"
	+ "    SCHSCOM.COM_PARAMETROS\n"
	+ "WHERE 1 = 1\n"
	+ "    AND SISTEMA = ?\n"
	+ "    AND ENTIDAD = ?\n"
	+ "    AND CODIGO = ?";

	public static final String SQLPOSTGRESQL_SELECT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO_ACTIVO = ""
	+ "SELECT \n"
	+ "      VALOR_ALF AS PARAMETRO_ALF\n"
	+ "    , VALOR_NUM AS PARAMETRO_NUM\n"
	+ "    , DESCRIPCION AS DESCRIPCION\n"
	+ "FROM\n"
	+ "    SCHSCOM.COM_PARAMETROS\n"
	+ "WHERE 1 = 1\n"
	+ "    AND SISTEMA = ?\n"
	+ "    AND ENTIDAD = ?\n"
	+ "    AND CODIGO = ?\n"
	+ "    AND ACTIVO = ?";

	public static final String SQLPOSTGRESQL_COUNT_FOR_PARAMETRO_BY_SISTEMA_ENTIDAD_CODIGO_VALORALF = ""
	+ "SELECT COUNT(VALOR_ALF)\n"
	+ "FROM\n"
	+ "    SCHSCOM.COM_PARAMETROS\n"
	+ "WHERE 1 = 1\n"
	+ "    AND SISTEMA = ?\n"
	+ "    AND ENTIDAD = ?\n"
	+ "    AND CODIGO = ?\n"
	+ "    AND TRIM(VALOR_ALF)= TRIM(?)";

	@Override
	@Nullable
	public ComParametroBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ComParametroBean parametroAux = ComParametroBean.builder().build();
		parametroAux.setValor_alf(rs.getString("PARAMETRO_ALF"));
		parametroAux.setValor_num(rs.getLong("PARAMETRO_NUM"));
		parametroAux.setDescripcion(rs.getString("DESCRIPCION"));
		return parametroAux;
	}

}
