package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.AnormalidadXmlBean;

public class AnormalidadXmlMapper implements RowMapper<AnormalidadXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT= ""
    + "SELECT \n"
    + "    A.COD_ANORMALIDAD\n"
    + "FROM \n"
    + "      DIS_ORD_TAREA  D\n"
    + "    , DIS_ANORMALIDAD A\n"
    + "WHERE 1 = 1 \n"
    + "    AND D.ID_ANORMALIDAD = A.ID\n"
    + "    AND D.ID_ORDEN = ?\n"
    + "    AND D.TAREA_ESTADO = 'S'\n"
    + "LIMIT 1\n"
    + ";";

    @Override
    @Nullable
    public AnormalidadXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        AnormalidadXmlBean object = AnormalidadXmlBean.builder().build();
        object.setCODIGOANORMALIDAD(rs.getString("COD_ANORMALIDAD"));
       
		return object;
    }

    
}
