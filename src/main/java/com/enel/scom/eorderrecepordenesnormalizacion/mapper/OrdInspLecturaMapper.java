package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class OrdInspLecturaMapper {

	public static final String SQLPOSTGRESQL_INSERT_ORD_INSP_LECTURA =
			"Insert into ord_insp_lectura\n"
			+ "              (ID_INSP_LECTURA\n"
			+ "              ,ID_EMPRESA\n"
			+ "              ,ID_MEDI_INSP\n"
			+ "              ,ID_LECTURA)\n"
			+ "              values\n"
			+ "              ( NEXTVAL('SQINSPECCIONMEDIDORLECTURA')\n"
			+ "              ,?\n"
			+ "              ,?\n"
			+ "              ,?)";
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_SQINSPECCIONMEDIDORLECTURA =
			"SELECT NEXTVAL('SQINSPECCIONMEDIDORLECTURA')";
}
