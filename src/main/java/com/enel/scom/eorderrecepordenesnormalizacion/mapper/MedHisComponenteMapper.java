package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class MedHisComponenteMapper {

	public static final String SQLPOSTGRESQL_UPDATE_FEC_HASTA_MED_HIS_COMPONENTE = "update med_his_componente "
			+ "set fec_hasta = now() " + "where id_his_componente = (select max(id_his_componente) "
			+ "from med_his_componente where id_componente = ? )";

	public static final String SQLPOSTGRESQL_INSERT_MEDIDOR_RETIRO = "insert into med_his_componente "
			+ "(id_his_componente,id_orden,id_componente,id_est_componente "
			+ ",fec_desde,fec_hasta,id_ubicacion,type_ubicacion ) " + "values ( ?, ?, ? "
			+ ",(select id from med_est_componente mec where cod_interno = 'Retirado') " + ",now(), NULL "
			+ ",CASE WHEN ? = 'C' THEN ? ELSE " + "(select id_contratista from com_ejecutor where id_ejecutor = ?) END "
			+ ",CASE WHEN ? = 'C' THEN 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente' ELSE 'com.synapsis.synergia.common.domain.Contratista' END "
			+ ")";

	public static final String SQLPOSTGRESQL_INSERT_MEDIDOR_INSTALACION = "insert into med_his_componente "
			+ "  (id_his_componente,id_orden,id_componente,id_est_componente "
			+ " ,fec_desde,fec_hasta,id_ubicacion,type_ubicacion )" + "  values ( ?,?,? "
			+ " ,(select id from med_est_componente where cod_interno = 'Instalado')"
			+ " ,now(), NULL " + " ,?"
			+ ",'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'" + " )";
}
