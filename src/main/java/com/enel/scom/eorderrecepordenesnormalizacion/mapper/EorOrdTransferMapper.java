package com.enel.scom.eorderrecepordenesnormalizacion.mapper;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorOrdTransferBean;

public class EorOrdTransferMapper implements RowMapper<EorOrdTransferBean> {
	public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFER_BY_ESTADOORDEN_TIPOEORDER = ""
	+ "SELECT \n"
	+ "      OT.ID_ORD_TRANSFER\n"
	+ "    , OT.COD_TIPO_ORDEN_LEGACY\n"
	+ "    , OT.NRO_ORDEN_LEGACY\n"
	+ "    , OT.COD_TIPO_ORDEN_EORDER\n"
	+ "    , OT.NRO_ORDEN_EORDER\n"
	+ "    , OT.COD_ESTADO_ORDEN\n"
	+ "    , OT.COD_ESTADO_ORDEN_ANT\n"
	+ "    , OT.NRO_ENVIOS\n"
	+ "    , OT.NRO_RECEPCIONES\n"
	+ "    , OT.SUSPENDIDA\n"
	+ "    , OT.NRO_ORDEN_LGC_RELAC\n"
	+ "    , OT.COD_CUMPLIMENTACION\n"
	+ "    , OT.FEC_OPERACION\n"
	+ "    , OT.COD_OPERACION\n"
	+ "    , OT.NRO_ANULACIONES\n"
	+ "    , OT.TIENE_ANEXOS\n"
	+ "    , OT.COD_ESTADO_ANEXO\n"
	+ "    , OT.NOM_ARCH_ANEXO\n"
	+ "    , OT.OBSERVACIONES\n"
	+ "    , OT.FLAG_ENVIO\n"
	+ "    , OT.GENERACION\n"
	+ "    , OT.FEC_ESTADO\n"
	+ "    , OT.NRO_CUENTA\n"
	+ "    , OT.CREADA_EORDER\n"
	+ "    , OT.ID_EMPRESA\n"
	+ "    , OT.FECHA_CREACION\n"
	+ "FROM \n"
	+ "      schscom.EOR_ORD_TRANSFER ot\n"
	+ "    , schscom.EOR_ORD_TRANSFER_DET otd\n"
	+ "WHERE 1 = 1\n"
	+ "    and ot.cod_estado_orden = ?\n"
	+ "    AND ot.COD_TIPO_ORDEN_EORDER IN (SELECT VALOR_ALF FROM COM_PARAMETROS WHERE SISTEMA ='EORDER' and ENTIDAD = 'PROCESO' AND CODIGO = ?) \n"
	+ "    AND otd.ID_ORD_TRANSFER = ot.ID_ORD_TRANSFER \n"
	+ "    AND otd.accion = 'RECEPCION' \n"
	+ "    AND otd.NRO_EVENTO = ot.NRO_RECEPCIONES \n"
	+ "    AND ot.Fec_operacion = otd.Fec_Accion\n"
	// + "    AND ot.id_ord_transfer = 83214974\n" //prueba en inicial
	// + "    AND ot.id_ord_transfer = 83071952\n"
	// + "    AND ot.id_ord_transfer = 82970124\n" //borrar/comentar
	// + "    AND ot.id_ord_transfer in (83289548,83289249,83289133,39874391,83195844,83139295,83020998,82970124,83153930,83194965,83170294,83154857,83139714)\n" //borrar/comentar
	+ ";";

	public static final String SQLPOSTGRESQL_SELECT = ""
	+ "SELECT ID_ORD_TRANSFER, COD_TIPO_ORDEN_LEGACY, NRO_ORDEN_LEGACY, COD_TIPO_ORDEN_EORDER, NRO_ORDEN_EORDER, COD_ESTADO_ORDEN, COD_ESTADO_ORDEN_ANT, NRO_ENVIOS, NRO_RECEPCIONES, SUSPENDIDA, NRO_ORDEN_LGC_RELAC, COD_CUMPLIMENTACION, FEC_OPERACION, COD_OPERACION, NRO_ANULACIONES, TIENE_ANEXOS, COD_ESTADO_ANEXO, NOM_ARCH_ANEXO, OBSERVACIONES, FLAG_ENVIO, GENERACION, FEC_ESTADO, NRO_CUENTA, CREADA_EORDER, ID_EMPRESA, FECHA_CREACION\n"
	+ "FROM SCHSCOM.EOR_ORD_TRANSFER\n"
	+ "WHERE NRO_ORDEN_LEGACY = ?\n"
	;


	@Override
	@Nullable
	public EorOrdTransferBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		return EorOrdTransferBean.builder()
		.idOrdTransfer(rs.getLong("ID_ORD_TRANSFER"))
		.codTipoOrdenLegacy(rs.getString("COD_TIPO_ORDEN_LEGACY"))
		.nroOrdenLegacy(rs.getString("NRO_ORDEN_LEGACY"))
		.codTipoOrdenEorder(rs.getString("COD_TIPO_ORDEN_EORDER"))
		.nroOrdenEorder(rs.getLong("NRO_ORDEN_EORDER"))
		.codEstadoOrden(rs.getLong("COD_ESTADO_ORDEN"))
		.codEstadoOrdenAnt(rs.getLong("COD_ESTADO_ORDEN_ANT"))
		.nroEnvios(rs.getLong("NRO_ENVIOS"))
		.nroRecepciones(rs.getLong("NRO_RECEPCIONES"))
		.suspendida(rs.getString("SUSPENDIDA"))
		.nroOrdenLgcRelac(rs.getString("NRO_ORDEN_LGC_RELAC"))
		.codCumplimentacion(rs.getLong("COD_CUMPLIMENTACION"))
		.fecOperacion(rs.getTimestamp("FEC_OPERACION"))
		.codOperacion(rs.getString("COD_OPERACION"))
		.nroAnulaciones(rs.getLong("NRO_ANULACIONES"))
		.tieneAnexos(rs.getString("TIENE_ANEXOS"))
		.codEstadoAnexo(rs.getLong("COD_ESTADO_ANEXO"))
		.nomArchAnexo(rs.getString("NOM_ARCH_ANEXO"))
		.observaciones(rs.getString("OBSERVACIONES"))
		.flagEnvio(rs.getLong("FLAG_ENVIO"))
		.generacion(rs.getString("GENERACION"))
		.fecEstado(rs.getTimestamp("FEC_ESTADO"))
		.nroCuenta(rs.getLong("NRO_CUENTA"))
		.creadaEorder(rs.getString("CREADA_EORDER"))
		.idEmpresa(rs.getLong("ID_EMPRESA"))
		.fechaCreacion(rs.getTimestamp("FECHA_CREACION"))
		.build();

		
	}
	

}
