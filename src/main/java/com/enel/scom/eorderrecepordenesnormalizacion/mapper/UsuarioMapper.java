package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.UsuarioBean;

public class UsuarioMapper implements RowMapper<UsuarioBean>{
	public static final String SQLPOSTGRESQL_SELECT_FOR_USUARIO_BY_USERNAME = ""
	+ "SELECT \n"
	+ "      ID AS ID\n"
	+ "    , USERNAME AS USERNAME\n"
	+ "    , FECHA_ALTA AS FECHA_ALTA\n"
	+ "    , FECHA_BAJA AS FECHA_BAJA\n"
	+ "    , FECHA_ULTIMO_INGRESO AS FECHA_ULTIMO_INGRESO\n"
	+ "    , ID_PERFIL AS ID_PERFIL\n"
	+ "    , ACTIVO AS ACTIVO\n"
	+ "FROM \n"
	+ "    USUARIO\n"
	+ "WHERE 1 = 1\n"
	+ "    AND USERNAME  = ?\n"
	+ ";";


	@Override
	@Nullable
	public UsuarioBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		UsuarioBean aux = UsuarioBean.builder().build();
		aux.setId(rs.getLong("ID"));// int8 NOT NULL,
		aux.setUsername(rs.getString("USERNAME"));// varchar(50) NULL,
		aux.setFecha_alta(rs.getDate("FECHA_ALTA"));// timestamp NULL,
		aux.setFecha_baja(rs.getDate("FECHA_BAJA"));// timestamp NULL,
		aux.setFecha_ultimo_ingreso(rs.getDate("FECHA_ULTIMO_INGRESO"));// timestamp NULL,
		aux.setId_perfil(rs.getLong("ID_PERFIL"));// int8 NULL,
		aux.setActivo(rs.getBoolean("ACTIVO"));// bool NULL,
		return aux;
	}


}
