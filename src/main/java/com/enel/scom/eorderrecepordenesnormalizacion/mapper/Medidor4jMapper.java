package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.Medidor4jBean;

public class Medidor4jMapper  implements RowMapper<Medidor4jBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_OPERACION = ""
    + "SELECT \n"
    + "      NRO_COMPONENTE \n"
    + "    , MM.COD_MARCA \n"
    + "    , ML.COD_MODELO\n"
    + "FROM \n"
    + "      SCHSCOM.MED_COMPONENTE A\n"
    + "    , SCHSCOM.MED_MARCA MM\n"
    + "    , SCHSCOM.MED_MODELO ML\n"
    + "WHERE 1 = 1\n"
    + "    AND A.ID_UBICACION = ?\n"
    + "    AND A.TYPE_UBICACION = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'  \n"
    + "    AND A.ID_EST_COMPONENTE = (SELECT ID FROM MED_EST_COMPONENTE WHERE COD_INTERNO = 'Instalado') \n"
    + "    AND A.ID_MODELO = ML.ID \n"
    + "    AND ML.ID_MARCA = MM.ID\n"
    + ";";

    @Override
    @Nullable
    public Medidor4jBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        Medidor4jBean object = Medidor4jBean.builder().build();
        
        object.setNroComponente(rs.getString("NRO_COMPONENTE"));
        object.setCodMarca(rs.getString("COD_MARCA"));
        object.setCodModelo(rs.getString("COD_MODELO"));
        

       
		return object;
    }

    
}
