package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAbBean;

public class OrdOrdenAbMapper implements RowMapper<OrdOrdenAbBean>{
    public static final String SQLPOSTGRESQL_SELECT = ""
    + "SELECT \n"
				+ "      OO.ID AS IDORDEN\n"
				+ "    , WW.ID AS IDWORKFLOW\n"
				+ "    , WW.ID_STATE AS IDSTATE\n"
				+ "    , WW1.ID AS IDWORKFLOWSEG\n"
				+ "    , OO.ID_SERVICIO AS IDSERVICIO\n"
				+ "    , C.NRO_SERVICIO AS NROSERVICIO\n"
				+ "    , DON.ID_ORD_INSP AS IDORDINSP\n"
				+ "FROM \n"
				+ "      ORD_ORDEN OO\n"
				+ "    , ORD_TIPO_ORDEN OTO\n"
				+ "    , WKF_WORKFLOW WW\n"
				+ "    , DIS_ORD_NORM DON\n"
				+ "    , DIS_ORD_INSP DOI\n"
				+ "    , DIS_SEG_INSP DSI\n"
				+ "    , WKF_WORKFLOW WW1\n"
				+ "    , CLIENTE C\n"
				+ "WHERE 1 = 1\n"
				+ "    AND OO.ID_TIPO_ORDEN = OTO.ID\n"
				+ "    AND OO.NRO_ORDEN = ?\n"
				+ "    AND OTO.COD_INTERNO = 'OrdenNormalizacion'\n"
				+ "    AND OO.ID_WORKFLOW = WW.ID\n"
				+ "    AND OO.ID = DON.ID_ORDEN\n"
				+ "    AND DON.ID_ORD_INSP = DOI.ID_ORDEN\n"
				+ "    AND DOI.ID_SEGINSP = DSI.ID\n"
				+ "    AND DSI.ID_WKF = WW1.ID\n"
				+ "    AND OO.ID_SERVICIO = C.ID_SERVICIO \n"
    ;
    @Override
    @Nullable
    public OrdOrdenAbBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return OrdOrdenAbBean.builder()
        .idOrden(rs.getLong("IDORDEN"))
        .idWorkflow(rs.getLong("IDWORKFLOW"))
        .idState(rs.getString("IDSTATE"))
        .idWorkflowSeg(rs.getLong("IDWORKFLOWSEG"))
        .idServicio(rs.getLong("IDSERVICIO"))
        .nroServicio(rs.getLong("NROSERVICIO"))
        .idOrdInsp(rs.getLong("IDORDINSP"))
        .build();
    }
    
}
