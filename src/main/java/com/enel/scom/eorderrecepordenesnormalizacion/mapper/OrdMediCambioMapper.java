package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class OrdMediCambioMapper {

	public static final String SQLPOSTGRESQL_INSERT_ORD_MEDI_CAMBIO = 
			"insert into ord_medi_cambio\n"
			+ "                  (id_orden\n"
			+ "                  ,id_componente\n"
			+ "                  ,id_empresa\n"
			+ "                  ,accion\n"
			+ "                  ,id_medi_cambio)\n"
			+ "                  values\n"
			+ "                  (?\n"
			+ "                   ,?\n"
			+ "                   ,3\n"
			//+ "                   ,'Retiro'\n"
			// Agregado R.I. Se agrega accion
			+ "                   ,?\n"
			+ "                   ,NEXTVAL('SQCAMBIOMEDIDORORDEN')\n"
			+ "                  )";
}
