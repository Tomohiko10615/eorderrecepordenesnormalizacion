package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DatoProcesoXmlBean;

public class DatoProcesoMapper implements RowMapper<DatoProcesoXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_DATOPROCESO = ""
            + "SELECT \n"
            + "      UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/LLAVE_SECRETA/text()', D.REG_XML))::TEXT AS LLAVE_SECRETA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Distribuidora/text()', D.REG_XML))::TEXT AS CODIGO_DISTRIBUIDORA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Sistema_Externo_de_Origen/text()', D.REG_XML))::TEXT AS CODIGO_SISTEMA_EXTERNO_ORIGEN \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Tipo_de_TdC/text()', D.REG_XML))::TEXT AS CODIGO_TIPO_DE_TDC \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Externo_del_TdC/text()', D.REG_XML))::TEXT AS CODIGO_EXTERNO_DEL_TDC \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Interno_del_TdC/text()', D.REG_XML))::TEXT AS CODIGO_INTERNO_DEL_TDC \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Proceso/text()', D.REG_XML))::TEXT AS CODIGO_PROCESO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_SubProceso/text()', D.REG_XML))::TEXT AS CODIGO_SUBPROCESO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/TDC_creado_en_Eorder/text()', D.REG_XML))::TEXT AS TDC_CREADO_EN_EORDER \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', D.REG_XML))::TEXT AS CODIGO_RESULTADO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Causal_Resultado/text()', D.REG_XML))::TEXT AS CODIGO_CAUSAL_RESULTADO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Nota_Codificada/text()', D.REG_XML))::TEXT AS CODIGO_NOTA_CODIFICADA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Inicio_TdC/text()', D.REG_XML))::TEXT AS INICIO_TDC \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fin_TdC/text()', D.REG_XML))::TEXT AS FIN_TDC \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Duracion_Ejecucion/text()', D.REG_XML))::TEXT AS DURACION_EJECUCION \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Cuadrilla/text()', D.REG_XML))::TEXT AS CODIGO_CUADRILLA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Placa_Vehiculo/text()', D.REG_XML))::TEXT AS PLACA_VEHICULO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Nombre_Usuario/text()', D.REG_XML))::TEXT AS NOMBRE_USUARIO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Apellido_Usuario/text()', D.REG_XML))::TEXT AS APELLIDO_USUARIO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Matricula_Usuario/text()', D.REG_XML))::TEXT AS MATRICULA_USUARIO \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_medidor/text()', D.REG_XML))::TEXT AS LATITUD_MEDIDOR \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_medidor/text()', D.REG_XML))::TEXT AS LONGITUD_MEDIDOR \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_empalme/text()', D.REG_XML))::TEXT AS LATITUD_EMPALME \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_empalme/text()', D.REG_XML))::TEXT AS LONGITUD_EMPALME \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fecha_Hora_Inicio_Cita/text()', D.REG_XML))::TEXT AS FECHA_HORA_INICIO_CITA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fecha_Hora_Fin_Cita/text()', D.REG_XML))::TEXT AS FECHA_HORA_FIN_CITA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Nombre_persona_contacto_para_cita/text()', D.REG_XML))::TEXT AS NOMBRE_PERSONA_CONTACTO_CITA \n"
            + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Telefono_persona_contacto_cita/text()', D.REG_XML))::TEXT AS TELEFONO_PERSONA_CONTACTO_CITA \n"
            + "FROM \n"
            + "    SCHSCOM.EOR_ORD_TRANSFER_DET D \n"
            + "WHERE 1 = 1 \n"
            + "    AND D.ID_ORD_TRANSFER = ?\n"
            + "    AND D.ACCION ='RECEPCION'\n"
            + "    AND D.NRO_EVENTO = ?\n"
            + "    AND D.FEC_ACCION IN (SELECT C.FEC_OPERACION FROM SCHSCOM.EOR_ORD_TRANSFER C WHERE D.ID_ORD_TRANSFER = C.ID_ORD_TRANSFER) \n"
            + ";";

    @Override
    @Nullable
    public DatoProcesoXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        DatoProcesoXmlBean object = DatoProcesoXmlBean.builder().build();
        object.setLLAVE_SECRETA(rs.getString("LLAVE_SECRETA"));
        object.setCODIGO_DISTRIBUIDORA(rs.getString("CODIGO_DISTRIBUIDORA"));
        object.setCODIGO_SISTEMA_EXTERNO_ORIGEN(rs.getString("CODIGO_SISTEMA_EXTERNO_ORIGEN"));
        object.setCODIGO_TIPO_DE_TDC(rs.getString("CODIGO_TIPO_DE_TDC"));
        object.setCODIGO_EXTERNO_DEL_TDC(rs.getString("CODIGO_EXTERNO_DEL_TDC"));
        object.setCODIGO_INTERNO_DEL_TDC(rs.getString("CODIGO_INTERNO_DEL_TDC"));
        object.setCODIGO_PROCESO(rs.getString("CODIGO_PROCESO"));
        object.setCODIGO_SUBPROCESO(rs.getString("CODIGO_SUBPROCESO"));
        object.setTDC_CREADO_EN_EORDER(rs.getString("TDC_CREADO_EN_EORDER"));
        object.setCODIGO_RESULTADO(rs.getString("CODIGO_RESULTADO"));
        object.setCODIGO_CAUSAL_RESULTADO(rs.getString("CODIGO_CAUSAL_RESULTADO"));
        object.setCODIGO_NOTA_CODIFICADA(rs.getString("CODIGO_NOTA_CODIFICADA"));
        object.setINICIO_TDC(rs.getString("INICIO_TDC"));
        object.setFIN_TDC(rs.getString("FIN_TDC"));
        object.setDURACION_EJECUCION(rs.getString("DURACION_EJECUCION"));
        object.setCODIGO_CUADRILLA(rs.getString("CODIGO_CUADRILLA"));
        object.setPLACA_VEHICULO(rs.getString("PLACA_VEHICULO"));
        object.setNOMBRE_USUARIO(rs.getString("NOMBRE_USUARIO"));
        object.setAPELLIDO_USUARIO(rs.getString("APELLIDO_USUARIO"));
        object.setMATRICULA_USUARIO(rs.getString("MATRICULA_USUARIO"));
        object.setLATITUD_MEDIDOR(rs.getString("LATITUD_MEDIDOR"));
        object.setLONGITUD_MEDIDOR(rs.getString("LONGITUD_MEDIDOR"));
        object.setLATITUD_EMPALME(rs.getString("LATITUD_EMPALME"));
        object.setLONGITUD_EMPALME(rs.getString("LONGITUD_EMPALME"));
        object.setFECHA_HORA_INICIO_CITA(rs.getString("FECHA_HORA_INICIO_CITA"));
        object.setFECHA_HORA_FIN_CITA(rs.getString("FECHA_HORA_FIN_CITA"));
        object.setNOMBRE_PERSONA_CONTACTO_CITA(rs.getString("NOMBRE_PERSONA_CONTACTO_CITA"));
        object.setTELEFONO_PERSONA_CONTACTO_CITA(rs.getString("TELEFONO_PERSONA_CONTACTO_CITA"));
        return object;
    }

}
