package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenBean;

public class OrdOrdenMapper implements RowMapper<OrdOrdenBean>{
    public static final String SQLPOSTGRESQL_SELECT_ALL_FROM_ORDORDEN_BY_ID_AND_DISCRIMINADOR = ""
                + "SELECT \n"
				+ "      ID\n"
				+ "    , ID_ORDEN_SC4J\n"
				+ "    , ID_WORKFLOW\n"
				+ "    , ID_TIPO_ORDEN\n"
				+ "    , NRO_ORDEN\n"
				+ "    , ID_BUZON\n"
				+ "    , FECHA_CREACION\n"
				+ "    , DISCRIMINADOR\n"
				+ "    , ID_USUARIO_CREADOR\n"
				+ "    , ID_SERVICIO\n"
				+ "    , FECHA_FINALIZACION\n"
				+ "    , LEIDO\n"
				+ "    , FECHA_INGRESO_ESTADO_ACTUAL\n"
				+ "    , ID_MOTIVO_ANULACION\n"
				+ "    , ID_MOTIVO\n"
				+ "    , FECHA_VENCIMIENTO\n"
				+ "    , FECHA_VENC_LEGAL\n"
				+ "    , ID_USUARIO_REGISTRO\n"
				+ "    , FEC_REGISTRO\n"
				+ "    , ID_USUARIO_MODIF\n"
				+ "    , FEC_MODIF\n"
				+ "    , ACTIVO\n"
				+ "FROM \n"
				+ "    ORD_ORDEN\n"
				+ "WHERE 1 = 1\n"
				+ "    AND ID = ?\n"
				+ "    AND DISCRIMINADOR = ?\n"
                ;
    @Override
    @Nullable
    public OrdOrdenBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return OrdOrdenBean.builder()
        .id(rs.getLong("ID"))
        .idOrdenSc4j(rs.getLong("ID_ORDEN_SC4J"))
        .idWorkflow(rs.getLong("ID_WORKFLOW"))
        .idTipoOrden(rs.getLong("ID_TIPO_ORDEN"))
        .nroOrden(rs.getString("NRO_ORDEN"))
        .idBuzon(rs.getLong("ID_BUZON"))
        .fechaCreacion(rs.getTimestamp("FECHA_CREACION"))
        .discriminador(rs.getString("DISCRIMINADOR"))
        .idUsuarioCreador(rs.getLong("ID_USUARIO_CREADOR"))
        .idServicio(rs.getLong("ID_SERVICIO"))
        .fechaFinalizacion(rs.getTimestamp("FECHA_FINALIZACION"))
        .leido(rs.getString("LEIDO"))
        .fechaIngresoEstadoActual(rs.getTimestamp("FECHA_INGRESO_ESTADO_ACTUAL"))
        .idMotivoAnulacion(rs.getLong("ID_MOTIVO_ANULACION"))
        .idMotivo(rs.getLong("ID_MOTIVO"))
        .fechaVencimiento(rs.getTimestamp("FECHA_VENCIMIENTO"))
        .fechaVencLegal(rs.getTimestamp("FECHA_VENC_LEGAL"))
        .idUsuarioRegistro(rs.getLong("ID_USUARIO_REGISTRO"))
        .fecRegistro(rs.getTimestamp("FEC_REGISTRO"))
        .idUsuarioModif(rs.getLong("ID_USUARIO_MODIF"))
        .fecModif(rs.getTimestamp("FEC_MODIF"))
        .activo(rs.getBoolean("ACTIVO"))
        .build();
    }

  
    
}
