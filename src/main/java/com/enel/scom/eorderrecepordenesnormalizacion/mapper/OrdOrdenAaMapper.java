package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdOrdenAaBean;

public class OrdOrdenAaMapper implements RowMapper<OrdOrdenAaBean> {
    public static final String SQLPOSTGRESQL_SELECT = ""
            + "SELECT \n"
            + "      DET.NRO_ORDEN\n"
            + "    , C.NRO_CUENTA\n"
            + "FROM \n"
            + "      ORD_ORDEN OO\n"
            + "    , WKF_WORKFLOW WW \n"
            + "    , ORD_TIPO_ORDEN OTO\n"
            + "    , DIS_ORD_NORM NOR\n"
            + "    , ORD_ORDEN DET\n"
            + "    , CLIENTE C\n"
            + "WHERE \n"
            + "    OO.ID_WORKFLOW = WW.ID\n"
            + "    AND OO.ID_TIPO_ORDEN = OTO.ID\n"
            + "    AND NOR.ID_ORD_INSP = OO.ID\n"
            + "    AND NOR.ID_ORDEN= DET.ID\n"
            + "    AND DET.ID_TIPO_ORDEN = 7\n"
            + "    AND OO.NRO_ORDEN = ?\n"
            + "    AND OTO.COD_INTERNO = 'OrdenInspeccion'\n"
            + "    AND ID_STATE IN ('Recibida')\n"
            + "    AND C.ID_SERVICIO = DET.ID_SERVICIO\n";

    @Override
    @Nullable
    public OrdOrdenAaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return OrdOrdenAaBean.builder().nroOrden(rs.getString("NRO_ORDEN")).nroCuenta(rs.getLong("NRO_CUENTA")).build();
    }

}
