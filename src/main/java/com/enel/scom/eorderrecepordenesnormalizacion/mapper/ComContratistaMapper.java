package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class ComContratistaMapper {

	public static final String SQLPOSTGRESQL_COUNT_COM_CONTRATISTA =
			"SELECT COUNT(ID) "
			+ "FROM COM_CONTRATISTA WHERE COD_CONTRATISTA = ? "
			+ "AND ACTIVO = 'S'";
}
