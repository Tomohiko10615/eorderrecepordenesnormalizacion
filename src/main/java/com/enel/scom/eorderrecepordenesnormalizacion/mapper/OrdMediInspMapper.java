package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class OrdMediInspMapper {

	public static final String SQLPOSTGRESQL_INSERT_ORD_MEDI_INSP = 
			"insert into ord_medi_insp\n"
			+ "            (ID_ORDEN\n"
			+ "            ,ID_COMPONENTE\n"
			+ "            ,ID_EMPRESA\n"
			+ "            ,ID_MEDI_INSP\n"
			+ "            )\n"
			+ "            values\n"
			+ "            (?\n"
			+ "            ,?\n"
			+ "            ,?\n"
			+ "            ,?\n"
			+ "            )";
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_SQINSPECCIONMEDIDORESORDEN = 
			"SELECT NEXTVAL('SQINSPECCIONMEDIDORESORDEN')";
}
