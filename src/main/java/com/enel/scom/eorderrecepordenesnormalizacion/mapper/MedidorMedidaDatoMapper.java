package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorMedidaDatoBean;

public class MedidorMedidaDatoMapper implements RowMapper<MedidorMedidaDatoBean> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_RETIRAR = ""
	+ "select\n"
	+ "	  CASE WHEN b1.id is NULL THEN 'N' ELSE 'S' END AS tiene_enfp\n"
	+ "	, CASE WHEN b2.id is NULL THEN 'N' ELSE 'S' END AS tiene_enhp\n"
	+ "	, CASE WHEN b3.id is NULL THEN 'N' ELSE 'S' END AS tiene_dmfp\n"
	+ "	, CASE WHEN b4.id is NULL THEN 'N' ELSE 'S' END AS tiene_dmhp\n"
	+ "	, CASE WHEN b5.id is NULL THEN 'N' ELSE 'S' END AS tiene_reac\n"
	+ "	, c1.ID AS dIdMedida1, b1.CANT_ENTEROS AS dEnteros1, b1.CANT_DECIMALES AS dDecimales1, b1.VAL_FACTOR AS dFactor1, b1.ID_TIP_CALCULO AS dIdTipoCalculo1\n"
	+ "	, c2.ID AS dIdMedida2, b2.CANT_ENTEROS AS dEnteros2, b2.CANT_DECIMALES AS dDecimales2, b2.VAL_FACTOR AS dFactor2, b2.ID_TIP_CALCULO AS dIdTipoCalculo2\n"
	+ "	, c3.ID AS dIdMedida3, b3.CANT_ENTEROS AS dEnteros3, b3.CANT_DECIMALES AS dDecimales3, b3.VAL_FACTOR AS dFactor3, b3.ID_TIP_CALCULO AS dIdTipoCalculo3\n"
	+ "	, c4.ID AS dIdMedida4, b4.CANT_ENTEROS AS dEnteros4, b4.CANT_DECIMALES AS dDecimales4, b4.VAL_FACTOR AS dFactor4, b4.ID_TIP_CALCULO AS dIdTipoCalculo4\n"
	+ "	, c5.ID AS dIdMedida5, b5.CANT_ENTEROS AS dEnteros5, b5.CANT_DECIMALES AS dDecimales5, b5.VAL_FACTOR AS dFactor5, b5.ID_TIP_CALCULO AS dIdTipoCalculo5\n"
	+ "	, CASE WHEN d.cod_interno = 'Cliente' THEN 'C' ELSE 'E' END AS spropiedad_ret\n"
	+ "from\n"
	+ "	med_componente a\n"
	+ "	left join med_medida_medidor b1 on a.id = b1.id_componente and b1.ID_MEDIDA = 1\n"
	+ "	left join med_medida_medidor b2 on a.id = b2.id_componente and b2.ID_MEDIDA = 2\n"
	+ "	left join med_medida_medidor b3 on a.id = b3.id_componente and b3.ID_MEDIDA = 4\n"
	+ "	left join med_medida_medidor b4 on a.id = b4.id_componente and b4.ID_MEDIDA = 5\n"
	+ "	left join med_medida_medidor b5 on a.id = b5.id_componente and b5.ID_MEDIDA = 6\n"
	+ "	, med_medida c1\n"
	+ "	, med_medida c2\n"
	+ "	, med_medida c3\n"
	+ "	, med_medida c4\n"
	+ "	, med_medida c5\n"
	+ "	, MED_PROPIEDAD d\n"
	+ "where 1 = 1\n"
	+ "	and c1.COD_INTERNO = 'ActivaFueraPunta'\n"
	+ "	and c2.COD_INTERNO = 'ActivaHoraPunta'\n"
	+ "	and c3.COD_INTERNO = 'DemandaFueraPunta'\n"
	+ "	and c4.COD_INTERNO = 'DemandaHoraPunta'\n"
	+ "	and c5.COD_INTERNO = 'Reactiva'\n"
	+ "	and a.id_propiedad = d.id\n"
	+ "	and a.id = ?\n"
	;

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_INSTALAR = ""
	+ "select\n"
	+ "	  CASE WHEN b1.id is NULL THEN 'N' ELSE 'S' END AS tiene_enfp\n"
	+ "	, CASE WHEN b2.id is NULL THEN 'N' ELSE 'S' END AS tiene_enhp\n"
	+ "	, CASE WHEN b3.id is NULL THEN 'N' ELSE 'S' END AS tiene_dmfp\n"
	+ "	, CASE WHEN b4.id is NULL THEN 'N' ELSE 'S' END AS tiene_dmhp\n"
	+ "	, CASE WHEN b5.id is NULL THEN 'N' ELSE 'S' END AS tiene_reac\n"
	+ "	, c1.ID AS dIdMedida1, b1.CANT_ENTEROS AS dEnteros1, b1.CANT_DECIMALES AS dDecimales1, b1.VAL_FACTOR AS dFactor1, b1.ID_TIP_CALCULO AS dIdTipoCalculo1\n"
	+ "	, c2.ID AS dIdMedida2, b2.CANT_ENTEROS AS dEnteros2, b2.CANT_DECIMALES AS dDecimales2, b2.VAL_FACTOR AS dFactor2, b2.ID_TIP_CALCULO AS dIdTipoCalculo2\n"
	+ "	, c3.ID AS dIdMedida3, b3.CANT_ENTEROS AS dEnteros3, b3.CANT_DECIMALES AS dDecimales3, b3.VAL_FACTOR AS dFactor3, b3.ID_TIP_CALCULO AS dIdTipoCalculo3\n"
	+ "	, c4.ID AS dIdMedida4, b4.CANT_ENTEROS AS dEnteros4, b4.CANT_DECIMALES AS dDecimales4, b4.VAL_FACTOR AS dFactor4, b4.ID_TIP_CALCULO AS dIdTipoCalculo4\n"
	+ "	, c5.ID AS dIdMedida5, b5.CANT_ENTEROS AS dEnteros5, b5.CANT_DECIMALES AS dDecimales5, b5.VAL_FACTOR AS dFactor5, b5.ID_TIP_CALCULO AS dIdTipoCalculo5\n"
	+ "from\n"
	+ "	med_componente a\n"
	+ "	left join med_medida_medidor b1 on a.id = b1.id_componente and b1.ID_MEDIDA = 1\n"
	+ "	left join med_medida_medidor b2 on a.id = b2.id_componente and b2.ID_MEDIDA = 2\n"
	+ "	left join med_medida_medidor b3 on a.id = b3.id_componente and b3.ID_MEDIDA = 4\n"
	+ "	left join med_medida_medidor b4 on a.id = b4.id_componente and b4.ID_MEDIDA = 5\n"
	+ "	left join med_medida_medidor b5 on a.id = b5.id_componente and b5.ID_MEDIDA = 6\n"
	+ "	, med_medida c1\n"
	+ "	, med_medida c2\n"
	+ "	, med_medida c3\n"
	+ "	, med_medida c4\n"
	+ "	, med_medida c5\n"
	+ "where 1 = 1\n"
	+ "	and c1.COD_INTERNO = 'ActivaFueraPunta'\n"
	+ "	and c2.COD_INTERNO = 'ActivaHoraPunta'\n"
	+ "	and c3.COD_INTERNO = 'DemandaFueraPunta'\n"
	+ "	and c4.COD_INTERNO = 'DemandaHoraPunta'\n"
	+ "	and c5.COD_INTERNO = 'Reactiva'\n"
	+ "	and a.id = ?\n"
	;

	@Override
	public MedidorMedidaDatoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		MedidorMedidaDatoBean aux = MedidorMedidaDatoBean.builder().build();
		aux.setTieneMedidaEnfp(rs.getString("tiene_enfp"));
		aux.setTieneMedidaEnhp(rs.getString("tiene_enhp"));
		aux.setTieneMedidaDmfp(rs.getString("tiene_dmfp"));
		aux.setTieneMedidaDmhp(rs.getString("tiene_dmhp"));
		aux.setTieneMedidaReac(rs.getString("tiene_reac"));
		aux.setIdMedida1(rs.getLong("dIdMedida1"));
		aux.setCantEnteros1(rs.getLong("dEnteros1"));
		aux.setCantDecimales1(rs.getLong("dDecimales1"));
		aux.setValFactor1(rs.getBigDecimal("dFactor1"));
		aux.setIdTipCalculo1(rs.getLong("dIdTipoCalculo1"));
		aux.setIdMedida2(rs.getLong("dIdMedida2"));
		aux.setCantEnteros2(rs.getLong("dEnteros2"));
		aux.setCantDecimales2(rs.getLong("dDecimales2"));
		aux.setValFactor2(rs.getBigDecimal("dFactor2"));
		aux.setIdTipCalculo2(rs.getLong("dIdTipoCalculo2"));
		aux.setIdMedida3(rs.getLong("dIdMedida3"));
		aux.setCantEnteros3(rs.getLong("dEnteros3"));
		aux.setCantDecimales3(rs.getLong("dDecimales3"));
		aux.setValFactor3(rs.getBigDecimal("dFactor3"));
		aux.setIdTipCalculo3(rs.getLong("dIdTipoCalculo3"));
		aux.setIdMedida4(rs.getLong("dIdMedida4"));
		aux.setCantEnteros4(rs.getLong("dEnteros4"));
		aux.setCantDecimales4(rs.getLong("dDecimales4"));
		aux.setValFactor4(rs.getBigDecimal("dFactor4"));
		aux.setIdTipCalculo4(rs.getLong("dIdTipoCalculo4"));
		aux.setIdMedida5(rs.getLong("dIdMedida5"));
		aux.setCantEnteros5(rs.getLong("dEnteros5"));
		aux.setCantDecimales5(rs.getLong("dDecimales5"));
		aux.setValFactor5(rs.getBigDecimal("dFactor5"));
		aux.setIdTipCalculo5(rs.getLong("dIdTipoCalculo5"));
		try {
			aux.setPropiedad(rs.getString("spropiedad_ret"));
		} catch (Exception e) {
			
		}
		return aux;
	}
}
