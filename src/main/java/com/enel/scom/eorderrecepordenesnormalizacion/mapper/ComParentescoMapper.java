package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class ComParentescoMapper {

	public static final String SQLPOSTGRESQL_SELECT_FOR_PARENTESCO =
			"SELECT id_parentesco "
			+ "FROM com_parentesco "
			+ "WHERE upper(cod_parentesco) = upper(trim(?)) "
			+ "AND id_empresa = ?";

}
