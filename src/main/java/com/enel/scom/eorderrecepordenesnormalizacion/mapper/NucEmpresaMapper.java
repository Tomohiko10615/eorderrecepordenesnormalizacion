package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.NucEmpresaBean;

public class NucEmpresaMapper implements RowMapper<NucEmpresaBean>{
	public static final String SQLPOSTGRESQL_SELECT_FOR_NUCEMPRESA_BY_CODPARTITION = ""
	+ "SELECT \n"
	+ "      id_empresa\n"
	+ "    , tiene_datos\n"
	+ "    , tiene_usuarios\n"
	+ "    , es_global\n"
	+ "    , cod_partition\n"
	+ "    , des_partition\n"
	+ "    , name_application\n"
	+ "FROM \n"
	+ "    schscom.nuc_empresa\n"
	+ "where 1 = 1\n"
	+ "    and cod_partition = ?\n"
	+ ";";

	@Override
	public NucEmpresaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		NucEmpresaBean aux = NucEmpresaBean.builder().build();
		aux.setId_empresa(rs.getLong("id_empresa"));// id_empresa int8 NOT NULL,
		aux.setTiene_datos(rs.getString("tiene_datos"));// tiene_datos varchar(1) NULL,
		aux.setTiene_usuarios(rs.getString("tiene_usuarios"));// tiene_usuarios varchar(1) NULL,
		aux.setEs_global(rs.getString("es_global"));// es_global varchar(1) NULL,
		aux.setCod_partition(rs.getString("COD_PARTITION"));// cod_partition varchar(6) NULL,
		aux.setDes_partition(rs.getString("des_partition"));// des_partition varchar(100) NULL,
		aux.setName_application(rs.getString("name_application"));// name_application varchar(50) NULL,
		return aux;
	}	
}
