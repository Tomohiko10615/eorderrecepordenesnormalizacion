package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.ComCuadrillaAaBean;

public class ComCuadrillaMapper implements RowMapper<ComCuadrillaAaBean> {

	public static final String SQLPOSTGRESQL_SELECT_COUNT_CUADRILLA = "SELECT count(*) "
			+ "FROM com_cuadrilla com_cu, com_ejecutor com_e "
			+ "WHERE com_e.ID_CUADRILLA = com_cu.ID_CUADRILLA "
			+ "and upper(com_cu.COD_CUADRILLA) = upper(trim(?)) ";
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_CUADRILLA = "SELECT "
			+ "com_cu.ID_CUADRILLA,com_e.ID_EJECUTOR "
			+ "FROM com_cuadrilla com_cu, com_ejecutor com_e "
			+ "WHERE com_e.ID_CUADRILLA = com_cu.ID_CUADRILLA "
			+ "and upper(com_cu.COD_CUADRILLA)= upper(trim(?)) ";
	
	@Override
	public ComCuadrillaAaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ComCuadrillaAaBean parametroAux = ComCuadrillaAaBean.builder().build();
		parametroAux.setIdCuadrilla(rs.getLong("ID_CUADRILLA"));
		parametroAux.setIdEjecutor(rs.getLong("ID_EJECUTOR"));
		return parametroAux;
	}

}
