package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.SrvActComercialBean;

public class SrvActComercialMapper implements RowMapper<SrvActComercialBean> {
    public static final String SQLPOSTGRESQL_SELECT_ALL = ""
            + "SELECT id_act_comercial, id_empresa, cod_act_comercial, des_act_comercial, activo, cod_interno\n"
            + "FROM schscom.srv_act_comercial\n"
            + "WHERE upper(cod_act_comercial) = upper(trim(?))\n"
            + "AND id_empresa = ?\n";

    @Override
    @Nullable
    public SrvActComercialBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return SrvActComercialBean.builder()
                .idActComercial(rs.getLong("id_act_comercial"))
                .idEmpresa(rs.getLong("id_empresa"))
                .codActComercial(rs.getString("cod_act_comercial"))
                .desActComercial(rs.getString("des_act_comercial"))
                .activo(rs.getString("activo"))
                .codInterno(rs.getString("cod_interno"))
                .build();
    }

}
