package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.OperacionXmlBean;

public class OperacionXmlMapper implements RowMapper<OperacionXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_OPERACION = ""
    + "SELECT \n"
    + "      UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Codigo_Operacion/text()', D.REG_XML))::TEXT AS CODIGO_OPERACION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Codigo_Resultado/text()', D.REG_XML))::TEXT AS CODIGO_RESULTADO\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Codigo_Causal_Resultado/text()', D.REG_XML))::TEXT AS CODIGO_CAUSAL_RESULTADO\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Codigo_Nota_Codificada/text()', D.REG_XML))::TEXT AS CODIGO_NOTA_CODIFICADA\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Inicio_Operacion/text()', D.REG_XML))::TEXT AS INICIO_OPERACION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Fin_Operacion/text()', D.REG_XML))::TEXT AS FIN_OPERACION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Duracion_Operacion/text()', D.REG_XML))::TEXT AS DURACION_OPERACION\n"
    + "    , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/operaciones/item/Notas_Operacion/text()', D.REG_XML))::TEXT AS NOTAS_OPERACION\n"
    + "FROM\n"
    + "    SCHSCOM.EOR_ORD_TRANSFER_DET D\n"
    + "WHERE 1 = 1\n"
    + "    AND D.ID_ORD_TRANSFER = ?\n"
    + "    AND D.ACCION ='RECEPCION'\n"
    + "    AND D.NRO_EVENTO = ?\n"
    + "    AND D.FEC_ACCION IN (SELECT C.FEC_OPERACION FROM SCHSCOM.EOR_ORD_TRANSFER C WHERE D.ID_ORD_TRANSFER = C.ID_ORD_TRANSFER)\n"
    + ";";

    @Override
    @Nullable
    public OperacionXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        OperacionXmlBean object = OperacionXmlBean.builder().build();
        object.setCODIGO_OPERACION(rs.getString("CODIGO_OPERACION"));
        object.setCODIGO_RESULTADO(rs.getString("CODIGO_RESULTADO"));
        object.setCODIGO_CAUSAL_RESULTADO(rs.getString("CODIGO_CAUSAL_RESULTADO"));
        object.setCODIGO_NOTA_CODIFICADA(rs.getString("CODIGO_NOTA_CODIFICADA"));
        object.setINICIO_OPERACION(rs.getString("INICIO_OPERACION"));
        object.setFIN_OPERACION(rs.getString("FIN_OPERACION"));
        object.setDURACION_OPERACION(rs.getString("DURACION_OPERACION"));
        object.setNOTAS_OPERACION(rs.getString("NOTAS_OPERACION"));
		return object;
    }

    
    
}