package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class DisOrdTareaMapper {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_ORD_TAREA =
			"SELECT ID "
			+ "FROM DIS_ORD_TAREA "
			+ "WHERE "
			+ "ID_ORDEN = ? "
			+ "AND ID_ANORMALIDAD = ? "
			+ "AND ID_TAREA = ?";
	
	public static final String SQLPOSTGRESQL_INSERT_INTO_DIS_ORD_TAREA =
			"insert into dis_ord_tarea ("
			+ "ID "
			+ ",ID_ORDEN "
			+ ",ID_ANORMALIDAD "
			+ ",ID_TAREA "
			+ ",TAREA_ESTADO "
			+ ") "
			+ "values "
			+ "(? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",'S' "
			+ ")";
	
	public static final String SQLPOSTGRESQL_UPDATE_ESTADO_BY_ID_ORDEN_TAREA =
			"update dis_ord_tarea "
			+ "set TAREA_ESTADO='S' "
			+ "where ID = ?";
	
	public static final String SQLPOSTGRESQL_UPDATE_ESTADO_BY_ID_ORDEN =
			"update dis_ord_tarea set tarea_estado = 'N' "
			+ "where id_orden = ?";
	
	public static final String SQLPOSTGRESQL_SELECT_NEXTVAL_SQTAREAORDEN =
	"SELECT NEXTVAL('SQTAREAORDEN')";
}
