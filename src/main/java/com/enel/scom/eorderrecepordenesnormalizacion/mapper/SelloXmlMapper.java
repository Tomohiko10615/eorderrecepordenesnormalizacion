package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.SelloXmlBean;

public class SelloXmlMapper implements RowMapper<SelloXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR = ""
    ;

    @Override
    @Nullable
    public SelloXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        SelloXmlBean object = SelloXmlBean.builder().build();
        object.setNUMEROSELLO(rs.getString("NUMEROSELLO"));
        object.setUBICACIONSELLO(rs.getString("UBICACIONSELLO"));
        object.setSERIESELLO(rs.getString("SERIESELLO"));
        object.setTIPOSELLO(rs.getString("TIPOSELLO"));
        object.setCOLORSELLO(rs.getString("COLORSELLO"));
        object.setESTADOSELLO(rs.getString("ESTADOSELLO"));
        object.setACCIONSELLO(rs.getString("ACCIONSELLO"));
        object.setDESTINOSELLO(rs.getString("DESTINOSELLO"));
        object.setMEDIDORASOCIADO(rs.getString("MEDIDORASOCIADO"));
		return object;
    }

    
}
