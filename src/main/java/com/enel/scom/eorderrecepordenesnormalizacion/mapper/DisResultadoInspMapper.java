package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisResultadoInspAaBean;

public class DisResultadoInspMapper implements RowMapper<DisResultadoInspAaBean> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_DIS_RESULTADO_INSP =
			"SELECT id, cod_interno "
			+ "FROM dis_resultado_insp "
			+ "WHERE upper(cod_resultado) = upper(trim(?))";
	
	@Override
	public DisResultadoInspAaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		DisResultadoInspAaBean parametroAux = DisResultadoInspAaBean.builder().build();
		parametroAux.setId(rs.getLong("ID"));
		parametroAux.setCodInterno(rs.getString("cod_interno"));
		return parametroAux;
	}

}
