package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.MedidorXmlBean;

public class MedidorXmlMapper implements RowMapper<MedidorXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR = ""
    + "WITH MEDIDOR AS (\n"
    + "    SELECT\n"
    + "        UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item', D.REG_XML)) AS XML\n"
    + "    FROM SCHSCOM.EOR_ORD_TRANSFER_DET D\n"
    + "    WHERE 1 = 1\n"
    + "        AND  ACCION = 'RECEPCION'\n"
    + "        AND ID_ORD_TRANSFER = ?\n"
    + "        AND NRO_EVENTO = ?\n"
    + ")\n"
    + "SELECT\n"
    + "      UNNEST(XPATH('/item/numeroMedidor/text()', T.XML))::TEXT AS NUMEROMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/marcaMedidor/text()', T.XML))::TEXT AS MARCAMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/modeloMedidor/text()', T.XML))::TEXT AS MODELOMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/numeroFabricaMedidor/text()', T.XML))::TEXT AS NUMEROFABRICAMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/tipoMedidor/text()', T.XML))::TEXT AS TIPOMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/tecnologiaMedidor/text()', T.XML))::TEXT AS TECNOLOGIAMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/faseMedidor/text()', T.XML))::TEXT AS FASEMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/factorMedidor/text()', T.XML))::TEXT AS FACTORMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/accionMedidor/text()', T.XML))::TEXT AS ACCIONMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/tipoMedicion/text()', T.XML))::TEXT AS TIPOMEDICION\n"
    + "    , UNNEST(XPATH('/item/ubicacionMedidor/text()', T.XML))::TEXT AS UBICACIONMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/telemedido/text()', T.XML))::TEXT AS TELEMEDIDO\n"
    + "    , UNNEST(XPATH('/item/ElementoBorneroPrueba/text()', T.XML))::TEXT AS ELEMENTOBORNEROPRUEBA\n"
    + "    , UNNEST(XPATH('/item/AnoFabricacion/text()', T.XML))::TEXT AS ANOFABRICACION\n"
    + "    , UNNEST(XPATH('/item/propiedadMedidor/text()', T.XML))::TEXT AS PROPIEDADMEDIDOR\n"
    + "    , UNNEST(XPATH('/item/fechaInstalacion/text()', T.XML))::TEXT AS FECHAINSTALACION\n"
    + "FROM \n"
    + "    MEDIDOR T\n"
    + "WHERE 1 = 1\n"
    + "    AND (XMLEXISTS('/item/accionMedidor[text() = (''Encontrado'')]' PASSING BY REF T.XML) OR XMLEXISTS('/item/accionMedidor[text() = ''Instalado'']' PASSING BY REF T.XML))\n"
    + ";";
    @Override
    @Nullable
    public MedidorXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        MedidorXmlBean object = MedidorXmlBean.builder().build();
        object.setNUMEROMEDIDOR(rs.getString("NUMEROMEDIDOR"));
        object.setMARCAMEDIDOR(rs.getString("MARCAMEDIDOR"));
        object.setMODELOMEDIDOR(rs.getString("MODELOMEDIDOR"));
        object.setNUMEROFABRICAMEDIDOR(rs.getString("NUMEROFABRICAMEDIDOR"));
        object.setTIPOMEDIDOR(rs.getString("TIPOMEDIDOR"));
        object.setTECNOLOGIAMEDIDOR(rs.getString("TECNOLOGIAMEDIDOR"));
        object.setFASEMEDIDOR(rs.getString("FASEMEDIDOR"));
        object.setFACTORMEDIDOR(rs.getString("FACTORMEDIDOR"));
        object.setACCIONMEDIDOR(rs.getString("ACCIONMEDIDOR"));
        object.setTIPOMEDICION(rs.getString("TIPOMEDICION"));
        object.setUBICACIONMEDIDOR(rs.getString("UBICACIONMEDIDOR"));
        object.setTELEMEDIDO(rs.getString("TELEMEDIDO"));
        object.setELEMENTOBORNEROPRUEBA(rs.getString("ELEMENTOBORNEROPRUEBA"));
        object.setANOFABRICACION(rs.getString("ANOFABRICACION"));
        object.setPROPIEDADMEDIDOR(rs.getString("PROPIEDADMEDIDOR"));
        object.setFechaInstalacion(rs.getString("FECHAINSTALACION"));
		return object;
    }
    
  //INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA
    public static final String SQLPOSTGRESQL_UPDATE_MEDIDORES = "UPDATE med_medida_medidor AS mmm       "
    		+ "SET id_factor = mfm.id_factor,       "
    		+ "    val_factor = mf.val_factor       "
    		+ "FROM med_medida_modelo AS mmm1       "
    		+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo       "
    		+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id       "
    		+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "  where mc.id_modelo = mmod.id          "
    		+ "  and mmod.id_marca = mmar.id          "
    		+ "  and mc.nro_componente = ?          "
    		+ "  and mmod.cod_modelo = ?       "
    		+ "  and mmar.cod_marca = ? )        "
    		+ "    AND mmm1.id_modelo = (select mmod.id        "
    		+ "from med_modelo mmod, med_marca mmar          "
    		+ "  where mmod.cod_modelo = ?           "
    		+ "  and mmar.cod_marca = ?          "
    		+ "  and mmod.id_marca = mmar.id)       "
    		+ "    AND mf.cod_factor = ?       "
    		+ "    AND mmm.id_medida = mmm1.id_medida       "
    		+ "    AND NOT EXISTS (       "
    		+ "        SELECT 1       "
    		+ "        FROM med_medida_medidor AS mm       "
    		+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "		  where mc.id_modelo = mmod.id          "
    		+ "		  and mmod.id_marca = mmar.id          "
    		+ "		  and mc.nro_componente = ?          "
    		+ "		  and mmod.cod_modelo = ?       "
    		+ "		  and mmar.cod_marca = ? )        "
    		+ "        AND NOT EXISTS (       "
    		+ "            SELECT 1       "
    		+ "            FROM med_medida_modelo AS mmm2       "
    		+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo       "
    		+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id       "
    		+ "            WHERE mmm2.id_modelo = (select mmod.id        "
    		+ "			from med_modelo mmod, med_marca mmar          "
    		+ "			  where mmod.cod_modelo = ?           "
    		+ "			  and mmar.cod_marca = ?          "
    		+ "			  and mmod.id_marca = mmar.id)       "
    		+ "            AND mf2.cod_factor = ?       "
    		+ "            AND mm.id_medida = mmm2.id_medida ));";
	
    
    public static final String SQLPOSTGRES_SELECT_MEDIDOR_ENCONTRADO = "select distinct mf.cod_factor   "
			+ "from schscom.med_componente mc,   "
			+ "schscom.med_modelo mm,   "
			+ "schscom.med_marca mm2,   "
			+ "schscom.med_medida_medidor mmm,   "
			+ "schscom.med_factor mf   "
			+ "where mc.id_modelo = mm.id   "
			+ "and mm.id_marca = mm2.id   "
			+ "and mc.id = mmm.id_componente   "
			+ "and mmm.id_factor = mf.id   "
			+ "and mm.cod_modelo = ?    "
			+ "and mm2.cod_marca = ?   "
			+ "and mc.nro_componente = ?  ";


	public static final String SQLPOSTGRESQL_COUNT_MEDIDORES =  "SELECT count(*) from med_medida_medidor AS mmm,       "
    		+ "med_medida_modelo AS mmm1       "
    		+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo       "
    		+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id       "
    		+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "  where mc.id_modelo = mmod.id          "
    		+ "  and mmod.id_marca = mmar.id          "
    		+ "  and mc.nro_componente = ?          "
    		+ "  and mmod.cod_modelo = ?       "
    		+ "  and mmar.cod_marca = ? )        "
    		+ "    AND mmm1.id_modelo = (select mmod.id        "
    		+ "from med_modelo mmod, med_marca mmar          "
    		+ "  where mmod.cod_modelo = ?           "
    		+ "  and mmar.cod_marca = ?          "
    		+ "  and mmod.id_marca = mmar.id)       "
    		+ "    AND mf.cod_factor = ?       "
    		+ "    AND mmm.id_medida = mmm1.id_medida       "
    		+ "    AND NOT EXISTS (       "
    		+ "        SELECT 1       "
    		+ "        FROM med_medida_medidor AS mm       "
    		+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar          "
    		+ "		  where mc.id_modelo = mmod.id          "
    		+ "		  and mmod.id_marca = mmar.id          "
    		+ "		  and mc.nro_componente = ?          "
    		+ "		  and mmod.cod_modelo = ?       "
    		+ "		  and mmar.cod_marca = ? )        "
    		+ "        AND NOT EXISTS (       "
    		+ "            SELECT 1       "
    		+ "            FROM med_medida_modelo AS mmm2       "
    		+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo       "
    		+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id       "
    		+ "            WHERE mmm2.id_modelo = (select mmod.id        "
    		+ "			from med_modelo mmod, med_marca mmar          "
    		+ "			  where mmod.cod_modelo = ?           "
    		+ "			  and mmar.cod_marca = ?          "
    		+ "			  and mmod.id_marca = mmar.id)       "
    		+ "            AND mf2.cod_factor = ?       "
    		+ "            AND mm.id_medida = mmm2.id_medida ))";

    
  //FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) - JEGALARZA
}
