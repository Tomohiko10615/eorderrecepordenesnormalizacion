package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.LecturaXmlBean;

public class LecturaXmlMapper implements RowMapper<LecturaXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_OPERACION = ""
    + "WITH LECTURA AS (\n"
    + "    SELECT\n"
    + "          UNNEST(XPATH('/medidores/item'\n"
    + "        , UNNEST(XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores', D.REG_XML)))) AS XML\n"
    + "    FROM \n"
    + "        SCHSCOM.EOR_ORD_TRANSFER_DET D\n"
    + "    WHERE 1 = 1\n"
    + "        AND ACCION = 'RECEPCION'\n"
    + "        AND ID_ORD_TRANSFER = ?\n"
    + "        AND NRO_EVENTO = ?\n"
    + ")\n"
    + "SELECT\n"
    + "      UNNEST(XPATH('/item/lecturas/item/tipoLectura/text()', T.XML))::TEXT AS TIPOLECTURA \n"
    + "    , UNNEST(XPATH('/item/lecturas/item/horarioLectura/text()', T.XML))::TEXT AS HORARIOLECTURA \n"
    + "    , UNNEST(XPATH('/item/lecturas/item/fechaLectura/text()', T.XML))::TEXT AS FECHALECTURA \n"
    + "    , UNNEST(XPATH('/item/lecturas/item/estadoLeido/text()', T.XML))::TEXT AS ESTADOLEIDO \n"
    + "FROM \n"
    + "    LECTURA T\n"
    + "WHERE 1 = 1\n"
    + "    AND XMLEXISTS('/item/numeroMedidor[text() = ''%s'']' PASSING BY REF T.XML)\n"
    + "    AND XMLEXISTS('/item/accionMedidor[text() = ''%s'']' PASSING BY REF T.XML)\n"
    + ";"
    ;

    @Override
    @Nullable
    public LecturaXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        LecturaXmlBean object = LecturaXmlBean.builder().build();
        object.setTIPOLECTURA(rs.getString("TIPOLECTURA"));
        object.setHORARIOLECTURA(rs.getString("HORARIOLECTURA"));
        object.setFECHALECTURA(rs.getString("FECHALECTURA"));
        object.setESTADOLEIDO(rs.getString("ESTADOLEIDO"));
		return object;
    }

    
}
