package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.TareaXmlBean;

public class TareaXmlMapper implements RowMapper<TareaXmlBean> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_EORORDTRANSFERDET_MEDIDOR = ""
    + "SELECT\n"
    + "        UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/tareas/item/codigoTarea/text()', D.REG_XML))::TEXT AS TAREA\n"
    + "      , UNNEST(XPATH('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/tareas/item/ejecutada/text()', D.REG_XML))::TEXT AS TAREAEJECUTADA\n"
    + "FROM \n"
    + "    EOR_ORD_TRANSFER_DET D\n"
    + "WHERE 1 = 1\n"
    + "    AND D.ID_ORD_TRANSFER = ?\n"
    + "    AND ACCION = 'RECEPCION'\n"
    + "    AND NRO_EVENTO = ?\n"
    + "    AND D.FEC_ACCION IN (SELECT C.FEC_OPERACION FROM EOR_ORD_TRANSFER C WHERE D.ID_ORD_TRANSFER = C.ID_ORD_TRANSFER)\n"
    + ";";

    @Override
    @Nullable
    public TareaXmlBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        TareaXmlBean aux = TareaXmlBean.builder().build();
		aux.setCODIGOTAREA(rs.getString("TAREA"));// id_empresa int8 NOT NULL
		aux.setEJECUTADA(rs.getString("TAREAEJECUTADA"));// id_empresa int8 NOT NULL
		return aux;
    }
}
