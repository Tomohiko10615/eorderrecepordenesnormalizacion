package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class EorInterfazCnrMapper {

	public static final String SQLPOSTGRESQL_SELECT_FOR_COUNT_EOR_INTERFAZ_CNR =
			"SELECT count(*) "
			+ "FROM eor_interfaz_cnr "
			+ "WHERE NUMERO_INSPECCION= ? "
			+ "AND TIPO_ORDEN in ('CORT')";
	
	public static final String SQLPOSTGRESQL_SELECT_NEXTVAL_SQORDINTERFCNR =
			"SELECT NEXTVAL('SQORDINTERFCNR')";
	
	public static final String SQLPOSTGRESQL_INSERT_INTO_EOR_INTERFAZ_CNR =
			"INSERT INTO EOR_INTERFAZ_CNR("
			+ "ID_ORD_INTERFCNR, "
			+ "NUMERO_SERVICIO, "
			+ "NUMERO_INSPECCION, "
			+ "numero_cuenta, "
			+ "tipo_orden, "
			+ "tipo_restriccion, "
			+ "motivo, "
			+ "sub_tipo_orden, "
			+ "ficticio, "
			+ "fecha_registro, "
			+ "cod_estado_orden "
			+ ")VALUES( "
			+ "?, "
			+ "?, "
			+ "?, "
			+ "?, "
			+ "?, "
			+ "null, "
			+ "?,"
			+ "?, "
			+ "'S', "
			+ "now(),"
			+ "1 "
			+ ")";
}
