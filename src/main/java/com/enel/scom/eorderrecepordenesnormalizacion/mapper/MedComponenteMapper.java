package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class MedComponenteMapper {

	public static final String SQLPOSTGRESQL_UPDATE_COMPONENTE_FUENTE_CLIENTE = "update med_componente "
			+ "set type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.Cliente' "
			+ ",id_est_componente = (select id from med_est_componente where cod_interno = 'Retirado') "
			+ ",id_ubicacion = ? "
			+ " where id = ?";

	public static final String SQLPOSTGRESQL_UPDATE_COMPONENTE_FUENTE_EJECUTOR = "update med_componente "
			+ "                     set type_ubicacion = 'com.synapsis.synergia.common.domain.Contratista' "
			+ "                        ,id_ubicacion = (select id_contratista from com_ejecutor where id_ejecutor = ?) "
			+ "                        ,id_est_componente = (select id from med_est_componente where cod_interno = 'Retirado') "
			+ "                  where id = ?";

	public static final String SQLPOSTGRESQL_UPDATE_COMPONENTE_INSTALAR = " update med_componente "
			+ "                   set type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
			+ "                      ,id_ubicacion = ? "
			+ "                      ,id_est_componente = (select id from med_est_componente where cod_interno = 'Instalado') "
			+ "                      ,id_propiedad = ? "
			+ "                      ,FECHA_SUMINISTRO = to_date(?,'YYYY-MM-DD') "
			+ "                where id = ?";

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_ELECTRICO = " select a.id "
			+ "              from med_componente a, med_marca mm, med_modelo ml "
			+ "              where a.ID_UBICACION = ? "
			+ "                and type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
			+ "                and nro_componente = ? " + "and a.id_modelo = ml.id "
			+ "and ml.id_marca = mm.id " + "and mm.cod_marca = ? " + "and ml.cod_modelo = ?";

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_COMPONENTE_DISPONIBLE = " select a.id\n"
			+ "                from med_componente a, med_est_componente b\n"
			+ "                where a.nro_componente = ?\n" + "                  and a.id_modelo =  ?\n"
			+ "                  and a.id_est_componente = b.id\n"
			+ "                  and b.cod_interno = 'Disponible'";
}
