package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

public class DisOrdNormMapper {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ID_ORD_INSP =
			"SELECT ID_ORD_INSP FROM dis_ord_norm WHERE ID_ORDEN = ?";
	
	// public static final String SQLPOSTGRESQL_UPDATE_DIS_ORD_NORM = ""
	// 		 + "Update dis_ord_norm set "
	// 	     + " ID_EJECUTOR_eje  = ? "
	// 	     + ",FEC_hora_INI_EJE  = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss') "
	// 	     + ",FEC_hora_FIN_EJE  = TO_TIMESTAMP(?, 'yyyy-MM-dd hh24:mi:ss') "
	// 	     + ",ID_RESULTADO = ? "
	// 	     + ",nro_ot_fisica = coalesce(?, nro_ot_fisica) "
	// 	     + ",fec_normalizacion = TO_TIMESTAMP(coalesce(?, fec_normalizacion), 'yyyy-MM-dd hh24:mi:ss') "
	// 	     + ",ID_ACT_COM_NOTIF = coalesce(?, ID_ACT_COM_NOTIF) "
	// 	     + ",LUGAR_NOTIFICADO =  coalesce(?, LUGAR_NOTIFICADO) "
	// 	     + "where id_orden = ?"
	// 		 ;
	public static final String SQLPOSTGRESQL_UPDATE_DIS_ORD_NORM = ""
			 + "Update dis_ord_norm set "
		     + " ID_EJECUTOR_eje  = ? "
		     + ",FEC_hora_INI_EJE  = ? "
		     + ",FEC_hora_FIN_EJE  = ? "
		     + ",ID_RESULTADO = ? "
		     + ",nro_ot_fisica = coalesce(?, nro_ot_fisica) "
		     + ",fec_normalizacion = coalesce(?, fec_normalizacion) "
		     + ",ID_ACT_COM_NOTIF = coalesce(?, ID_ACT_COM_NOTIF) "
		     + ",LUGAR_NOTIFICADO =  coalesce(?, LUGAR_NOTIFICADO) "
		     + "where id_orden = ? "
			 ;			 

}
