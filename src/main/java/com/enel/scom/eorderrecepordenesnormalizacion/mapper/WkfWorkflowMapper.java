package com.enel.scom.eorderrecepordenesnormalizacion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.WkfWorkflowBean;

public class WkfWorkflowMapper implements RowMapper<WkfWorkflowBean>{

    @Override
    @Nullable
    public WkfWorkflowBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        WkfWorkflowBean obj = WkfWorkflowBean.builder().build();
        obj.setId(rs.getLong("id"));
        obj.setIdDescriptor(rs.getString("id_descriptor"));
        obj.setVersion(rs.getLong("version"));
        obj.setIdState(rs.getString("id_state"));
        obj.setHasParent(rs.getString("has_parent"));
        obj.setIdOldState(rs.getString("id_old_state"));
        obj.setParentType(rs.getString("parent_type"));
        obj.setFechaEstado(rs.getTimestamp("fecha_estado"));
		return obj;
    }
    
}
