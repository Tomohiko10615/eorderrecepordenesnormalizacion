package com.enel.scom.eorderrecepordenesnormalizacion.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.relational.core.conversion.DbAction.Insert;
import org.springframework.stereotype.Service;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.DisOrdTareaBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorOrdTransferBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.FwkAuditeventBean;
import com.enel.scom.eorderrecepordenesnormalizacion.bean.OrdObservacionBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IDisOrdTareaDao;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorOrdTransferDao;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IFwkAuditeventDao;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IOrdObservacionDao;

@Service
public class ProbarService {
    private static final Logger logger = LogManager.getLogger(ProbarService.class);
    @Autowired
    @Qualifier("connScom")
    Connection conn;

    @Autowired
    IFwkAuditeventDao fwkAuditeventDao;
    @Autowired
    IDisOrdTareaDao disOrdTareaDao;
    @Autowired
    IEorOrdTransferDao eorOrdTransferDao;
    @Autowired
    IOrdObservacionDao ordObservacionDao;

    public void accion1() throws SQLException {
        // try {
        for (int index = 0; index < 3; index++) {
            if (proceso(index)) {
                logger.info("ok");
                logger.info("commit");

                conn.commit();

            } else {
                logger.info("ko");
                logger.info("rollback");

                conn.rollback();
            }

            insertar();
            conn.commit();
            logger.info("ok");
            logger.info("commit");
            // } catch (Exception e) {
            // logger.error("ko");
            // conn.rollback();
            // }
        }

    }

    private boolean proceso(int index) {
        try {
            fwkAuditeventDao.insert(
                    FwkAuditeventBean.builder()
                            .id(null)
                            .usecase("use case")
                            .idFk(1L)
                            .idUser(1L)
                            .build()

            );
            if (index == 2) {
                int a = 1 / 0;
            }
            disOrdTareaDao.insert(DisOrdTareaBean
                    .builder()
                    .id(disOrdTareaDao.nextVal())
                    .idOrden(1520216848644L)
                    .idAnormalidad(1L)
                    .idTarea(1L)
                    .build());
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    private void insertar() {

        ordObservacionDao.insert(OrdObservacionBean.builder()
                .idObservacion(ordObservacionDao.nextVal())
                .idOrden(1L)
                .texto("cala")
                .fechaObservacion(new java.util.Date())
                .idUsuario(3353L)
                .stateName("cala")
                .discriminator("cala")
                .build());
    }
}
