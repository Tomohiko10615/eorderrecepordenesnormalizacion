package com.enel.scom.eorderrecepordenesnormalizacion.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.EorOrdTransferBean;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorOrdTransferDao;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.IEorOrdTransferDetDao;
// @Transactional
@Service
public class ActualizacionTransferenciaService {
    private static final Logger logger = LogManager.getLogger(ActualizacionTransferenciaService.class);
    @Autowired
    IEorOrdTransferDao eorOrdTransferDao;
    @Autowired
    IEorOrdTransferDetDao eorOrdTransferdetDao;

    // @Transactional
    public void actualizarTransferencia(EorOrdTransferBean eorOrdTransferBean) {

        eorOrdTransferDao.updateTranferProcesoRecepcionaOrdenNormalizacion(
                eorOrdTransferBean);
        logger.info("Transferencia actualizada.");
        eorOrdTransferdetDao.updateTransferDetProcesoRecepcionaOrdenNormalizacion(eorOrdTransferBean.getCodOperacion(),
                eorOrdTransferBean.getIdOrdTransfer(),
                eorOrdTransferBean.getNroRecepciones());

        logger.info("Transferencia detalle actualizada.");
    }
}
