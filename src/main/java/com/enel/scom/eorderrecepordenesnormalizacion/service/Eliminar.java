package com.enel.scom.eorderrecepordenesnormalizacion.service;

public class Eliminar {

}



// struct _arreglo_1
// {
// int  indice;
// char xml_tag[100];
// char xml_data[1024];
// char es_obligatorio;
// } arr_datosxml[MAX_TAGS];

// struct _arr_ordenes
// {
// long  db_id_ord_transfer;
// long  db_nro_orden;
// long  db_nro_recepciones;
// char  db_v_variable[7];
// } arr_ordenes[MAX_ORDENES];

// struct _arreglo_tags
// {
// char  db_nom_tag[31];
// char  db_nom_columna[51];
// char  db_tipo[21];
// char  db_edelnor[2];
// char  db_enarchivo[2];
// char  db_obligatorio[2];
// int   db_posentrada;
// };

// struct _arreglo_tags  arr_tags_comunes[1000];
// struct _arreglo_tags  arr_tags_insp[1000];


// struct _arr_salida
// {
// int  posicion;
// char chr_tag_name[1024];
// char chr_tan_contenido[2048];
// } arr_tag_salida[100];



// struct _xmlDatos
// {
// char  LLAVE_SECRETA[1024];
// char  CODIGO_DISTRIBUIDORA[1024];
// char  CODIGO_SISTEMA_EXTERNO_ORIGEN[1024];
// char  CODIGO_TIPO_DE_TDC[1024];
// char  CODIGO_EXTERNO_DEL_TDC[1024];
// char  CODIGO_INTERNO_DEL_TDC[1024];
// char  CODIGO_PROCESO[1024];
// char  CODIGO_SUBPROCESO[1024];
// char  TDC_CREADO_EN_EORDER[1024];
// char  CODIGO_RESULTADO[1024];
// char  CODIGO_CAUSAL_RESULTADO[7];
// char  CODIGO_NOTA_CODIFICADA[1024];
// char  INICIO_TDC[21];
// char  FIN_TDC[21];
// char  DURACION_EJECUCION[1024];
// char  CODIGO_CUADRILLA[7];
// char  PLACA_VEHICULO[1024];
// char  NOMBRE_USUARIO[1024];
// char  APELLIDO_USUARIO[1024];
// char  MATRICULA_USUARIO[1024];
// char  LATITUD_MEDIDOR[1024];
// char  LONGITUD_MEDIDOR[1024];
// char  LATITUD_EMPALME[1024];
// char  LONGITUD_EMPALME[1024];
// char  FECHA_HORA_INICIO_CITA[1024];
// char  FECHA_HORA_FIN_CITA[1024];
// char  NOMBRE_PERSONA_CONTACTO_CITA[1024];
// char  TELEFONO_PERSONA_CONTACTO_CITA[1024];
// } xmlDatos[MAX_TAGS];

// struct _xmlOperaciones
// {
// char CODIGO_OPERACION[1024];
// char CODIGO_RESULTADO[1024];
// char CODIGO_CAUSAL_RESULTADO[1024];
// char CODIGO_NOTA_CODIFICADA[1024];
// char INICIO_OPERACION[1024];
// char FIN_OPERACION[1024];
// char DURACION_OPERACION[1024];
// char NOTAS_OPERACION[301]; /* TRC 06/04/2015 */
// } xmlOperaciones[MAX_TAGS];

// struct _xmlAnexos
// {
// char TIPO_ANEXO[1024];
// char NOMBRE_ANEXO[1024];
// char RUTA_ANEXO[1024];
// } xmlAnexos[MAX_TAGS];

// struct _xmlMedidores
// {
// char NUMEROMEDIDOR[1024];
// char MARCAMEDIDOR[1024];
// char MODELOMEDIDOR[1024];
// char NUMEROFABRICAMEDIDOR[1024];
// char TIPOMEDIDOR[1024];
// char TECNOLOGIAMEDIDOR[1024];
// char FASEMEDIDOR[1024];
// char FACTORMEDIDOR[1024];
// char ACCIONMEDIDOR[1024];
// char TIPOMEDICION[1024];
// char UBICACIONMEDIDOR[1024];
// char TELEMEDIDO[1024];
// char ELEMENTOBORNEROPRUEBA[1024];
// char ANOFABRICACION[1024];
// char PROPIEDADMEDIDOR[50];
// char fechaInstalacion[50];
// } xmlMedidores[MAX_TAGS];


// struct _sMedidores
// {
// char  cNUMEROMEDIDOR[1024];
// char  cMARCAMEDIDOR[1024];
// char  cMODELOMEDIDOR[1024];
// char  cNUMEROFABRICAMEDIDOR[1024];
// char  cTIPOMEDIDOR[1024];
// //char  cTECNOLOGIAMEDIDOR[1024];
// //char  cFASEMEDIDOR[1024];
// //char  cFACTORMEDIDOR[1024];
// char  cACCIONMEDIDOR[1024];
// //char  cTIPOMEDICION[1024];
// char  cUBICACIONMEDIDOR[1024];
// //char  cTELEMEDIDO[1024];
// //char  cELEMENTOBORNEROPRUEBA[1024];
// //char  cANOFABRICACION[1024];
// char  cPROPIEDADMEDIDOR[50];
// char  cLectENFP[12];
// char  cLectENHP[12];
// char  cLectDMFP[12];
// char  cLectDMHP[12];
// char  cLectEnRe[12];
// char fechaInstalacion[50];
// };
// struct _sMedidores medidoresRet[MAX_TAGS];
// struct _sMedidores medidoresInst[MAX_TAGS];
// struct _sMedidores medidoresAux[MAX_TAGS];


// struct _sMedidores4J
// {
// char  cNUMEROMEDIDOR[1024];
// char  cMARCAMEDIDOR[1024];   /* ANL_20200909 */
// char  cMODELOMEDIDOR[1024];  /* ANL_20200909 */
// };
// struct _sMedidores4J medidores4J[MAX_TAGS];

// struct _xmlLecturas
// {
// char TIPOLECTURA[1024];
// char HORARIOLECTURA[50];
// char FECHALECTURA[1024];
// char ESTADOLEIDO[12];
// } xmlLecturas[MAX_TAGS];

// struct _xmlSellos
// {
// char NUMEROSELLO[1024];
// char UBICACIONSELLO[1024];
// char SERIESELLO[1024];
// char TIPOSELLO[1024];
// char COLORSELLO[1024];
// char ESTADOSELLO[1024];
// char ACCIONSELLO[1024];
// char DESTINOSELLO[1024];
// char MEDIDORASOCIADO[1024];
// } xmlSellos[MAX_TAGS];

// struct _xmlGPNormalizacion
// {
// char  NroOrdenInspeccion[20];
// char  TieneCambioMed[50];
// char  CodActComercial[50];
// char  DetalleInspeccion[50];
// char  NroNotificacion[20];
// //char  CodAnormalidad[50];
// //char  CodParentesco[7];
// //char  CodEjecutor[20];
// //char  Observacion[251];
// } xmlGPNormalizacion[MAX_TAGS];

// struct _xmlAnormalidad
// {
// char CODIGOANORMALIDAD[50];
// } xmlAnormalidad[MAX_TAGS];

// struct _xmlMateriales
// {
// char CODIGOMATERIAL[50];
// char CANTIDAD[20];
// } xmlMateriales[MAX_TAGS];

// struct _xmlTareas
// {
// char CODIGOTAREA[20];
// } xmlTareas[MAX_TAGS];
