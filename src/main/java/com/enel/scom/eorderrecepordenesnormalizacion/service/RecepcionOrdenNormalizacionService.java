package com.enel.scom.eorderrecepordenesnormalizacion.service;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.enel.scom.eorderrecepordenesnormalizacion.bean.*;
import com.enel.scom.eorderrecepordenesnormalizacion.dao.*;
import com.enel.scom.eorderrecepordenesnormalizacion.dto.EntradaDto;
import com.enel.scom.eorderrecepordenesnormalizacion.dto.MedidoresXML;

import java.sql.Connection;
import java.sql.SQLException;

// @Transactional
@Service
public class RecepcionOrdenNormalizacionService {
	private static final Logger logger = LogManager.getLogger(RecepcionOrdenNormalizacionService.class);

	@Autowired
	@Qualifier("connScom")
	Connection connScom;

	@Autowired
	@Qualifier("connSc4j")
	Connection connSc4j;

	@Autowired
	ActualizacionTransferenciaService actualizacionTransferenciaService;
	@Autowired
	INucClienteDao nucClienteDao;
	@Autowired
	ISrvElectricoDao srvElectricoDao;
	@Autowired
	IMedPropiedadDao medPropiedadDao;

	@Autowired
	IMedModeloDao medModeloDao;
	@Autowired
	IMedMarcaDao medMarcaDao;
	@Autowired
	IMedidorMedidaDatoDao medidorMedidaDatoDao;
	@Autowired
	IOrdTipoOrdenDao ordTipoOrdenDao;
	@Autowired
	IComParametroDao comParametroDao;
	@Autowired
	INucEmpresaDao nucEmpresaDao;
	@Autowired
	IUsuarioDao usuarioDao;
	@Autowired
	IEorOrdTransferDao eorOrdTransferDao;
	@Autowired
	IEorOrdTransferDetDao eorOrdTransferDetDao;
	@Autowired
	ILecturaXmlDao lecturaXmlDao;
	@Autowired
	IComContratistaDao comContratistaDao;
	@Autowired
	IOperacionXmlDao operacionXmlDao;
	@Autowired
	IGpNormalizacionXmlDao gpNormalizacionXmlDao;
	@Autowired
	IMedidorXmlDao medidorXmlDao;
	@Autowired
	ISelloXmlDao selloXmlDao;
	@Autowired
	IAnormalidadXmlDao anormalidadXmlDao;
	@Autowired
	ITareaXmlDao tareaXmlDao;
	@Autowired
	IDisOrdTareaDao disOrdTareaDao;
	@Autowired
	IDisTareaDao disTareaDao;
	@Autowired
	IDisAnormalidadDao disAnormalidadDao;
	@Autowired
	IOrdOrdenDao ordOrdenDao;
	@Autowired
	IOrdInspLecturaDao ordInspLecturaDao;
	@Autowired
	IOrdMediCambioDao ordMediCambioDao;
	@Autowired
	IOrdMediInspDao ordMediInspDao;
	@Autowired
	IOrdObservacionDao ordObservacionDao;

	@Autowired
	ISrvActComercialDao srvActComercialDao;

	@Autowired
	IWkfWorkflowDao wkfWorkflowDao;
	@Autowired
	IClienteDao clienteDao;
	@Autowired
	IComCuadrillaDao comCuadrillaDao;
	@Autowired
	IComParentescoDao comParentescoDao;
	@Autowired
	IDisOrdNormDao disOrdNormDao;
	@Autowired
	IDisResultadoInspDao disResultadoInspDao;
	@Autowired
	IEorInterfazCnrDao eorInterfazCnrDao;
	@Autowired
	IFwkAuditeventDao fwkAuditeventDao;
	@Autowired
	IMedComponenteDao medComponenteDao;
	@Autowired
	IMedComponenteMedidaMedidorDao medComponenteMedidaMedidorDao;
	@Autowired
	IMedHisComponenteDao medHisComponenteDao;
	@Autowired
	IMedidor4jDao medidor4jDao;
	@Autowired
	IMedMagnitudDao medMagnitudDao;

	/****************************************************************************************************************************
	 * INICIO DEFINICION DE VARIABLES
	 ****************************************************************************************************************************/

	// #include <stdio.h>
	// #include <stdlib.h> /*TRANSFORMACION AMH; ESTA LIBRERIA DEBE SER INCLUIDA*/
	// #include <string.h> /*TRANSFORMACION AMH; ESTA LIBRERIA DEBE SER INCLUIDA*/
	// #include <sqlca.h>
	// #include <math.h>
	// #include <ctype.h>
	// #include <time.h>

	// EXEC SQL INCLUDE SQLCA;

	private static final int LOG_DEBUG = 3; // #define LOG_DEBUG 3
	private static final int LOG_FILE = 2; // #define LOG_FILE 2
	private static final int LOG_FORMAT_MESSAGE = 3; // #define LOG_FORMAT_MESSAGE 3
	private static final String PROCESO = "EOrderRecepOrdenesNormalizacion"; // #define PROCESO
																				// "EOrderRecepOrdenesNormalizacion"
	private static final String NOMBPROCESO = "NORMALIZACION"; // #define NOMBPROCESO "NORMALIZACION"
	private static final int TRUE = 1; // #define TRUE 1
	private static final int FALSE = 0; // #define FALSE 0
	private static final int MAX_ORDENES = 5000; // #define MAX_ORDENES 5000
	private static final int MAX_TAGS = 100; // #define MAX_TAGS 100

	// #define datetime(dt) {time_t tmp=time(0);struct tm
	// *tl=localtime(&tmp);strftime(dt,21,"%Y/%m/%d %H:%M:%S",tl);}

	private static final boolean DEBUG = false; // #define DEBUG FALSE
	private static final boolean DEBUG_COMMIT = false; // #define DEBUG_COMMIT FALSE //FALSE: COMMIT / TRUE: ROLLBACK
	private static final boolean DEBUG_SQL = false; // #define DEBUG_SQL FALSE //TRUE
	private static final boolean DEBUG_XML = true; // #define DEBUG_XML TRUE

	private static final int NODATAFOUND = 1403; // #define NODATAFOUND 1403

	private String fecha_hora;// char fecha_hora[30];
	private String msg_error;// char msg_error[1024];

	// EXEC SQL BEGIN DECLARE SECTION;

	private String cSubProceso = "GPN";// char cSubProceso[3]="GPN"; // Parametros de configuracion Principal para
										// Filtrar por tipo de TDC
	private String cQuery;// char cQuery[2000];

	/*** Parametros de Entrada **/
	private String db_cod_empresa;// char db_cod_empresa[30];
	private String db_conexion;// char db_conexion[30];
	private String db_cod_usuario;// char db_cod_usuario[30];
	private String db_nroODT;// char db_nroODT[30];
	private long db_id_empresa; // long db_id_empresa;
	private long db_id_usuario; // long db_id_usuario;

	private int int_max_tags_comunes;
	private int int_max_tags_insp;

	private String db_ruta_salida;

	private long vCodEstPendRecep;
	private long vCodEstEnProceso;
	private Long vCodEstRecep;
	private Long vCodEstRecepError;

	private String vCodTipoTDC;

	private Long vIdOrdTransfer;
	private Long vNroRecepciones;
	private String vNroOrden;
	private String vNroMedidor;
	private String vAccionMedidor;
	private String V_VARIABLE;
	private String vTipoOrdenLegacy;
	private String vCodOperacion;
	private String path_error;
	private String path_salida;
	private String doctoXML;

	private String vStateSeguimiento; // [40];//agregado por AVC 20150717 - solicitado por FVERA

	/***** Datos de Parametricos por cada Proceso ****/
	private String cmensaje_out; // [300];

	// char vCodOperacion[30];
	private String vObservacion; // [250];
	private String vCodErrASY000; // [30];
	private String vDesErrASY000; // [250];
	private String vCodErrASY013; // [30];
	private String vDesErrASY013; // [250];
	private String vCodErrASY024; // [30];
	private String vDesErrASY024; // [250];
	private String vCodErrASY099; // [30]; /* TRC 18/05/2015 */
	private String vDesErrASY099; // [250]; /* TRC 18/05/2015 */

	/*
	 * char cTmpCod[30]=""; char cTmpDesc[250]="";
	 */

	private int intResultRetirado;
	private int intResultInstalado;

	private int paso;
	private int paso_act;
	private int paso_veri = 0;
	private int paso_medidor;
	private int paso_tarea = 0;
	private int paso_anomalia;
	private String aCodIrregularidad; // [7];
	private String aCodTarea; // [7];
	private Long dIdIrregularidad = 0L;
	private Long dIdTarea = 0L;
	/* Inicio roliva 20042015 */
	private String cCod_Contratista; // [50];
	private String CodCuadrilla; // [251];
	private int cntcont = 0;

	private Long cNroCuenta; // [10+1];
	/* Fin roliva 20042015 */

	private Long dsec_magnitud = 0L;

	private String vTipoOrden; // [20];
	private String vTipoRestriccion; // [20];

	// EXEC SQL END DECLARE SECTION;

	private int int_cnt_ordenes = 0;
	private int int_cnt_ordenes_ant = 0;
	private int int_ordenes_procesadas = 0;
	private int int_ordenes_actualizadas = 0;
	private int int_ordenes_reenviadas = 0;
	private int int_ordenes_descartadas = 0;
	private int int_ordenes_enviadas = 0;
	private int int_ordenes_error = 0;
	private int int_cnt_tareas = 0;
	private int int_cnt_materiales = 0;
	private int int_cnt_anormalidades = 0;
	private int int_cnt_medidores = 0;
	private int int_cnt_lecturas = 0;
	private int hubo_error;
	private int es_reenvio;

	File f_error;
	File f_salida;
	private String arch_error; // [1024];

	// Inicio JERS 20211231
	private int iIdResultado;
	private int iCountCnrInterfaz;
	private String vEstadoActualSum; // [14]; // "HABILITADO" o "DESHABILITADO"
	private String vEstadoAnteriorSum; // [20];
	private Long dIdOrdenInsp;
	private Long dNroOrdenInsp;
	private Long dIdOrdInterfCnr;
	private double dMotivo;
	private String vSubtipo; // [5];
	private String vTipoInsp; // [7];
	private Long vNumeroCuenta; // [50];
	private Long vNumeroSuministro; // [50];
	// Fin JERS 20211231

	/* Funciones C */
	// private int do_errorsec(long,char *,int,int);
	/* int VerificarTipoDato(char[], char[], char[]); */
	/* int VerificarTagXML(char *,int); */
	// private int verificaParametros(int,char **);

	/* Funciones SQL */
	/******************************************************************
	 * SE COMENTA VARIABLES POR JUNTAR ARCHIVOS EN UNO SOLO:
	 ******************************************************************/
	/// Registros Principales
	private double dIdWorkflow;
	private String NumeroOrden;
	private String NumeroOrdenInsp; // [20];
	private Long NumeroServicio; // [50];
	private String NroCuenta; // [10+1];
	/* private String CodCuadrilla; //[6+1]; */ /* roliva comento 21042015 */
	private String CodResultado; // [6+1];
	private String CodResultadoCausal; // [6+1];
	private String Tdc_Creada_Campo; // [5];
	private String LLave_Secreta; // [15];
	private String CodProceso; // [5];
	private String CodSubProceso; // [5];
	private String TipoTdcLegacy; // [10];

	private String CodParentesco; // [7];
	private String CodActComercial; // [50];
	private String FechaIniEjec; // [21];
	private String FechaFinEjec; // [21];
	private String FechaNormalizacion; // [21];
	private String Observacion; // [1201]; /* TRC 06/04/2015 */
	private String Observacion_NotOpe; // [2001]; /* TRC 06/04/2015 */
	// private String CodPropiedad; //[50];
	private String NumNotificacion; // [10];
	private String NumOTFisica; // [20];
	private String TieneCambioMed; // [50];
	private String CodInternoResultado; // [20+1];
	private String LugarNotificacion; // [50];
	private String CodAnormalidad; // [50];

	private double dIdOrdenH;
	private Long dIdOrden;
	private Long dIdOrden_insp;
	private Long dIdWorkflowOrd;
	private Long dIdServicio;
	private double dIdWorkflowSeg;
	private double dIdResultado;

	private Long dIdParentesco;
	private Long dIdActComercial;

	private double dIdCuadrilla;
	private Long dIdEjecutor;
	private double dIdProducto;
	private double dIdJefeProducto;
	private double dIdCordCuadrilla;
	private String aEstadoOrden; // [60];
	private String aEstadoOrdenSeg; // [60];
	private int sw_no_seguir = FALSE;

	// @Autowired
	// IComParametroDao comParametroDao;

	// @Autowired
	// INucEmpresaDao nucEmpresaDao;

	// @Autowired
	// IUsuarioDao usuarioDao;

	// @Autowired
	// IEorOrdTransferDao eorOrdTransferDao;

	// @Autowired
	// IEorOrdTransferDetDao eorOrdTransferDetDao;

	List<DatoProcesoXmlBean> xmlDatos;
	List<OperacionXmlBean> xmlOperaciones;
	// List<> xmlAnexos;

	List<MedidorXmlBean> xmlMedidores;

	List<SMedidorXmlBean> medidoresRet;
	List<SMedidorXmlBean> medidoresInst;
	List<SMedidorXmlBean> medidoresAux;

	List<Medidor4jBean> medidores4J;

	List<LecturaXmlBean> xmlLecturas;
	List<SelloXmlBean> xmlSellos;
	List<GpNormalizacionXmlBean> xmlGPNormalizacion;
	List<AnormalidadXmlBean> xmlAnormalidad;
	// List<> xmlMateriales;
	List<TareaXmlBean> xmlTareas;

	// R.I. Agregado 27/10/2023 INICIO
	private String tiene_enfp;
	private String tiene_enhp;
	private String tiene_dmfp;
	private String tiene_dmhp;
	private String tiene_reac;
	// R.I. Agregado 27/10/2023 FIN

	// @Autowired
	// ILecturaXmlDao lecturaXmlDao;

	// @Autowired
	// IComContratistaDao comContratistaDao;

	// @Autowired
	// IOperacionXmlDao operacionXmlDao;

	// @Autowired
	// IGpNormalizacionXmlDao gpNormalizacionXmlDao;

	// @Autowired
	// IMedidorXmlDao medidorXmlDao;

	// @Autowired
	// ISelloXmlDao selloXmlDao;

	// @Autowired
	// IAnormalidadXmlDao anormalidadXmlDao;

	// @Autowired
	// ITareaXmlDao tareaXmlDao;

	public static final String RUTA_LINUX_DES_LOCAL = "D:/cfdiaze/indra/linux";

	public boolean recepcionarOrdenNormalizacion(EntradaDto entradaDto) throws SQLException {

		db_cod_empresa = entradaDto.getEmpresa();
		// db_conexion = entradaDto.getConexion();
		db_cod_usuario = entradaDto.getCodigoUsuario();
		db_nroODT = entradaDto.getNroODT();

		if (!ObtenerIDEmpresa_Usuario()) {
			return false;
		}

		logger.info("Datoso obtenidos.");
		logger.info("Id empresa: {}.", db_id_empresa);
		logger.info("Id usuario: {}.", db_id_usuario);

		if (!ObtenerEstadosTransferencia()) {
			return false;
		}

		// ObtenerMedidores();

		if (!ObtenerErroresTransferencia()) {
			return false;
		}

		if (!ObtenerRutas()) {
			return false;
		}

		arch_error = String.format("%s/EOR_INS_RECEPCION_ORD_ERRORES_%s.txt", path_error, db_nroODT);

		try {
			Files.deleteIfExists(Paths.get(arch_error));
			logger.info("Archivo eliminado.");
		} catch (IOException e) {
			logger.error("No se puede eliminar el Archivo.");
		}

		logger.info("arch_error: {}.", arch_error);
		if (!crearArchivo(arch_error)) {
			logger.error("Error: No fue posible crear archivo de errores.");
			return false;
		}
		logger.info("Archivo {} existe: {}.", arch_error, Files.exists(Paths.get(arch_error)));

		// if (!TransferenciaOrden()) {
		// return false;
		// }

		TransferenciaOrden();

		logger.info("Resumen final.");
		logger.info("Ordenes procesadas: {}.", int_ordenes_procesadas);
		logger.info("Ordenes actualizadas: {}.", int_ordenes_actualizadas);
		logger.info("Ordenes erradas: {}.", int_ordenes_error);

		// fprintf(f_error,"%s\tFinaliza Proceso.\n\n",fecha_hora);
		String pattern = "MM/dd/yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		escribirArchivo(arch_error,
				String.format("%s Finaliza Proceso%s", df.format(new Date()), System.lineSeparator()));

		return true;
	}

	private boolean TransferenciaOrden() throws SQLException {
		logger.info("Buscando ordenes de {}.", NOMBPROCESO);
		// ObtenerOrdenes(); declaracion de consulta
		logger.info("Comenzando a Recorrer Ordenes de {}.", NOMBPROCESO);

		int_ordenes_procesadas = 0;
		int_ordenes_actualizadas = 0;
		int_ordenes_enviadas = 0;
		int_ordenes_reenviadas = 0;
		int_ordenes_error = 0;
		int_ordenes_descartadas = 0;

		// PrepareCursoresProceso(); declaracion de varias consultas
		// PrepareObtenerContratista(); declaracion de consulta

		logger.info("Consulta Principal.");

		List<EorOrdTransferBean> ordenesTransfer = eorOrdTransferDao
				.findByCodEstadoOrdenAndCodTipoOrdenEorder(vCodEstPendRecep, cSubProceso);
		logger.info("Cantidad encontrada: {}", ordenesTransfer.size());
		logger.info("Inicio de iteracion.");
		for (EorOrdTransferBean ordenTransfer : ordenesTransfer) {
			logger.info(
					"*****************************************************************************************************************************************************************************************");
			logger.info(
					"*****************************************************************************************************************************************************************************************");
			logger.info(
					"*****************************************************************************************************************************************************************************************");
			logger.info(
					"*****************************************************************************************************************************************************************************************");
			logger.info(
					"*****************************************************************************************************************************************************************************************");

			try {

				int_ordenes_procesadas++;
				logger.info("Iteracion: {}", int_ordenes_procesadas);

				vIdOrdTransfer = ordenTransfer.getIdOrdTransfer();
				vNroOrden = ordenTransfer.getNroOrdenLegacy();
				// vNroOrden = Long.parseLong(ordenTransfer.getNroOrdenLegacy());
				vNroRecepciones = ordenTransfer.getNroRecepciones();
				V_VARIABLE = ordenTransfer.getCodTipoOrdenEorder();
				vTipoOrdenLegacy = ordenTransfer.getCodTipoOrdenLegacy();

				logger.info("IdOrdTransfer: {}.", vIdOrdTransfer);
				logger.info("NroOrdenLegacy: {}.", vNroOrden);
				logger.info("NroRecepciones: {}.", vNroRecepciones);
				logger.info("CodTipoOrdenEorder: {}.", V_VARIABLE);
				logger.info("CodTipoOrdenLegacy: {}.", vTipoOrdenLegacy);

				paso = 0;

				logger.info("({}) IdOrdTransfer({}) Nro Orden({})", int_ordenes_procesadas, vIdOrdTransfer, vNroOrden);

				if (!ObtenerTipoTDC()) {
					return false;
				}

				ObtenerDatos();

				if (validarContratista()) {
					logger.info("Contratista validado con éxito");
					vCodOperacion = vCodErrASY000;
					vObservacion = vDesErrASY000;
					hubo_error = FALSE;
					ObtenerOperaciones();
					// ObtenerAnexos();
					ObtenerSellos();
					ObtenerGPNormalizacion();

					if (InicioProceso()) {
						// commit;
						int_ordenes_actualizadas++;
						// borrar/comentar
						connScom.commit();
						connSc4j.commit();
						// connScom.rollback();
						// connSc4j.rollback();
						logger.info(
								"COMMIT WORK. Paso de seguimiento :({})-->Fueron guardados los resultados de la Orden: {}.",
								vIdOrdTransfer, vNroOrden);
						logger.info("Actualiza Synergia Correctamente.");
					} else {
						// logger.info("Rollback");
						// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

						// rollback;
						connScom.rollback();
						connSc4j.rollback();
						logger.info(
								"ROLLBACK WORK. ({})-->No se guardaron los resultados de la Orden: {}.Paso Actualizacion {}.",
								vIdOrdTransfer, vNroOrden, paso_act);
						logger.info("No se pudo Actualizar Synergia.");
						int_ordenes_error++;
					}

				} else {
					logger.info("No se valido el contratista");
					int_ordenes_error++;
				}

			} catch (Exception e) {
				logger.error("Error al procesar");
				logger.error("Error mensaje: {}", e.getMessage());
				logger.error("Fallo! en etapa 1", e);
				vObservacion = vDesErrASY099;
				vCodOperacion = vCodErrASY099;
				int_ordenes_error++;
				connScom.rollback();
				connSc4j.rollback();
			}

			try {
				ActualizoTransferencia2();
				// borrar/comentar
				connScom.commit();
				// connScom.rollback();

				// commit;
				logger.info("({})-->Actualiza Estados de Transferencias: {} .\n", vIdOrdTransfer, vNroOrden);
			} catch (Exception e) {
				logger.error("Error al actualizar las datos de la transferencia");
				logger.error("Error mensaje: {}", e.getMessage());
				logger.error("Fallo! en etapa 2", e);

				connScom.rollback();
			}

		}
		logger.info("Fin de iteracion");
		// logger.info("Resumen");
		// logger.info("Total procesadas: {}", int_ordenes_procesadas);
		// logger.info("Total actualizadas: {}", int_ordenes_actualizadas);
		// logger.info("Total erradas: {}", int_ordenes_procesadas);
		return true;
	}

	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	private void ActualizoTransferencia() {

		// eorOrdTransferDao.update(vCodOperacion, Tdc_Creada_Campo, NumeroOrden,
		// vCodErrASY000, vCodEstRecep, vCodEstRecepError, vObservacion, cNroCuenta,
		// vCodEstPendRecep, vCodTipoTDC, vIdOrdTransfer);
		logger.info("vCodOperacion: {}.", vCodOperacion);
		logger.info("NumeroOrden: {}.", NumeroOrden);
		logger.info("vCodErrASY000: {}.", vCodErrASY000);
		logger.info("vCodEstRecep: {}.", vCodEstRecep);
		logger.info("vCodEstRecepError: {}.", vCodEstRecepError);
		logger.info("vObservacion: {}.", vObservacion);
		logger.info("cNroCuenta: {}.", cNroCuenta);
		logger.info("vCodEstPendRecep: {}.", vCodEstPendRecep);
		logger.info("vCodTipoTDC: {}.", vCodTipoTDC);
		logger.info("vIdOrdTransfer: {}.", vIdOrdTransfer);
		logger.info("Tdc_Creada_Campo: {}.", Tdc_Creada_Campo);

		eorOrdTransferDao.updateTranferProcesoRecepcionaOrdenNormalizacion(
				EorOrdTransferBean.builder().codOperacion(vCodOperacion).nroOrdenLegacy(NumeroOrden)
						.codEstadoOrden(vCodOperacion.equals(vCodErrASY000) ? vCodEstRecep : vCodEstRecepError)
						.observaciones(vObservacion).codTipoOrdenLegacy("NORM").nroCuenta(cNroCuenta)
						.codEstadoOrdenAnt(vCodEstPendRecep).codTipoOrdenEorder(vCodTipoTDC)
						.idOrdTransfer(vIdOrdTransfer).nroOrdenEorder(Long.parseLong(Tdc_Creada_Campo)).build());

		eorOrdTransferDetDao.updateTransferDetProcesoRecepcionaOrdenNormalizacion(vCodOperacion, vIdOrdTransfer,
				vNroRecepciones);
	}

	// @Transactional
	private void ActualizoTransferencia2() {

		logger.info("vCodOperacion: {}.", vCodOperacion);
		logger.info("NumeroOrden: {}.", NumeroOrden);
		logger.info("vCodErrASY000: {}.", vCodErrASY000);
		logger.info("vCodEstRecep: {}.", vCodEstRecep);
		logger.info("vCodEstRecepError: {}.", vCodEstRecepError);
		logger.info("vObservacion: {}.", vObservacion);
		logger.info("cNroCuenta: {}.", cNroCuenta);
		logger.info("vCodEstPendRecep: {}.", vCodEstPendRecep);
		logger.info("vCodTipoTDC: {}.", vCodTipoTDC);
		logger.info("vIdOrdTransfer: {}.", vIdOrdTransfer);
		logger.info("Tdc_Creada_Campo: {}.", Tdc_Creada_Campo);

		actualizacionTransferenciaService.actualizarTransferencia(EorOrdTransferBean.builder()
				.codOperacion(vCodOperacion).nroOrdenLegacy(NumeroOrden != null ? NumeroOrden : "")
				.codEstadoOrden(vCodOperacion.equals(vCodErrASY000) ? vCodEstRecep : vCodEstRecepError)
				.observaciones(vObservacion).codTipoOrdenLegacy("NORM").nroCuenta(cNroCuenta)
				.codEstadoOrdenAnt(vCodEstPendRecep).codTipoOrdenEorder(vCodTipoTDC).idOrdTransfer(vIdOrdTransfer)
				.nroOrdenEorder(Long.parseLong(Tdc_Creada_Campo)).nroRecepciones(vNroRecepciones).build());

	}

	// @Transactional
	private boolean InicioProceso() {
		logger.info("InicioProceso");
		try {

			// bzero(&cNroCuenta,sizeof(cNroCuenta));
			cNroCuenta = null;
			paso_medidor = 0;
			ParametrosOrden();

			if (!ObtenerMedidores()) {
				// error_indra = true;}
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701

			if (!ObtenerLecturas()) {
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150820
			if (!ValidarRecepciones()) { // aaaaaaaaaaaaaaaaaaaaaaa
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701
			if (!ValidarMedidorRetirar()) { // aaaaaaaaaaaaaaaaaaaaaaa
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150820

			// INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) -
			// JEGALARZA
			// FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) -
			// JEGALARZA

			SetearContadores();
			if (!ObtenerAnormalidades()) {
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701
			if (!ObtenerTareas()) {
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701
			if (!ValidarOrdenSinTareas()) {
				return false;
			} // agregado por DAR 20220929
			if (!ActualizarOrden()) { // falta
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701
			if (!GrabarTareas()) { // falta
				// error_indra = true;
				// TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			} // agregado por AVC 20150701
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Fallo! en etapa 3", e);
			logger.error("Error en el proceso principal. {}.", e.getMessage());
			logger.error("Traza 1.");
			vObservacion = vDesErrASY099;
			vCodOperacion = vCodErrASY099;
			return false;
			// throw e;
		}
		return true;// agregado por AVC 20150701
	}

	// agregado por DAR 20220929
	private boolean ValidarOrdenSinTareas() {
		if (strcmp(CodResultado, "REA") == 0) {
			if (!ValidarNormalizacionRealizada())
				return false;
		} else {
			if (!ValidarNormalizacionNoRealizada())
				return false;
		}

		return true;
	}

	private boolean ValidarNormalizacionRealizada() {
		int contarTareasEjecutadas = 0;
		int contarTareasNOEjecutadas = 0;
		int x;

		for (x = 0; x < int_cnt_tareas; x++) {
			if (strcmp(xmlTareas.get(x).getEJECUTADA(), "SI") == 0) {
				contarTareasEjecutadas++;
			}
		}

		if (contarTareasEjecutadas == 0) {
			vObservacion = "Error: Normalización Realizada sin tareas ejecutadas, operación no permitida)";
			vCodOperacion = "ASY045";
			return false;
		}

		for (x = 0; x < int_cnt_tareas; x++) {
			if (contarTareasEjecutadas == 1 && strcmp(xmlTareas.get(x).getEJECUTADA(), "SI") == 0
					&& strcmp(xmlTareas.get(x).getCODIGOTAREA(), "0") == 0) {
				vObservacion = "Error: Normalización Realizada con unica tarea 0, operación no permitida)";
				vCodOperacion = "ASY045";
				return false;
			}
		}

		return true;
	}

	private boolean ValidarNormalizacionNoRealizada() {

		int x = 0;
		int contarTareasPendiente = 0;

		for (x = 0; x < int_cnt_tareas; x++) {
			if (strcmp(xmlTareas.get(x).getEJECUTADA(), "NO") == 0) {
				contarTareasPendiente++;
			}
		}

		if (contarTareasPendiente == 0) {
			vObservacion = "Error: Normalización No Realizada sin tareas pendientes, operación no permitida)";
			vCodOperacion = "ASY045";
			return false;
		}

		for (x = 0; x < int_cnt_tareas; x++) {
			if (contarTareasPendiente == 1 && strcmp(xmlTareas.get(x).getEJECUTADA(), "NO") == 0
					&& strcmp(xmlTareas.get(x).getCODIGOTAREA(), "0") == 0) {
				vObservacion = "Error: Normalización No realizada con unica tarea 0, operación no permitida)";
				vCodOperacion = "ASY045";
				return false;
			}
		}

		return true;
	}
	// agregado por DAR 20220929

	private boolean ActualizarOrden() {
		logger.info("Metodo ActualizarOrden inicio.");
		int paso_actualizacion = 0;
		/*
		 * R.I. Comentado 27/10/2023 String tiene_enfp; String tiene_enhp; String
		 * tiene_dmfp; String tiene_dmhp; String tiene_reac;
		 */
		int tipo_magnitud = 4;
		Double id_aux = 0.0;
		Long dIdMedida1 = 0L;
		Long dEnteros1 = 0L;
		Long dDecimales1 = 0L;
		Double dFactor1 = 0.0;
		Long dIdTipoCalculo1 = 0L;
		Long dIdMedida2 = 0L;
		Long dEnteros2 = 0L;
		Long dDecimales2 = 0L;
		Double dFactor2 = 0.0;
		Long dIdTipoCalculo2 = 0L;
		Long dIdMedida3 = 0L;
		Long dEnteros3 = 0L;
		Long dDecimales3 = 0L;
		Double dFactor3 = 0.0;
		Long dIdTipoCalculo3 = 0L;
		Long dIdMedida4 = 0L;
		Long dEnteros4 = 0L;
		Long dDecimales4 = 0L;
		Double dFactor4 = 0.0;
		Long dIdTipoCalculo4 = 0L;
		Long dIdMedida5 = 0L;
		Long dEnteros5 = 0L;
		Long dDecimales5 = 0L;
		Double dFactor5 = 0.0;
		Long dIdTipoCalculo5 = 0L;
		String spropiedad_ret;
		String cQuery;
		String aux_error;
		Long aux_id;
		Long aux_id_magnitud;
		Long aux_id_medi_insp;
		int error;

		dsec_magnitud = 0L;
		error = 0;

		try {
			ordOrdenDao.updateFechaFinalizacionById(dIdOrden, FechaFinEjec);

		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 1;
			vObservacion = String.format("Error al insertar Estado de la Orden(Workflow). Nro. Orden: %s .",
					NumeroOrden);
			vCodOperacion = "ASY045";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		// chanfle

		try {
			wkfWorkflowDao.updateIdStateByIdWorkflow(dIdWorkflowOrd, "SFinalizada");

		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 2;
			// sprintf(vObservacion,"%s Error al insertar Estado de la Orden(Workflow). Nro.
			// Orden: %d . ",vObservacion,NumeroOrden);
			vObservacion = "Error al insertar Estado de la Orden(Workflow)";
			vCodOperacion = "ASY045";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		vStateSeguimiento = "";

		try {
			WkfWorkflowBean wkfWorkflowBean = wkfWorkflowDao.findById(dIdWorkflowOrd);
			vStateSeguimiento = wkfWorkflowBean.getIdState();
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 3;
			vObservacion = String.format("Error al obtener estado de seguimiento");
			vCodOperacion = "ASY045";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;
		}

		if (strcmp(vStateSeguimiento, "SInspGenerada") == 0 || strcmp(vStateSeguimiento, "SInspInformada") == 0) {
			error = 1;
			paso_actualizacion = 3;
			vObservacion = String.format("Estado de seguimiento incorrecto (", vStateSeguimiento, ")");
			vCodOperacion = "ASY045";
			logger.info("Validando : {} . \n", vObservacion);
			return false;
		} else {
			if (strcmp(vStateSeguimiento, "SNormGenerada") == 0) {

				try {
					wkfWorkflowDao.updateIdStateByIdWorkflowAndIdState(dIdWorkflowOrd, "SNormGenerada",
							"SNormSeguimiento");
				} catch (Exception e) {
					error = 1;
					paso_actualizacion = 3;
					vObservacion = String.format("Error al actualizar Estado de seguimiento (%s)", vStateSeguimiento);
					vCodOperacion = "ASY045";
					logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
					return false;
				}
			} // fin if SNormGenerada
		}

		error = 0;

		try {
			fwkAuditeventDao.insert(FwkAuditeventBean.builder().id(null)
					.usecase(String.format("RecepMasivaNormalizacion(SE:'%s')", NumeroOrden))
					.objectref("com.synapsis.synergia.dis.domain.OrdenNormalizacion").idFk(dIdOrden)
					.fechaEjecucion(null).specificAuditevent(null).idUser(db_id_usuario).idEmpresa(1L).build());
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 4;
			// sprintf(vObservacion,"%s Error al insertar la auditoria. Nro. Orden: %d
			// .",vObservacion,NumeroOrden);
			vObservacion = "Error al insertar la auditoria";
			vCodOperacion = "ASY028";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		try {
			disOrdTareaDao.updateTareaEstadoByIdOrden(dIdOrden);
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 5;
			// sprintf(vObservacion,"%s Error al desactivar Tareas. Nro. Orden: %d
			// .",vObservacion,NumeroOrden);
			vObservacion = "Error al momento de desactivar Tareas";
			vCodOperacion = "ASY028";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		try {
			aux_id = ordObservacionDao.nextVal();
			logger.info("Fecha a insertar en OrdObservacion: {}", FechaFinEjec);
			DateFormat fomatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // acaaca

			logger.info("Datos a insertar en la observacion de la orden {}", dIdOrden);
			logger.info("aux_id: {}", aux_id);
			logger.info("dIdOrden: {}", dIdOrden);
			logger.info("Observacion: {}", Observacion);
			logger.info("FechaFinEjec: {}", FechaFinEjec);
			logger.info("db_id_usuario: {}", db_id_usuario);
			logger.info("db_id_empresa: {}", db_id_empresa);

			ordObservacionDao.insert(OrdObservacionBean.builder().idObservacion(aux_id).idOrden(dIdOrden)
					.texto(Observacion == null ? "Normalizacion desde eOrder" : Observacion)
					.fechaObservacion(fomatoFecha.parse(FechaFinEjec)).idUsuario(db_id_usuario).idEmpresa(db_id_empresa)
					.stateName("SFinalizada").discriminator("ObeservacionOrden").build());
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 6;
			// sprintf(vObservacion,"%s Error al insertar la observacion. Nro. Orden: %d
			// .",vObservacion,NumeroOrden);
			vObservacion = "Error al insertar la observacion";
			vCodOperacion = "ASY029";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			logger.error("Error: {}", e);
			return false;// agregado por AVC 20150701
		}

		if (strcmp(TieneCambioMed, "S") == 0) {
			tipo_magnitud = 2; // de retiro

			if ((intResultRetirado > 0) || (intResultInstalado > 0))// CASO: Existe medidor a instalar o
																	// retirar/normalizar
			{
				error = 0;
				try {
					dsec_magnitud = srvElectricoDao.getSecMagnitudByIdServicio(dIdServicio);
					if (dsec_magnitud == null) {

						throw new Exception();
					}
				} catch (Exception e) {
					error = 1;
					paso_actualizacion = 7;
					vObservacion = String.format("No pudo obtenerse Secuencia de Lectura");
					vCodOperacion = "ASY030";
					logger.info("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
					return false;
				}
			}
		}

		// logger.info("Hasta aquí se ha probado ELIMINAR SYSTEM.EXIT(0)"); Richard
		// System.exit(0);

		logger.info("TieneCambioMed: {}", intResultRetirado);
		logger.info("intResultRetirado: {}", intResultRetirado);
		logger.info("intResultInstalado: {}", TieneCambioMed);

		if ((intResultRetirado > 0) && (strcmp(TieneCambioMed, "S") == 0)) {
			if (!RetirarMedidor(tipo_magnitud)) {
				return false;
			}
		}

		if ((intResultInstalado > 0) && (strcmp(TieneCambioMed, "S") == 0)) {
			if (!InstalarMedidor(tipo_magnitud)) {
				return false;
			}
			if (!cambioDeFactor()) {
				return false;
			}
		}

		if ((intResultRetirado > 0) || (intResultInstalado > 0)) {
			error = 0;
			try {
				srvElectricoDao.updateSecMagnitudByIdServicio(dIdServicio, dsec_magnitud);
			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 603;
				vObservacion = "Error al Update secuencia del srv_electrico";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;
			}

		}

		/****/

		/****/

		DateFormat fomatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {

			Long nroOtFisica = NumOTFisica == null ? null : Long.parseLong(NumOTFisica);
			Date fechaNormalizacion = FechaNormalizacion == null ? null : fomatoFecha.parse(FechaNormalizacion);
			Long idActComNotif = dIdActComercial == null ? null : dIdActComercial;
			String lugarNotificado = LugarNotificacion == null ? null : LugarNotificacion;

			disOrdNormDao.update(DisOrdNormBean.builder().idEjecutorEje(Double.valueOf(dIdEjecutor).longValue())
					.fecHoraIniEje(fomatoFecha.parse(FechaIniEjec)).fecHoraFinEje(fomatoFecha.parse(FechaFinEjec))
					.idResultado(Double.valueOf(dIdResultado).longValue()).nroOtFisica(nroOtFisica)
					.fecNormalizacion(fechaNormalizacion).idActComNotif(idActComNotif).idParentNotificado(dIdParentesco)
					.lugarNotificado(LugarNotificacion).idOrden(Double.valueOf(dIdOrden).longValue()).build());
		} catch (Exception e) {
			paso_act = 99;
			cmensaje_out = String.format(
					"Error: Actualizando orden Norm(Gestion de Perdidas). - Paso: %d - SQLCODE = %s. ", paso_act,
					e.getMessage());
			logger.info("%s", cmensaje_out);
			vObservacion = cmensaje_out;
			vCodOperacion = "ASY002";
			return false;
		}

		try {
			dIdOrdenInsp = disOrdNormDao.findByIdOrden(dIdOrden);
		} catch (Exception e) {
			vObservacion = "No pudo obtenerse el ID de Orden de Inspeccion.";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;
		}

		try {
			OrdOrdenBean ordOrdenBean = ordOrdenDao.findByIdOrdenAndDiscriminador(dIdOrdenInsp, "INSPECCION");
			dNroOrdenInsp = Long.parseLong(ordOrdenBean.getNroOrden());
		} catch (Exception e) {
			vObservacion = "No pudo obtenerse el Nro de Orden de Inspeccion.";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;
		}

		try {
			iCountCnrInterfaz = eorInterfazCnrDao.getCountByNumeroInspeccionAndTipoOrden(dNroOrdenInsp);
		} catch (Exception e) {
			vObservacion = "No pudo obtenerse el ID de Resultado de Inspeccion.";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;
		}

		logger.info("V_VARIABLE: {}.", V_VARIABLE);
		logger.info("iCountCnrInterfaz: {}.", iCountCnrInterfaz);

		if ((strcmp(V_VARIABLE, "NOR.01") == 0 || (strcmp(V_VARIABLE, "NOR.03") == 0)) && (iCountCnrInterfaz > 0)) {

			vNumeroSuministro = NumeroServicio;

			try {
				ComParametroBean comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "MOTIVO",
						"REPO");
				vTipoOrden = comParametroBean.getCodigo();
				dMotivo = Double.parseDouble(comParametroBean.getDescripcion());
			} catch (Exception e) {
				vObservacion = String.format("Error: %s - No se encuentra Tipo de Orden.", e.getMessage());
				return false;
			}

			try {
				ClienteBean clienteBean = clienteDao.findByNroServicio(vNumeroSuministro);
				vNumeroSuministro = clienteBean.getNroServicio();
				vNumeroCuenta = clienteBean.getNroCuenta();
			} catch (Exception e) {
				vObservacion = String.format(
						"Error: %s - No se encuentra Nro. de servicio, Nro. Cuenta, Tipo de Orden ni Tipo de Restriccion.",
						e.getMessage());
				vCodOperacion = "ASY030";
				return false;
			}

			try {
				ComParametroBean comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER",
						"SUBTIPO", "ORD_REPO_F");
				// vTipoOrden = comParametroBean.getCodigo();
				// dMotivo = Double.parseDouble(comParametroBean.getDescripcion());
				vSubtipo = comParametroBean.getValor_alf();
			} catch (Exception e) {
				vObservacion = String.format("Error: %s - No se encuentra el Subtipo.", e.getMessage());
				return false;
			}

			try {
				dIdOrdInterfCnr = eorInterfazCnrDao.nextVal();
			} catch (Exception e) {
				vObservacion = String.format("Error: %s - No se encuentra Secuencia para eor_interfaz_cnr.",
						e.getMessage());
				vCodOperacion = "ASY045";
				return false;
			}

			try {
				eorInterfazCnrDao.insert(
						EorInterfazCnrBean.builder().idOrdInterfcnr(dIdOrdInterfCnr).numeroServicio(vNumeroSuministro)
								.numeroInspeccion(Double.valueOf(dNroOrdenInsp).longValue()).numeroCuenta(vNumeroCuenta)
								.tipoOrden(vTipoOrden).tipoRestriccion(null).motivo(String.valueOf(dMotivo))
								.subTipoOrden(vSubtipo).ficticio("S").fechaRegistro(null).codEstadoOrden(1L).build());

			} catch (Exception e) {
				vObservacion = String.format("Error: %s - No se pudo insertar los datos en la tabla EOR_INTERFAZ_CNR.",
						e.getMessage());
				vCodOperacion = "ASY030";
				return false;
			}

		}
		logger.info("Metodo ActualizarOrden inicio.");
		return true;
	}

	private boolean InstalarMedidor(int tipoMagnitud) {
		logger.info("InstalarMedidor({})", tipoMagnitud);
		int i_tipo_magnitud;
		String vObs;
		String vTipLec;

		int paso_actualizacion = 0;
		/*
		 * R.I. Comentado 27/10/2023 String tiene_enfp; String tiene_enhp; String
		 * tiene_dmfp; String tiene_dmhp; String tiene_reac;
		 */

		int tipo_magnitud = 4;
		String spropiedad_ret;

		Long dIdTipoCalculo1 = 0L;
		Long dIdTipoCalculo2 = 0L;
		Long dIdTipoCalculo3 = 0L;
		Long dIdTipoCalculo4 = 0L;
		Long dIdTipoCalculo5 = 0L;

		Long dIdMedida1 = 0L;
		Long dIdMedida2 = 0L;
		Long dIdMedida3 = 0L;
		Long dIdMedida4 = 0L;
		Long dIdMedida5 = 0L;

		Long dDecimales1 = 0L;
		Long dDecimales2 = 0L;
		Long dDecimales3 = 0L;
		Long dDecimales4 = 0L;
		Long dDecimales5 = 0L;

		Long dEnteros1 = 0L;
		Long dEnteros2 = 0L;
		Long dEnteros3 = 0L;
		Long dEnteros4 = 0L;
		Long dEnteros5 = 0L;

		BigDecimal dFactor1 = new BigDecimal(0);
		BigDecimal dFactor2 = new BigDecimal(0);
		BigDecimal dFactor3 = new BigDecimal(0);
		BigDecimal dFactor4 = new BigDecimal(0);
		BigDecimal dFactor5 = new BigDecimal(0);

		Long aux_id;
		Long aux_id_magnitud;
		Long aux_id_medi_insp;
		int error;
		int xx;
		String NroComponente_i;
		String CodMarca_i;
		String CodModelo_i;

		Long dIdModelo;
		Long dIdComponente_i;
		String CodPropiedad;
		Long dIdPropiedad;
		Long dsecMagnitud = 0L;
		String fechaInstalacion;
		int iSeRegistroLectura = 0;
		i_tipo_magnitud = tipoMagnitud;

		logger.info("medidoresInst: {}", medidoresInst);
		for (SMedidorXmlBean medidorInst : medidoresInst) {
			logger.info("medidorInst: {}", medidorInst);
			NroComponente_i = medidorInst.getCNUMEROMEDIDOR();
			CodModelo_i = medidorInst.getCMODELOMEDIDOR();
			CodMarca_i = medidorInst.getCMARCAMEDIDOR();
			CodPropiedad = medidorInst.getCPROPIEDADMEDIDOR();
			fechaInstalacion = medidorInst.getFechaInstalacion();

			dIdComponente_i = 0L;
			dIdModelo = 0L;
			dIdPropiedad = 0L;
			iSeRegistroLectura = 0;

			if (strcmp(CodModelo_i, "") != 0) {
				try {
					dIdModelo = medModeloDao.getIdModelByCodModelAndCodMarca(CodModelo_i, CodMarca_i);

				} catch (Exception e) {
					error = 1;
					vObservacion = String.format("Modelo no existe. (%s)", CodModelo_i);
					vCodOperacion = "ASY045";
					return false;
				}

			}

			if (strcmp(NroComponente_i, "") != 0) {

				try {
					dIdComponente_i = medComponenteDao.getIdComponenteDisponibleByNroComponenteAndIdModeloAndIdEmpresa(
							NroComponente_i, dIdModelo);

				} catch (Exception e) {
					error = 1;
					vObservacion = String.format("Componente no existe o no está disponible (%s)", NroComponente_i);
					vCodOperacion = "ASY045";
					return false;
				}

			} else {
				error = 1;
				vObservacion = String.format("El cambio de medidor requiere el medidor a instalar", NroComponente_i);
				vCodOperacion = "ASY045";
				return false;
			}
			if (strcmp(CodPropiedad, "") != 0) {

				try {
					MedPropiedadBean medPropiedadBean = medPropiedadDao.findByCodPropiedadAndIdEmpresa(CodPropiedad,
							1L);
					dIdPropiedad = medPropiedadBean.getId();
				} catch (Exception e) {
					vObservacion = String.format("Propiedad no existe. (%s)", CodPropiedad);
					vCodOperacion = "ASY045";
					return false;
				}

			} // fin propiedad

			error = 0;
			try {
				MedidorMedidaDatoBean medidorMedidaDatoBean = medidorMedidaDatoDao
						.findByIdComponenteInstalar(dIdComponente_i);
				/*
				 * R.I. Comentado 27/10/2023 Ahora se evaluará con los horarios de lectura del
				 * medidor a retirar tiene_enfp = medidorMedidaDatoBean.getTieneMedidaEnfp();
				 * tiene_enhp = medidorMedidaDatoBean.getTieneMedidaEnhp(); tiene_dmfp =
				 * medidorMedidaDatoBean.getTieneMedidaDmfp(); tiene_dmhp =
				 * medidorMedidaDatoBean.getTieneMedidaDmhp(); tiene_reac =
				 * medidorMedidaDatoBean.getTieneMedidaReac();
				 */
				dIdMedida1 = medidorMedidaDatoBean.getIdMedida1();
				dEnteros1 = medidorMedidaDatoBean.getCantEnteros1();
				dDecimales1 = medidorMedidaDatoBean.getCantDecimales1();
				dFactor1 = medidorMedidaDatoBean.getValFactor1();
				dIdTipoCalculo1 = medidorMedidaDatoBean.getIdTipCalculo1();
				dIdMedida2 = medidorMedidaDatoBean.getIdMedida2();
				dEnteros2 = medidorMedidaDatoBean.getCantEnteros2();
				dDecimales2 = medidorMedidaDatoBean.getCantDecimales2();
				dFactor2 = medidorMedidaDatoBean.getValFactor2();
				dIdTipoCalculo2 = medidorMedidaDatoBean.getIdTipCalculo2();
				dIdMedida3 = medidorMedidaDatoBean.getIdMedida3();
				dEnteros3 = medidorMedidaDatoBean.getCantEnteros3();
				dDecimales3 = medidorMedidaDatoBean.getCantDecimales3();
				dFactor3 = medidorMedidaDatoBean.getValFactor3();
				dIdTipoCalculo3 = medidorMedidaDatoBean.getIdTipCalculo3();
				dIdMedida4 = medidorMedidaDatoBean.getIdMedida4();
				dEnteros4 = medidorMedidaDatoBean.getCantEnteros4();
				dDecimales4 = medidorMedidaDatoBean.getCantDecimales4();
				dFactor4 = medidorMedidaDatoBean.getValFactor4();
				dIdTipoCalculo4 = medidorMedidaDatoBean.getIdTipCalculo4();
				dIdMedida5 = medidorMedidaDatoBean.getIdMedida5();
				dEnteros5 = medidorMedidaDatoBean.getCantEnteros5();
				dDecimales5 = medidorMedidaDatoBean.getCantDecimales5();
				dFactor5 = medidorMedidaDatoBean.getValFactor5();
				dIdTipoCalculo5 = medidorMedidaDatoBean.getIdTipCalculo5();

				logger.info("tiene_enfp: {}", tiene_enfp);
				logger.info("tiene_enhp: {}", tiene_enhp);
				logger.info("tiene_dmfp: {}", tiene_dmfp);
				logger.info("tiene_dmhp: {}", tiene_dmhp);
				logger.info("tiene_reac: {}", tiene_reac);
				logger.info("dIdMedida1: {}", dIdMedida1);
				logger.info("dEnteros1: {}", dEnteros1);
				logger.info("dDecimales1: {}", dDecimales1);
				logger.info("dFactor1: {}", dFactor1);
				logger.info("dIdTipoCalculo1: {}", dIdTipoCalculo1);
				logger.info("dIdMedida2: {}", dIdMedida2);
				logger.info("dEnteros2: {}", dEnteros2);
				logger.info("dDecimales2: {}", dDecimales2);
				logger.info("dFactor2: {}", dFactor2);
				logger.info("dIdTipoCalculo2: {}", dIdTipoCalculo2);
				logger.info("dIdMedida3: {}", dIdMedida3);
				logger.info("dEnteros3: {}", dEnteros3);
				logger.info("dDecimales3: {}", dDecimales3);
				logger.info("dFactor3: {}", dFactor3);
				logger.info("dIdTipoCalculo3: {}", dIdTipoCalculo3);
				logger.info("dIdMedida4: {}", dIdMedida4);
				logger.info("dEnteros4: {}", dEnteros4);
				logger.info("dDecimales4: {}", dDecimales4);
				logger.info("dFactor4: {}", dFactor4);
				logger.info("dIdTipoCalculo4: {}", dIdTipoCalculo4);
				logger.info("dIdMedida5: {}", dIdMedida5);
				logger.info("dEnteros5: {}", dEnteros5);
				logger.info("dDecimales5: {}", dDecimales5);
				logger.info("dFactor5: {}", dFactor5);
				logger.info("dIdTipoCalculo5: {}", dIdTipoCalculo5);
			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 8;
				// sprintf(vObservacion,"%s Error: Al leer los datos del medidor instalar, para
				// el Nro. Orden: %d .",vObservacion,NumeroOrden);
				vObservacion = "Error: Al leer los datos del medidor instalar";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;// agregado por AVC 20150701
			}

			tipo_magnitud = 1; // de instalacion
			i_tipo_magnitud = tipo_magnitud;
			/************************************************************************************************************************/
			// Energia Fuera de Punta
			/************************************************************************************************************************/
			logger.info("medidorInst.getCLectENFP(): {}", medidorInst.getCLectENFP());
			if (strcmp(medidorInst.getCLectENFP(), "") == 0) {
				error = 0;
				logger.info("tiene_enfp: {}.", tiene_enfp);
				if ((strcmp(tiene_enfp, "S") == 0)) {
					error = 1;
					paso_actualizacion = 109;
					vObservacion = "Medidor a instalar tiene medida ENFP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.error("Validando : {} .\n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_enfp: {}", tiene_enfp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_enfp, "S") == 0) && (iSeRegistroLectura == 0)) {
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;

					if (!InsertaLectura(medidorInst.getCLectENFP(), i_tipo_magnitud, vObs, dIdComponente_i, dIdMedida1,
							dEnteros1, dDecimales1, dFactor1, dIdTipoCalculo1)) {
						return false;
					}
					iSeRegistroLectura++;
				}
			}
			/************************************************************************************************************************/
			// Energia Hora de Punta
			/************************************************************************************************************************/
			logger.info("medidorInst.getCLectENHP(): {}", medidorInst.getCLectENHP());
			if (strcmp(medidorInst.getCLectENHP(), "") == 0) {
				error = 0;
				logger.info("tiene_enhp: {}.", tiene_enhp);
				if ((strcmp(tiene_enhp, "S") == 0)) {
					error = 1;
					paso_actualizacion = 209;

					vObservacion = "Medidor a instalar tiene medida ENHP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.error("Validando : {} . Error \n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_enhp: {}", tiene_enhp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_enhp, "S") == 0) && (iSeRegistroLectura == 0)) {
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					if (!InsertaLectura(medidorInst.getCLectENHP(), i_tipo_magnitud, vObs, dIdComponente_i, dIdMedida2,
							dEnteros2, dDecimales2, dFactor2, dIdTipoCalculo2)) {
						return false;
					}
					iSeRegistroLectura++;
				}
			}
			/************************************************************************************************************************/
			// Demanda Fuera de Punta
			/************************************************************************************************************************/
			logger.info("medidorInst.getCLectDMFP(): {}", medidorInst.getCLectDMFP());
			if (strcmp(medidorInst.getCLectDMFP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_dmfp, "S") == 0)) {
					error = 1;
					paso_actualizacion = 309;
					vObservacion = "Medidor a instalar tiene medida DMFP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.error("Validando : {} . \n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_dmfp: {}", tiene_dmfp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_dmfp, "S") == 0) && (iSeRegistroLectura == 0)) {
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;

					if (!InsertaLectura(medidorInst.getCLectDMFP(), i_tipo_magnitud, vObs, dIdComponente_i, dIdMedida3,
							dEnteros3, dDecimales3, dFactor3, dIdTipoCalculo3)) {
						return false;
					}
					iSeRegistroLectura++;
				}
			}
			/************************************************************************************************************************/
			// Demanda Hora de Punta
			/************************************************************************************************************************/
			logger.info("medidorInst.getCLectDMHP(): {}", medidorInst.getCLectDMHP());
			if (strcmp(medidorInst.getCLectDMHP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_dmhp, "S") == 0)) {
					error = 1;
					paso_actualizacion = 409;

					vObservacion = "Medidor a instalar tiene medida DMHP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.error("Validando : {} . \n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_dmhp: {}", tiene_dmhp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_dmhp, "S") == 0) && (iSeRegistroLectura == 0)) {
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;

					if (!InsertaLectura(medidorInst.getCLectDMHP(), i_tipo_magnitud, vObs, dIdComponente_i, dIdMedida4,
							dEnteros4, dDecimales4, dFactor4, dIdTipoCalculo4)) {
						return false;
					}
					iSeRegistroLectura++;
				}
			}
			/************************************************************************************************************************/
			// Reactiva
			/************************************************************************************************************************/
			logger.info("medidorInst.getCLectEnRe(): {}", medidorInst.getCLectEnRe());
			if (strcmp(medidorInst.getCLectEnRe(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_reac, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 509;

					vObservacion = String.format(
							"Medidor a instalar tiene medida En.Reactiva y NO se ingreso lectura de retiro para la misma,para el Nro. Orden: %s .",
							NumeroOrden);
					vCodOperacion = "ASY030";
					logger.error("Validando : {} . \n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_reac: {}", tiene_reac);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_reac, "S") == 0) && (iSeRegistroLectura == 0)) {
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;

					if (!InsertaLectura(medidorInst.getCLectEnRe(), i_tipo_magnitud, vObs, dIdComponente_i, dIdMedida5,
							dEnteros5, dDecimales5, dFactor5, dIdTipoCalculo5)) {
						return false;
					}
					iSeRegistroLectura++;
				}
			}

			if (dIdComponente_i > 0) {

				error = 0;
				try {
					medComponenteDao.updateComponenteInstalar(dIdComponente_i, fechaInstalacion, dIdPropiedad,
							dIdServicio);

				} catch (Exception e) {
					vObservacion = "Error al Update la tabla med_componente";
					vCodOperacion = "ASY030";
					logger.error("Validando : {} . Error : {}", vObservacion, e.getMessage());
					logger.error("Fecha Instalacion:{}", fechaInstalacion);
					return false;
				}

			}

			try {
				ordMediCambioDao.insert(OrdMediCambioBean.builder().idOrden(dIdOrden).idComponente(dIdComponente_i)
						.idEmpresa(3L).accion("Instalacion").idMediCambio(null).build());
			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 601;
				// sprintf(vObservacion,"%s Error al Insertar la tabla ord_medi_cambio,para el
				// Nro. Orden: %d .",vObservacion,NumeroOrden);
				vObservacion = "Error al Insertar la tabla ord_medi_cambio";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;// agregado por AVC 20150701
			}

			error = 0;

			try {
				medHisComponenteDao.updateFecHastaByIdComponete(dIdComponente_i);

			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 602;
				// sprintf(vObservacion,"%s Error al Update la tabla med_his_componente,para el
				// Nro. Orden: %d .",vObservacion,NumeroOrden);
				vObservacion = "Error al Update la tabla med_his_componente";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;// agregado por AVC 20150701
			}

			try {
				Long idHisComponente = medHisComponenteDao.nextVal();
				medHisComponenteDao.insertMedidorInstalacion(idHisComponente, dIdOrden, dIdComponente_i, dIdServicio);

			} catch (Exception e) {
				paso_actualizacion = 99;
				cmensaje_out = String.format("Error: Actualizando med_his_componente (i). - Paso: %d .",
						paso_actualizacion);
				logger.error("%s", cmensaje_out);
				logger.error("Error exception: {}", e);
				vObservacion = cmensaje_out;
				vCodOperacion = "ASY002";

				// return paso_actualizacion;//comenatdo por AVC 20150701
				return false;// agregado por AVC 20150701
			}

			// R.I. Agregado 03/11/2023 INICIO
			List<Integer> idMedidas = new ArrayList<>();
			if ("S".equals(tiene_enfp)) {
				idMedidas.add(1);
			}
			if ("S".equals(tiene_enhp)) {
				idMedidas.add(2);
			}
			if ("S".equals(tiene_dmfp)) {
				idMedidas.add(4);
			}
			if ("S".equals(tiene_dmhp)) {
				idMedidas.add(5);
			}
			if ("S".equals(tiene_reac)) {
				idMedidas.add(6);
			}
			if (!idMedidas.isEmpty()) {
				medComponenteMedidaMedidorDao.deleteFromMedidaMedidorByIdComponente(dIdComponente_i, idMedidas);
			}
			// R.I. Agregado 03/11/2023 FIN
		} // end for
		return true;
	}

	private boolean RetirarMedidor(int tipoMagnitud) {
		logger.info("RetirarMedidor({})", tipoMagnitud);
		int i_tipo_magnitud;
		String vObs;
		String vTipLec;
		int paso_actualizacion = 0;
		/*
		 * R.I. Comentado 27/10/2023 String tiene_enfp; String tiene_enhp; String
		 * tiene_dmfp; String tiene_dmhp; String tiene_reac;
		 */
		int tipo_magnitud = 4;
		String spropiedad_ret;
		Long dIdTipoCalculo1 = 0L;
		Long dIdTipoCalculo2 = 0L;
		Long dIdTipoCalculo3 = 0L;
		Long dIdTipoCalculo4 = 0L;
		Long dIdTipoCalculo5 = 0L;
		Long dIdMedida1 = 0L;
		Long dIdMedida2 = 0L;
		Long dIdMedida3 = 0L;
		Long dIdMedida4 = 0L;
		Long dIdMedida5 = 0L;
		Long dDecimales1 = 0L;
		Long dDecimales2 = 0L;
		Long dDecimales3 = 0L;
		Long dDecimales4 = 0L;
		Long dDecimales5 = 0L;
		Long dEnteros1 = 0L;
		Long dEnteros2 = 0L;
		Long dEnteros3 = 0L;
		Long dEnteros4 = 0L;
		Long dEnteros5 = 0L;
		BigDecimal dFactor1 = new BigDecimal(0);
		BigDecimal dFactor2 = new BigDecimal(0);
		BigDecimal dFactor3 = new BigDecimal(0);
		BigDecimal dFactor4 = new BigDecimal(0);
		BigDecimal dFactor5 = new BigDecimal(0);
		Long aux_id;
		Long aux_id_magnitud;
		Long aux_id_medi_insp;
		int error;
		int xx;
		String NroComponente;
		Long dIdComponente;
		Long dsecMagnitud = 0L;
		String cMarcamedidor = "";
		String cModelomedidor = "";
		int iSeRegistroLectura = 0;
		i_tipo_magnitud = tipoMagnitud;

		logger.info("medidoresRet: {}", medidoresRet);
		for (SMedidorXmlBean medidorRet : medidoresRet) {
			logger.info("medidorRet: {}", medidorRet);
			NroComponente = "";
			cMarcamedidor = "";
			cModelomedidor = "";

			NroComponente = medidorRet.getCNUMEROMEDIDOR();
			cMarcamedidor = medidorRet.getCMARCAMEDIDOR();
			cModelomedidor = medidorRet.getCMODELOMEDIDOR();
			iSeRegistroLectura = 0;
			dIdComponente = 0L;

			if (strcmp(NroComponente, "") != 0) {
				try {
					dIdComponente = medComponenteDao
							.getIdComponenteElectricoByIdServicioAndNroComponenteAndMarcaMedidorAndModeloMedidor(
									Double.valueOf(dIdServicio).longValue(), NroComponente, cMarcamedidor,
									cModelomedidor);
				} catch (Exception e) {
					error = 1;
					vObservacion = String.format("Componente no existe o no está instalado (%s)", NroComponente);
					vCodOperacion = "ASY045";
					return false;
				}
			} else {
				dIdComponente = 0L;
			}
			if (dIdComponente <= 0) {
				error = 1;
				vObservacion = String.format("El cambio de medidor requiere el medidor a Retirar", NroComponente);
				vCodOperacion = "ASY045";
				return false;
			}

			error = 0;

			try {
				MedidorMedidaDatoBean medidorMedidaDatoBean = medidorMedidaDatoDao
						.findByIdComponenteRetirar(dIdComponente);
				tiene_enfp = medidorMedidaDatoBean.getTieneMedidaEnfp();
				tiene_enhp = medidorMedidaDatoBean.getTieneMedidaEnhp();
				tiene_dmfp = medidorMedidaDatoBean.getTieneMedidaDmfp();
				tiene_dmhp = medidorMedidaDatoBean.getTieneMedidaDmhp();
				tiene_reac = medidorMedidaDatoBean.getTieneMedidaReac();
				dIdMedida1 = medidorMedidaDatoBean.getIdMedida1();
				dEnteros1 = medidorMedidaDatoBean.getCantEnteros1();
				dDecimales1 = medidorMedidaDatoBean.getCantDecimales1();
				dFactor1 = medidorMedidaDatoBean.getValFactor1();
				dIdTipoCalculo1 = medidorMedidaDatoBean.getIdTipCalculo1();
				dIdMedida2 = medidorMedidaDatoBean.getIdMedida2();
				dEnteros2 = medidorMedidaDatoBean.getCantEnteros2();
				dDecimales2 = medidorMedidaDatoBean.getCantDecimales2();
				dFactor2 = medidorMedidaDatoBean.getValFactor2();
				dIdTipoCalculo2 = medidorMedidaDatoBean.getIdTipCalculo2();
				dIdMedida3 = medidorMedidaDatoBean.getIdMedida3();
				dEnteros3 = medidorMedidaDatoBean.getCantEnteros3();
				dDecimales3 = medidorMedidaDatoBean.getCantDecimales3();
				dFactor3 = medidorMedidaDatoBean.getValFactor3();
				dIdTipoCalculo3 = medidorMedidaDatoBean.getIdTipCalculo3();
				dIdMedida4 = medidorMedidaDatoBean.getIdMedida4();
				dEnteros4 = medidorMedidaDatoBean.getCantEnteros4();
				dDecimales4 = medidorMedidaDatoBean.getCantDecimales4();
				dFactor4 = medidorMedidaDatoBean.getValFactor4();
				dIdTipoCalculo4 = medidorMedidaDatoBean.getIdTipCalculo4();
				dIdMedida5 = medidorMedidaDatoBean.getIdMedida5();
				dEnteros5 = medidorMedidaDatoBean.getCantEnteros5();
				dDecimales5 = medidorMedidaDatoBean.getCantDecimales5();
				dFactor5 = medidorMedidaDatoBean.getValFactor5();
				dIdTipoCalculo5 = medidorMedidaDatoBean.getIdTipCalculo5();
				spropiedad_ret = medidorMedidaDatoBean.getPropiedad();

				logger.info("tiene_enfp: {}", tiene_enfp);
				logger.info("tiene_enhp: {}", tiene_enhp);
				logger.info("tiene_dmfp: {}", tiene_dmfp);
				logger.info("tiene_dmhp: {}", tiene_dmhp);
				logger.info("tiene_reac: {}", tiene_reac);
				logger.info("dIdMedida1: {}", dIdMedida1);
				logger.info("dEnteros1: {}", dEnteros1);
				logger.info("dDecimales1: {}", dDecimales1);
				logger.info("dFactor1: {}", dFactor1);
				logger.info("dIdTipoCalculo1: {}", dIdTipoCalculo1);
				logger.info("dIdMedida2: {}", dIdMedida2);
				logger.info("dEnteros2: {}", dEnteros2);
				logger.info("dDecimales2: {}", dDecimales2);
				logger.info("dFactor2: {}", dFactor2);
				logger.info("dIdTipoCalculo2: {}", dIdTipoCalculo2);
				logger.info("dIdMedida3: {}", dIdMedida3);
				logger.info("dEnteros3: {}", dEnteros3);
				logger.info("dDecimales3: {}", dDecimales3);
				logger.info("dFactor3: {}", dFactor3);
				logger.info("dIdTipoCalculo3: {}", dIdTipoCalculo3);
				logger.info("dIdMedida4: {}", dIdMedida4);
				logger.info("dEnteros4: {}", dEnteros4);
				logger.info("dDecimales4: {}", dDecimales4);
				logger.info("dFactor4: {}", dFactor4);
				logger.info("dIdTipoCalculo4: {}", dIdTipoCalculo4);
				logger.info("dIdMedida5: {}", dIdMedida5);
				logger.info("dEnteros5: {}", dEnteros5);
				logger.info("dDecimales5: {}", dDecimales5);
				logger.info("dFactor5: {}", dFactor5);
				logger.info("dIdTipoCalculo5: {}", dIdTipoCalculo5);
				logger.info("spropiedad_ret: {}", spropiedad_ret);
			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 8;
				vObservacion = String.format(
						"Error: Al leer los datos del medidor retirar/normalizar para el Nro. Orden: %s .",
						NumeroOrden);
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;
			}

			/************************************************************************************************************************/
			// Energia Fuera de Punta
			/************************************************************************************************************************/
			logger.info("medidorRet.getCLectENFP(): {}", medidorRet.getCLectENFP());
			if (strcmp(medidorRet.getCLectENFP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_enfp, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 9;
					vObservacion = "Medidor a retirar tiene medida ENFP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.info("Validando : {} .\n", vObservacion);
					return false;
				}
			} else {
				logger.info("tiene_enfp: {}", tiene_enfp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_enfp, "S") == 0) && (iSeRegistroLectura == 0)) /* ANL_20200909 */
				{
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					if (!InsertaLectura(medidorRet.getCLectENFP(), i_tipo_magnitud, vObs, dIdComponente, dIdMedida1,
							dEnteros1, dDecimales1, dFactor1, dIdTipoCalculo1)) {
						return false;
					}
					iSeRegistroLectura++;

				}
			}

			/************************************************************************************************************************/
			// Energia Hora de Punta
			/************************************************************************************************************************/
			logger.info("medidorRet.getCLectENHP(): {}", medidorRet.getCLectENHP());
			if (strcmp(medidorRet.getCLectENHP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_enhp, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 19;
					// sprintf(vObservacion,"%s Medidor a retirar tiene medida ENHP y NO se ingreso
					// lectura de retiro para la misma,para el Nro. Orden: %d
					// .",vObservacion,NumeroOrden);
					vObservacion = "Medidor a retirar tiene medida ENHP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.info("Validando : {} . \n", vObservacion);
					return false;// agregado por AVC 20150701
				}
			} else// if(strcmp(LectENHP, "") == 0)
			{
				// if(strcmp(tiene_enhp, "S") == 0)
				logger.info("tiene_enhp: {}", tiene_enhp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_enhp, "S") == 0) && (iSeRegistroLectura == 0)) /* ANL_20200909 */
				{
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					// printf("\n***InsertaLectura medidoresRet cLectENHP %s",NroComponente);
					if (!InsertaLectura(medidorRet.getCLectENHP(), i_tipo_magnitud, vObs, dIdComponente, dIdMedida2,
							dEnteros2, dDecimales2, dFactor2, dIdTipoCalculo2)) {
						return false;
					}
					iSeRegistroLectura++; /* ANL_20200909 */
				} // if(strcmp(tiene_enhp, "S") == 0)
			} // else//if(strcmp(LectENHP, "") == 0)

			// lo que faltaba
			/************************************************************************************************************************/
			// Demanda Fuera de Punta
			/************************************************************************************************************************/
			logger.info("medidorRet.getCLectDMFP(): {}", medidorRet.getCLectDMFP());
			if (strcmp(medidorRet.getCLectDMFP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_dmfp, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 29;
					// sprintf(vObservacion,"%s Medidor a retirar tiene medida DMFP y NO se ingreso
					// lectura de retiro para la misma,para el Nro. Orden: %d
					// .",vObservacion,NumeroOrden);
					vObservacion = "Medidor a retirar tiene medida DMFP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.info("Validando : {} . \n", vObservacion);
					return false;// agregado por AVC 20150701
				}
			} else// if(strcmp(LectDMFP, "") == 0)
			{
				// if(strcmp(tiene_dmfp, "S") == 0)
				logger.info("tiene_dmfp: {}", tiene_dmfp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_dmfp, "S") == 0) && (iSeRegistroLectura == 0)) /* ANL_20200909 */
				{
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					// printf("\n***InsertaLectura medidoresRet cLectDMFP %s",NroComponente);
					if (!InsertaLectura(medidorRet.getCLectDMFP(), i_tipo_magnitud, vObs, dIdComponente, dIdMedida3,
							dEnteros3, dDecimales3, dFactor3, dIdTipoCalculo3)) {
						return false;
					}
					iSeRegistroLectura++; /* ANL_20200909 */
				} // if(strcmp(tiene_dmfp, "S") == 0)
			} // else//if(strcmp(LectDMFP, "") == 0)

			/************************************************************************************************************************/
			// Demanda Hora de Punta
			/************************************************************************************************************************/
			logger.info("medidorRet.getCLectDMHP(): {}", medidorRet.getCLectDMHP());
			if (strcmp(medidorRet.getCLectDMHP(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_dmhp, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 39;
					// sprintf(vObservacion,"%s Medidor a retirar tiene medida DMHP y NO se ingreso
					// lectura de retiro para la misma,para el Nro. Orden: %d
					// .",vObservacion,NumeroOrden);
					vObservacion = "Medidor a retirar tiene medida DMHP y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.info("Validando : {} .\n", vObservacion);
					return false;// agregado por AVC 20150701
				}
			} else// if(strcmp(LectDMHP, "") == 0)
			{
				// if(strcmp(tiene_dmhp, "S") == 0)
				logger.info("tiene_dmhp: {}", tiene_dmhp);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_dmhp, "S") == 0) && (iSeRegistroLectura == 0)) /* ANL_20200909 */
				{
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					// printf("\n***InsertaLectura medidoresRet cLectDMHP %s",NroComponente);
					if (!InsertaLectura(medidorRet.getCLectDMHP(), i_tipo_magnitud, vObs, dIdComponente, dIdMedida4,
							dEnteros4, dDecimales4, dFactor4, dIdTipoCalculo4)) {
						return false;
					}
					iSeRegistroLectura++; /* ANL_20200909 */
				} // if(strcmp(tiene_dmhp, "S") == 0)
			} // else//if(strcmp(LectDMHP, "") == 0)

			/************************************************************************************************************************/
			// Reactiva
			/************************************************************************************************************************/
			logger.info("medidorRet.getCLectEnRe(): {}", medidorRet.getCLectEnRe());
			if (strcmp(medidorRet.getCLectEnRe(), "") == 0) {
				error = 0;
				if ((strcmp(tiene_reac, "S") == 0) && (strcmp(TieneCambioMed, "S") == 0)) {
					error = 1;
					paso_actualizacion = 49;
					// sprintf(vObservacion,"%s Medidor a retirar tiene medida En.Reactiva y NO se
					// ingreso lectura de retiro para la misma,para el Nro. Orden: %d
					// .",vObservacion,NumeroOrden);
					vObservacion = "Medidor a retirar tiene medida En.Reactiva y NO se ingreso lectura de retiro para la misma";
					vCodOperacion = "ASY030";
					logger.info("Validando : {} . \n", vObservacion);
					return false;// agregado por AVC 20150701
				}
			} else// if(strcmp(LectEnRe, "") == 0)
			{
				// if(strcmp(tiene_reac, "S") == 0)
				logger.info("tiene_reac: {}", tiene_reac);
				logger.info("iSeRegistroLectura: {}", iSeRegistroLectura);
				if ((strcmp(tiene_reac, "S") == 0) && (iSeRegistroLectura == 0)) /* ANL_20200909 */
				{
					vObs = "Lectura Modulo Hurtos (Norm)";
					dsec_magnitud = dsec_magnitud + 1;
					// printf("\n***InsertaLectura medidoresRet cLectEnRe %s",NroComponente);
					if (!InsertaLectura(medidorRet.getCLectEnRe(), i_tipo_magnitud, vObs, dIdComponente, dIdMedida5,
							dEnteros5, dDecimales5, dFactor5, dIdTipoCalculo5)) {
						return false;
					}
					iSeRegistroLectura++; /* ANL_20200909 */
				} // if(strcmp(tiene_reac, "S") == 0)
			} // else//if(strcmp(LectEnRe, "") == 0)

			if (dIdComponente > 0) {
				if (strcmp(spropiedad_ret, "C") == 0) {
					error = 0;
					try {
						Long idCliente = nucClienteDao.getIdClienteByIdServicio(dIdServicio);
						medComponenteDao.updateComponenteFuenteCliente(dIdComponente, idCliente);
					} catch (Exception e) {
						error = 1;
						paso_actualizacion = 56;
						// sprintf(vObservacion,"%s Error al Update la tabla med_componente,para el Nro.
						// Orden: %d .",vObservacion,NumeroOrden);
						vObservacion = "Error al Update la tabla med_componente";
						vCodOperacion = "ASY030";
						logger.error("Validando : {} . Error : {}", vObservacion, e.getMessage());
						return false;// agregado por AVC 20150701
					}
				} else {
					try {
						medComponenteDao.updateComponenteFuenteEjecutor(dIdComponente, dIdEjecutor);
					} catch (Exception e) {
						error = 1;
						paso_actualizacion = 57;
						// sprintf(vObservacion,"%s Error al Update la tabla med_componente,para el Nro.
						// Orden: %d .",vObservacion,NumeroOrden);
						vObservacion = "Error al Update la tabla med_componente";
						vCodOperacion = "ASY030";
						logger.error("Validando : {} . Error : {}", vObservacion, e.getMessage());
						return false;// agregado por AVC 20150701
					}
				}
			}

			error = 0;
			try {
				ordMediCambioDao.insert(OrdMediCambioBean.builder().idOrden(dIdOrden).idComponente(dIdComponente)
						.idEmpresa(3L).accion("Retiro").idMediCambio(null).build());
			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 57;
				// sprintf(vObservacion,"%s Error al Insertar la tabla ord_medi_cambio,para el
				// Nro. Orden: %d .",vObservacion,NumeroOrden);
				vObservacion = "Error al Insertar la tabla ord_medi_cambio";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;// agregado por AVC 20150701
			}

			error = 0;

			try {
				medHisComponenteDao.updateFecHastaByIdComponete(dIdComponente);

			} catch (Exception e) {
				error = 1;
				paso_actualizacion = 57;
				// sprintf(vObservacion,"%s Error al Update la tabla med_his_componente,para el
				// Nro. Orden: %d .",vObservacion,NumeroOrden);
				vObservacion = "Error al Update la tabla med_his_componente";
				vCodOperacion = "ASY030";
				logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
				return false;// agregado por AVC 20150701
			}

			try {
				Long idHisComponente = medHisComponenteDao.nextVal();
				medHisComponenteDao.insertMedidorRetiro(idHisComponente, dIdOrden, dIdComponente, spropiedad_ret,
						dIdServicio, dIdEjecutor);

			} catch (Exception e) {
				// bzero(&cmensaje_out,sizeof(cmensaje_out));
				cmensaje_out = String.format("Error: Actualizando med_his_componente. - Paso: %d .",
						paso_actualizacion);
				logger.error("%s", cmensaje_out);
				logger.error("Error exception: {}", e);
				vObservacion = cmensaje_out;
				vCodOperacion = "ASY002";
				return false;
			}

		} // end for

		return true;
	}

	private boolean InsertaLectura(String cLecturaIn, int iTipoMagnitud, String cObsIn, Long dIdComponenteIn,
			Long dIdMedida, Long dEnteros, Long dDecimales, BigDecimal dFactor, Long dIdTipoCalculo) {
		logger.info("InsertaLectura.");
		Long aux_id;
		Long aux_id_magnitud;
		Long aux_id_medi_insp;
		int error;
		int paso_actualizacion = 0;

		Long dIdComp;

		String cLectura;
		String cObs;
		Long dsecMagnitud = 0L;
		cLectura = cLecturaIn;
		cObs = cObsIn;
		dIdComp = dIdComponenteIn;

		try {
			aux_id_magnitud = medMagnitudDao.nextVal();

		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 10;
			// sprintf(vObservacion,"%s No pudo obtenerse Secuencia de la Magnitud,para el
			// Nro. Orden: %d .",vObservacion,NumeroOrden);
			vObservacion = "No pudo obtenerse Secuencia de la Magnitud";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		// Long l= new Long(i);//first way
		// Long l2=Long.valueOf(i);//second way

		try {
			DateFormat fomatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			BigDecimal valorLectura = new BigDecimal(cLectura);

			medMagnitudDao.insertMagnitudLectura(MedMagnitudBean.builder().idMagnitud(aux_id_magnitud)
					.idEmpresa(db_id_empresa).idComponente(dIdComp).idServicio(dIdServicio).idMedida(dIdMedida)
					.enteros(dEnteros).decimales(dDecimales).factor(dFactor).valor(valorLectura).tipo("LECTURA")
					.idTipMagnitud(new Long(iTipoMagnitud)).observaciones(cObs)
					.fecLecTerreno(fomatoFecha.parse(FechaFinEjec)).secMagnitud(dsec_magnitud).idEstMagnitud(1L)
					.idTipCalculo(dIdTipoCalculo).idTipoCons(1L).build());
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 11;
			// sprintf(vObservacion,"%s Error al insertar la medida,para el Nro. Orden: %d
			// .",vObservacion,NumeroOrden);
			vObservacion = "Error al insertar la medida";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		try {
			aux_id_medi_insp = ordMediInspDao.nextVal();
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 12;
			// sprintf(vObservacion,"%s No pudo obtenerse Secuencia de ord_medi_insp,para el
			// Nro. Orden: %d .",vObservacion,NumeroOrden);
			vObservacion = "No pudo obtenerse Secuencia de ord_medi_insp";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		try {
			ordMediInspDao.insert(OrdMediInspBean.builder().idOrden(dIdOrden).idComponente(dIdComp)
					.idEmpresa(db_id_empresa).idMediInsp(aux_id_medi_insp).build());
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 13;
			// sprintf(vObservacion,"%s Error al insertar en ord_medi_insp,para el Nro.
			// Orden: %d .",vObservacion,NumeroOrden);
			vObservacion = String.format("Error al insertar en ord_medi_insp - %s", cObs);
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		try {
			aux_id = ordInspLecturaDao.nextVal();
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 14;
			// sprintf(vObservacion,"%s No pudo obtenerse Secuencia de ord_insp_lectura,para
			// el Nro. Orden: %d .",vObservacion,NumeroOrden);
			vObservacion = "No pudo obtenerse Secuencia de ord_insp_lectura";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;
		}

		try {
			ordInspLecturaDao.insert(OrdInspLecturaBean.builder().idInspLectura(aux_id).idEmpresa(db_id_empresa)
					.idMediInsp(aux_id_medi_insp).idLectura(aux_id_magnitud).build());
		} catch (Exception e) {
			error = 1;
			paso_actualizacion = 15;
			// sprintf(vObservacion,"%s Error al insertar en ord_insp_lectura,para el Nro.
			// Orden: %d .",vObservacion,NumeroOrden);
			vObservacion = "Error al insertar en ord_insp_lectura";
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 20150701
		}

		return true;
	}

	private boolean ValidarMedidorRetirar() {
		logger.info("Metodo ValidarMedidorRetirar inicio.");

		String NroComponente;
		Long dIdComponente;
		String cMarcamedidor = "";
		String cModelomedidor = "";

		int iCont = 0;
		int xx = 0;
		int yy = 0;
		int flag = 0;
		int iContMedOk = 0;
		int intResultRetiradoAux;

		if (intResultRetirado <= 0) {
			flag = 1;

		} else {
			for (SMedidorXmlBean medidorRet : medidoresRet) {
				NroComponente = "";
				cMarcamedidor = "";
				cModelomedidor = "";
				NroComponente = medidorRet.getCNUMEROMEDIDOR();
				cMarcamedidor = medidorRet.getCMARCAMEDIDOR();
				cModelomedidor = medidorRet.getCMODELOMEDIDOR();
				dIdComponente = 0L;

				try {
					dIdComponente = medComponenteDao
							.getIdComponenteElectricoByIdServicioAndNroComponenteAndMarcaMedidorAndModeloMedidor(
									Double.valueOf(dIdServicio).longValue(), NroComponente, cMarcamedidor,
									cModelomedidor);
				} catch (Exception e) {
					vObservacion = String.format("Error al obtener Medidor a Retirar (%s)", NroComponente);
					vCodOperacion = "ASY045";
					logger.info("\n%s", vObservacion);
					return false;
				}
				iContMedOk++;

			}
			if (iContMedOk == 0) {
				flag = 1;
			}
		}

		if (flag == 1) {
			medidores4J = medidor4jDao.findByIdUbicacion(dIdServicio);
			iCont = medidores4J.size();
			if (iCont > 0) {
				intResultRetirado = 0;
				medidoresRet = new ArrayList<>();
				for (Medidor4jBean medidor4jBean : medidores4J) {
					medidoresRet.add(SMedidorXmlBean.builder().cNUMEROMEDIDOR(medidor4jBean.getNroComponente())
							.cMARCAMEDIDOR(medidor4jBean.getCodMarca()).cMODELOMEDIDOR(medidor4jBean.getCodModelo())
							.cACCIONMEDIDOR("Encontrado").cLectENFP("0").cLectENHP("0").cLectDMFP("0").cLectDMHP("0")
							.cLectEnRe("0").build());

					intResultRetirado = intResultRetirado + 1;

				}
			}
		} else {
			medidores4J = medidor4jDao.findByIdUbicacion(Double.valueOf(dIdServicio).longValue());
			iCont = medidores4J.size();
			intResultRetiradoAux = 0;
			medidoresAux = new ArrayList<>();
			if (iCont > 0) {
				for (Medidor4jBean medidor4jBean : medidores4J) {
					flag = 0;// no existe
					for (SMedidorXmlBean medidorRet : medidoresRet) {

						if ((strcmp(medidorRet.getCNUMEROMEDIDOR(), medidor4jBean.getNroComponente()) == 0)
								&& (strcmp(medidorRet.getCMARCAMEDIDOR(), medidor4jBean.getCodMarca()) == 0)
								&& (strcmp(medidorRet.getCMODELOMEDIDOR(), medidor4jBean.getCodModelo()) == 0)) {
							flag = 1;

							medidoresAux.add(SMedidorXmlBean.builder().cNUMEROMEDIDOR(medidorRet.getCNUMEROMEDIDOR())
									.cMARCAMEDIDOR(medidorRet.getCMARCAMEDIDOR())
									.cMODELOMEDIDOR(medidorRet.getCMODELOMEDIDOR()).cACCIONMEDIDOR("Encontrado")
									.cLectENFP(medidorRet.getCLectENFP()).cLectENHP(medidorRet.getCLectENHP())
									.cLectDMFP(medidorRet.getCLectDMFP()).cLectDMHP(medidorRet.getCLectDMHP())
									.cLectEnRe(medidorRet.getCLectEnRe()).build());

							intResultRetiradoAux = intResultRetiradoAux + 1;

						}
					}

					if (flag == 0) {
						medidoresAux.add(SMedidorXmlBean.builder().cNUMEROMEDIDOR(medidor4jBean.getNroComponente())
								.cMARCAMEDIDOR(medidor4jBean.getCodMarca()).cMODELOMEDIDOR(medidor4jBean.getCodModelo())
								.cACCIONMEDIDOR("Encontrado").cLectENFP("0").cLectENHP("0").cLectDMFP("0")
								.cLectDMHP("0").cLectEnRe("0").build());
						intResultRetiradoAux = intResultRetiradoAux + 1;

					}
				}
			}

			medidoresRet = medidoresAux;
		}
		logger.info("Metodo ValidarMedidorRetirar fin.");

		return true;
	}

	private boolean ValidarRecepciones() {
		logger.info("NumeroOrden: {}", NumeroOrden);

		String numOrdenExist = "0";
		Long aux_cont = 0L;
		int error = 0;

		int xx;

		error = 0;
		aux_cont = 0L;

		aux_cont = comParametroDao.getCountBySistemaAndEntidadAndCodigoAndValorAlf("EORDER", "DOMINIOS", "RESULTADO",
				CodResultado);

		if (aux_cont == 0) {
			error = 1;
			vObservacion = String.format("Codigo de Resultado no es valor VALIDO del Dominio.Nro Orden:(%s)",
					NumeroOrden);
			vCodOperacion = "ASY045";
			return false;
		}

		error = 0;
		aux_cont = 0L;

		aux_cont = comParametroDao.getCountBySistemaAndEntidadAndCodigoAndValorAlf("EORDER", "DOMINIOS", "TDC_CREADO",
				Tdc_Creada_Campo);

		if (aux_cont == 0) {
			error = 1;
			vObservacion = String.format(
					"FLAG de Creacion de TDC en campo no es valor VALIDO del Dominio.Nro Orden:(%s)", NumeroOrden);
			vCodOperacion = "ASY045";
			return false;
		}
		logger.info("Tdc_Creada_Campo: {}.", Tdc_Creada_Campo);
		if (strcmp(Tdc_Creada_Campo, "1") == 0) {
			error = 0;
			aux_cont = 0L;

			// aux_cont =
			// eorOrdTransferDao.getCodEstadoOrdenByNroOrdenLegacy(NumeroOrdenInsp);
			logger.info("NumeroOrdenInsp: {}", NumeroOrdenInsp);
			EorOrdTransferBean eorOrdTransferBean = eorOrdTransferDao.findByNroOrdenLegacy(NumeroOrdenInsp);
			logger.info("Resultado de la condicion: {}",
					eorOrdTransferBean == null || eorOrdTransferBean.getCodEstadoOrden() == 0);
			if (eorOrdTransferBean == null || eorOrdTransferBean.getCodEstadoOrden() == 0) {
				// if (true) {
				error = 1;
				vObservacion = "Nro de Orden de Inspeccion asociada no existe en la tabla de Transferencia";
				vCodOperacion = "ASY009";
				return false;
			}
			aux_cont = eorOrdTransferBean.getCodEstadoOrden();
			logger.info("Codigo de estado de la orden de inspeccion con numero {}: {}", NumeroOrdenInsp, aux_cont);
			if (aux_cont != 6 && aux_cont != 8)// si NO esta recepcionada o Confirmada
			{
				error = 1;
				vObservacion = "Nro de Orden de Inspeccion asociada No esta Recepcionada";
				vCodOperacion = "ASY009";
				return false;
			}

			if (aux_cont == 6 || aux_cont == 8) {

				aux_cont = 0L;
				error = 0;

				OrdOrdenAaBean ordOrdenAaBean = ordOrdenDao.findAaByNroOrden(NumeroOrdenInsp);
				// numOrdenExist = Double.parseDouble(ordOrdenAaBean.getNroOrden());
				numOrdenExist = ordOrdenAaBean.getNroOrden();
				cNroCuenta = ordOrdenAaBean.getNroCuenta();
			}

			NumeroOrden = numOrdenExist;
			error = 0;

			OrdOrdenAbBean ordOrdenAbBean = ordOrdenDao.findAbByNroOrden(numOrdenExist, db_id_empresa);

			if (ordOrdenAbBean == null) {
				error = 1;
				vObservacion = String.format("Orden no existe. (%s)", NumeroOrden);
				vCodOperacion = "ASY009";
				logger.info(vObservacion);
				return false;
			}

			dIdOrden = ordOrdenAbBean.getIdOrden();
			dIdWorkflowOrd = ordOrdenAbBean.getIdWorkflow();
			aEstadoOrden = ordOrdenAbBean.getIdState().toString();
			dIdWorkflowSeg = ordOrdenAbBean.getIdWorkflowSeg();
			dIdServicio = ordOrdenAbBean.getIdServicio();
			NumeroServicio = ordOrdenAbBean.getNroServicio();
			dIdOrden_insp = ordOrdenAbBean.getIdOrdInsp();

			if (strcmp(aEstadoOrden, "SEmitida") != 0 && strcmp(aEstadoOrden, "SAsignada") != 0
					&& strcmp(aEstadoOrden, "SCreada") != 0) {
				error = 1;
				vObservacion = String.format(
						"Orden no esta en estado permitido (Emitida ,Asignada, o creada). Estado Actual (%s)",
						aEstadoOrden);
				vCodOperacion = "ASY009";
				logger.info(vObservacion);
				return false;
			}

		} else {
			error = 0;
			aux_cont = 0L;

			aux_cont = ordTipoOrdenDao.getCountByCodInternoAndCodTipoOrden("OrdenNormalizacion", vTipoOrdenLegacy);

			if (aux_cont == 0) {
				error = 1;
				vObservacion = String.format("Tipo de TDC Legacy no es valor VALIDO del Dominio.Nro Orden(%s)",
						NumeroOrden);
				vCodOperacion = "ASY045";
				logger.info(vObservacion);
				return false;
			}

			OrdOrdenAbBean ordOrdenAbBean2 = ordOrdenDao.findAbByNroOrden(vNroOrden, db_id_empresa);

			if (ordOrdenAbBean2 == null) {
				error = 1;
				vObservacion = String.format("Orden no existe. (%s)", NumeroOrden);
				vCodOperacion = "ASY009";
				logger.info(vObservacion);
				return false;
			}
			dIdOrden = ordOrdenAbBean2.getIdOrden();
			dIdWorkflowOrd = ordOrdenAbBean2.getIdWorkflow();
			aEstadoOrden = ordOrdenAbBean2.getIdState().toString();
			dIdWorkflowSeg = ordOrdenAbBean2.getIdWorkflowSeg();
			dIdServicio = ordOrdenAbBean2.getIdServicio();
			NumeroServicio = ordOrdenAbBean2.getNroServicio();
			dIdOrden_insp = ordOrdenAbBean2.getIdOrdInsp();

			if (strcmp(aEstadoOrden, "SEmitida") != 0 && strcmp(aEstadoOrden, "SAsignada") != 0
					&& strcmp(aEstadoOrden, "SCreada") != 0) {
				vObservacion = String.format(
						"Orden no esta en estado permitido (Emitida ,Asignada, o creada). Estado Actual (%s)",
						aEstadoOrden);
				vCodOperacion = "ASY001";
				logger.info(vObservacion);
				return false;
			}

		}

		error = 0;
		aux_cont = 0L;

		aux_cont = comCuadrillaDao.getCountByCodCuadrillaAndIdEmpresa(CodCuadrilla);
		if (aux_cont == 0) {
			error = 1;
			vObservacion = String.format("Cuadrilla no existe (%s)", CodCuadrilla);
			vCodOperacion = "ASY025";
			logger.info(vObservacion);
			return false;
		}

		if (aux_cont == 1) {
			ComCuadrillaAaBean comCuadrillaAaBean = comCuadrillaDao.findAaByCodCuadrillaAndIdEmpresa(CodCuadrilla);
			if (comCuadrillaAaBean == null) {
				error = 1;
				vObservacion = String.format("Cuadrilla no encontrada. (%s)", CodCuadrilla);
				vCodOperacion = "ASY025";
				logger.info(vObservacion);
				return false;
			}
			dIdCuadrilla = comCuadrillaAaBean.getIdCuadrilla();
			dIdEjecutor = comCuadrillaAaBean.getIdEjecutor();
		}

		if (aux_cont > 1) {
			error = 1;
			vObservacion = String.format("Existe mas de un técnico para el código de la cuadrilla(%s)", CodCuadrilla);
			vCodOperacion = "ASY026";
			logger.info(vObservacion);
			return false;
		}

		if (strcmp(CodActComercial, "") != 0) {

			try {
				SrvActComercialBean srvActComercialBean = srvActComercialDao
						.findByCodActComercialAndIdEmpresa(CodActComercial, db_id_empresa);
				dIdActComercial = srvActComercialBean.getIdActComercial();
			} catch (Exception e) {
				error = 1;
				vObservacion = String.format("Actividad Comercial no existe. (%s)", CodActComercial);
				vCodOperacion = "ASY027";
				logger.info(vObservacion);
				return false;
			}

		}
		if (strcmp(CodParentesco, "") != 0) {
			dIdParentesco = comParentescoDao.findByCodParentescoAndIdEmpresa(CodParentesco, db_id_empresa);
			if (dIdParentesco == null) {
				error = 1;
				vObservacion = String.format("Parentesco no existe. (%s)", CodParentesco);
				vCodOperacion = "ASY033";
				logger.info(vObservacion);
				return false;
			}
		}

		try {
			DisResultadoInspAaBean disResultadoInspAaBean = disResultadoInspDao
					.findByCodResultadoAndIdEmpresa(CodResultadoCausal);
			dIdResultado = disResultadoInspAaBean.getId();
			CodInternoResultado = disResultadoInspAaBean.getCodInterno();
		} catch (Exception e) {
			error = 1;
			vObservacion = String.format("Resultado no existe-Codigo Causal . (%s)", CodResultadoCausal);
			vCodOperacion = "ASY005";
			logger.info(vObservacion);
			return false;
		}

		TieneCambioMed = TieneCambioMed.toUpperCase().equals("S") ? "S" : "N";

		if (strcmp(TieneCambioMed, "S") == 0) {
			if (intResultInstalado <= 0) {
				error = 1;
				vObservacion = String.format("El cambio de medidor requiere el medidor a instalar");
				vCodOperacion = "ASY045";
				logger.info(vObservacion);
				return false;
			}
		}

		logger.info("Se valido las recepciones");

		return true;
	}

	private boolean GrabarTareas() {
		int x = 0;
		if (int_cnt_tareas != 0) {

			// bzero(&aCodIrregularidad,sizeof(aCodIrregularidad));

			// strcpy(aCodIrregularidad,CodAnormalidad);
			aCodIrregularidad = CodAnormalidad;
			for (x = 0; x < int_cnt_tareas; x++) {
				// bzero(&aCodTarea,sizeof(aCodTarea));
				dIdIrregularidad = 0L;
				dIdTarea = 0L;

				// strcpy(aCodTarea,xmlTareas[x].CODIGOTAREA);
				aCodTarea = xmlTareas.get(x).getCODIGOTAREA();
				if (!vfnValidarTareas2()) {
					return false;
				}
				if (!ActualizarTareas()) {
					return false;
				}

			} // fin for
		}
		return true;
	}

	// private String sprintf(String format, Object... args) {
	// return String.format(format, args);
	// }

	// private void sprintf(String variable, String format, Object... args) {
	// variable = String.format(format, args);
	// }

	// private String vObservacion2 = new String();

	private boolean ActualizarTareas() {
		Long aux_id;
		int error;
		int paso_trabajo;
		Long dId_ord_tarea;

		paso_trabajo = 0;
		dId_ord_tarea = 0L;
		error = 0;

		try {
			dId_ord_tarea = disOrdTareaDao.getIdOrdTareaByIdEmpresaAndIdOrdenAndIdIrregularidadAndIdTarea(dIdOrden,
					dIdIrregularidad, dIdTarea);
		} catch (Exception e) {
			error = 1;
			paso_trabajo = 7;
			// vObservacion = String.format("%s No pudo recuperar ID de la
			// tarea",vObservacion);
			// vObservacion2 = sprintf("%s No pudo recuperar ID de la tarea",vObservacion);
			// sprintf(vObservacion2,"%s No pudo recuperar ID de la tarea",vObservacion);
			vObservacion = String.format("%s No pudo recuperar ID de la tarea", vObservacion);

			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 21050701
		}

		if (dId_ord_tarea == null || dId_ord_tarea == 0) {
			try {
				aux_id = disOrdTareaDao.nextVal();
				// disOrdTareaDao.insert(99L, Double.valueOf(db_id_empresa).longValue(),
				// Double.valueOf(dIdOrden).longValue(),
				// Double.valueOf(dIdIrregularidad).longValue(),
				// Double.valueOf(dIdTarea).longValue(), "S");

				disOrdTareaDao.insert(DisOrdTareaBean.builder().id(aux_id).idEmpresa(db_id_empresa).idOrden(dIdOrden)
						.idAnormalidad(dIdIrregularidad).idTarea(dIdTarea).tareaEstado("S").build());

			} catch (Exception e) {
				error = 1;
				paso_trabajo = 7;
				vObservacion = String.format("No pudo Insertar la tarea");
				vCodOperacion = "ASY030";
				logger.error("Validando : {}. Error : {} \n", vObservacion, e.getMessage());
				logger.error("Traza 2");
				// throw e;
				// throw new Exception("Probando.");
				return false;// agregado por AVC 21050701
			}
		}

		try {
			disOrdTareaDao.updateTareaEstadoByIdOrdTarea(dId_ord_tarea);

		} catch (Exception e) {
			// TODO: handle exception

			error = 1;
			paso_trabajo = 7;
			vObservacion = String.format("Error: Al actualizar estado de la tarea");
			vCodOperacion = "ASY030";
			logger.error("Validando : {} . Error : {} \n", vObservacion, e.getMessage());
			return false;// agregado por AVC 21050701
		}

		return true;// agregado por AVC 21050701
	}

	private boolean vfnValidarTareas2() {

		try {
			dIdTarea = disTareaDao.findByCodTareaAndIdEmpresa(aCodTarea);
		} catch (Exception e) {
			vObservacion = String.format("Tarea no existe (%s).", aCodTarea);
			vCodOperacion = "ASY045";
			return false;
		}

		try {

			dIdIrregularidad = disAnormalidadDao.findByCodAnormalidadAndIdEmpresa(aCodIrregularidad);
			// } catch (EmptyResultDataAccessException e) {
			// vObservacion = String.format("Irregularidad no existe, asociada a ord de
			// inspeccion (%s)", aCodTarea);
			// vCodOperacion = "ASY045";
			// return false;

		} catch (Exception e) {
			vObservacion = String.format("Irregularidad no existe, asociada a ord de inspeccion (%s)", aCodTarea);
			vCodOperacion = "ASY045";
			return false;
		}

		return true;
	}

	private boolean ObtenerTareas() {

		int x;

		// EXEC SQL OPEN cXML_Tareas USING :vIdOrdTransfer, :vNroRecepciones;
		// if (bfnSQLError( "ERROR:OPEN Obtener Datos de Colecciones de Tareas",
		// __FILE__, __LINE__ ) == FALSE ) return ( 0 );

		// bzero(&xmlTareas,sizeof(xmlTareas));
		// EXEC SQL FETCH cXML_Tareas INTO :xmlTareas;
		// if (bfnSQLError( "ERROR:FETCH Obtener Datos de Colecciones de Tareas",
		// __FILE__, __LINE__ ) == FALSE ) return ( 0 );

		xmlTareas = tareaXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);

		int_cnt_tareas = xmlTareas.size();

		if (int_cnt_tareas != 0) {
			for (x = 0; x < int_cnt_tareas; x++) {
				/* VerificarTagXML(xmlTareas[x].CODIGOTAREA,66); */
				if (strcmp(xmlTareas.get(x).getCODIGOTAREA(), "") == 0) {
					vObservacion = String.format("Error: el Codigo Tarea es NULL(Posicion %d). ", x);
					vCodOperacion = "ASY045";
					logger.info("Validando : {}", vObservacion);
					return false;// agregado por AVC 20150701
				}
			}
		} else {
			vObservacion = String.format("Error: No exiten tareas configuradas");
			vCodOperacion = "ASY045";
			logger.info("Validando : {}", vObservacion);
			return false;// agregado por AVC 20150701
		}

		return true;// agregado por AVC 20150701
	}

	private boolean ObtenerAnormalidades() {
		int x;
		int intResult = 0;
		try {
			xmlAnormalidad = anormalidadXmlDao.findByIdOrdenInsp(dIdOrden_insp);

		} catch (Exception e) {
			logger.info("ERROR:FETCH Obtener Datos de Colecciones de Anormalidade.s");
			return false;
		}

		int_cnt_anormalidades = xmlAnormalidad.size();

		intResult = 0;
		if (int_cnt_anormalidades > 0) {
			CodAnormalidad = xmlAnormalidad.get(0).getCODIGOANORMALIDAD();
			logger.info("Codigo Anormalidad: {}", CodAnormalidad);
		}
		return true;
	}

	private void SetearContadores() {
		int_cnt_tareas = 0;
		int_cnt_anormalidades = 0;
		int_cnt_lecturas = 0;
		int_cnt_medidores = 0;
		paso_act = 99;// Indicador inicial de actualizacion de Ordenes en el Synergia
		paso_tarea = 99;
		paso_anomalia = 99;
	}

	private boolean ObtenerLecturas() {
		// EXEC SQL BEGIN DECLARE SECTION;
		String vNroMedidor; // [20];
		String vAccionMedidor; // [50];
		// EXEC SQL END DECLARE SECTION;
		int x;
		int xx;

		// for (SMedidorXmlBean medidorXmlBean : medidoresRet) {

		// }

		// for(xx=0;xx<intResultRetirado;xx++)
		xx = 0;
		for (SMedidorXmlBean medidorXmlBeanRetirado : medidoresRet) {

			vNroMedidor = medidorXmlBeanRetirado.getCNUMEROMEDIDOR();
			vAccionMedidor = medidorXmlBeanRetirado.getCACCIONMEDIDOR();

			xmlLecturas = lecturaXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones, vNroMedidor,
					vAccionMedidor);
			int_cnt_lecturas = xmlLecturas.size();

			if (int_cnt_lecturas != 0) {
				for (x = 0; x < int_cnt_lecturas; x++) {

					if (strcmp(xmlLecturas.get(x).getTIPOLECTURA(), "") == 0) {
						vObservacion = String.format("Error: el TIPOLECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);

						return false;
					}

					if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "") == 0) {
						vObservacion = String.format("Error: el HORARIOLECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (strcmp(xmlLecturas.get(x).getFECHALECTURA(), "") == 0) {
						vObservacion = String.format("Error: el FECHALECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (strcmp(xmlLecturas.get(x).getESTADOLEIDO(), "") == 0) {
						vObservacion = String.format("Error: el ESTADOLEIDO de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					} else {
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "ACFP") == 0) {
							medidorXmlBeanRetirado.setCLectENFP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "ACHP") == 0) {
							medidorXmlBeanRetirado.setCLectENHP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "DMFP") == 0) {
							medidorXmlBeanRetirado.setCLectDMFP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "DMHP") == 0) {
							medidorXmlBeanRetirado.setCLectDMHP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "REAC") == 0) {
							medidorXmlBeanRetirado.setCLectEnRe(xmlLecturas.get(x).getESTADOLEIDO());
						}
					}
				}
			} else {
				vObservacion = String.format("Error: No exiten Lecturas configuradas, para el Medidor (%s)",
						vNroMedidor);
				vCodOperacion = "ASY045";
				logger.info("Validando : {} ", vObservacion);
				return false;
			}
			xx++;
		}

		logger.info("medidoresRet: {}", medidoresRet);

		// for(xx=0;xx<intResultInstalado;xx++)
		xx = 0;

		for (SMedidorXmlBean medidorXmlBeanInst : medidoresInst) {
			vNroMedidor = medidorXmlBeanInst.getCNUMEROMEDIDOR();
			vAccionMedidor = medidorXmlBeanInst.getCACCIONMEDIDOR();

			// EXEC SQL OPEN cXML_Lecturas USING :vIdOrdTransfer, :vNroRecepciones
			// ,:vNroMedidor,:vAccionMedidor;
			// if (bfnSQLError( "ERROR:OPEN Obtener Datos de Colecciones de Lecturas",
			// __FILE__, __LINE__ ) == FALSE ) return ( 0 );

			// bzero(&xmlLecturas,sizeof(xmlLecturas));
			// EXEC SQL FETCH cXML_Lecturas INTO :xmlLecturas;
			// if (bfnSQLError( "ERROR:FETCH Obtener Datos de Colecciones de Lecturas",
			// __FILE__, __LINE__ ) == FALSE ) return ( 0 );

			xmlLecturas = lecturaXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones, vNroMedidor,
					vAccionMedidor);
			int_cnt_lecturas = xmlLecturas.size();

			// int_cnt_lecturas=sqlca.sqlerrd[2] ;

			if (int_cnt_lecturas != 0) {
				for (x = 0; x < int_cnt_lecturas; x++) {
					if (strcmp(xmlLecturas.get(x).getTIPOLECTURA(), "") == 0) {
						vObservacion = String.format("Error: el TIPOLECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "") == 0) {
						vObservacion = String.format("Error: el HORARIOLECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (strcmp(xmlLecturas.get(x).getFECHALECTURA(), "") == 0) {
						vObservacion = String.format("Error: el FECHALECTURA de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (strcmp(xmlLecturas.get(x).getESTADOLEIDO(), "") == 0) {
						vObservacion = String.format("Error: el ESTADOLEIDO de la Orden %s es NULL(Posicion %d).",
								vNroOrden, x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					} else {
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "ACFP") == 0) {
							medidorXmlBeanInst.setCLectENFP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "ACHP") == 0) {
							medidorXmlBeanInst.setCLectENHP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "DMFP") == 0) {
							medidorXmlBeanInst.setCLectDMFP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "DMHP") == 0) {
							medidorXmlBeanInst.setCLectDMHP(xmlLecturas.get(x).getESTADOLEIDO());
						}
						if (strcmp(xmlLecturas.get(x).getHORARIOLECTURA(), "REAC") == 0) {
							medidorXmlBeanInst.setCLectEnRe(xmlLecturas.get(x).getESTADOLEIDO());

						}
					}
				}
			} else {

				vObservacion = String.format("Error: No exiten Lecturas configuradas, para el Medidor : %s .",
						vNroMedidor);
				vCodOperacion = "ASY045";
				logger.info("Validando : {}", vObservacion);
				return false;
			}

			xx++;
		}

		logger.info("medidoresInst: {}", medidoresInst);

		logger.info("Se obtuvo las lecturas con exito");
		return true;

	}

	private boolean ObtenerMedidores() {

		medidoresInst = new ArrayList<>();
		medidoresRet = new ArrayList<>();

		List<MedidoresXML> listaMedidoresXML = new ArrayList<>();

		int x;
		int intResult = 0;
		int intResultLect = 0;

		intResultRetirado = 0;
		intResultInstalado = 0;

		/*
		 * SOLO PARA PRUEBAS INGRESAR DATOS DE UN XML CON DOS TAG DE MEDIDORES Long
		 * vIdOrdTransfer = 328953L; Long vNroRecepciones = 2L;
		 */
		xmlMedidores = medidorXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);
		logger.info("xmlMedidores: {}", xmlMedidores);
		int_cnt_medidores = xmlMedidores.size();
		logger.info("int_cnt_medidores: {}", int_cnt_medidores);

		for (x = 0; x < int_cnt_medidores; x++) {
			// bzero(&vNroMedidor,sizeof(vNroMedidor));
			// bzero(&vAccionMedidor,sizeof(vAccionMedidor));

			if (xmlMedidores.get(x).getNUMEROMEDIDOR() == null || xmlMedidores.get(x).getNUMEROMEDIDOR().equals("")) {
				// vObservacion = String.format("Error: el Numero de Componente (Accion:%s) es
				// NULO(Posicion %d).", xmlMedidores.get(x).getACCIONMEDIDOR(), x + 1);
				vObservacion = String.format(
						"Error: el Numero de Componente para la orden [%s] (Accion:[%s]) es NULO (Posicion [%d]).",
						vNroOrden, xmlMedidores.get(x).getACCIONMEDIDOR(), x + 1); /* ANL_20221108 */
				vCodOperacion = "ASY045";
				logger.info("Validando : {}", vObservacion);
				return false;
			}

			if (xmlMedidores.get(x).getACCIONMEDIDOR() == null || xmlMedidores.get(x).getACCIONMEDIDOR().equals("")) {
				// vObservacion = String.format("Error: La Accion del Medidor del Medidor %s es
				// NULL(Posicion %d).", xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1);
				vObservacion = String.format(
						"Error: La Accion del Medidor [%s] no puede ser Nulo (Posicion %d), orden [%s].",
						xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1, vNroOrden); /* ANL_20221108 */
				vCodOperacion = "ASY045";
				logger.info("Validando : %s", vObservacion);
				return false;
			} else {
				vAccionMedidor = xmlMedidores.get(x).getACCIONMEDIDOR();
				if (xmlMedidores.get(x).getACCIONMEDIDOR().equals("Encontrado")) {
					logger.info("Medidor accion Encontrado");
					// medidoresRet.set(intResultRetirado, SMedidorXmlBean
					// .builder()
					// .cNUMEROMEDIDOR(xmlMedidores.get(x).getNUMEROMEDIDOR())
					// .cMARCAMEDIDOR(xmlMedidores.get(x).getMARCAMEDIDOR())
					// .cMODELOMEDIDOR(xmlMedidores.get(x).getMODELOMEDIDOR())
					// .cACCIONMEDIDOR(xmlMedidores.get(x).getACCIONMEDIDOR())
					// .build());

					medidoresRet.add(SMedidorXmlBean.builder().cNUMEROMEDIDOR(xmlMedidores.get(x).getNUMEROMEDIDOR())
							.cMARCAMEDIDOR(xmlMedidores.get(x).getMARCAMEDIDOR())
							.cMODELOMEDIDOR(xmlMedidores.get(x).getMODELOMEDIDOR())
							.cACCIONMEDIDOR(xmlMedidores.get(x).getACCIONMEDIDOR())
							.cFACTOR(xmlMedidores.get(x).getFACTORMEDIDOR()).build());

					intResultRetirado = intResultRetirado + 1;
				}

				if (xmlMedidores.get(x).getACCIONMEDIDOR().equals("Instalado")) {
					logger.info("Medidor accion Instalado");
					// medidoresInst.set(intResultInstalado, SMedidorXmlBean
					// .builder()
					// .cNUMEROMEDIDOR(xmlMedidores.get(x).getNUMEROMEDIDOR())
					// .cMARCAMEDIDOR(xmlMedidores.get(x).getMARCAMEDIDOR())
					// .cMODELOMEDIDOR(xmlMedidores.get(x).getMODELOMEDIDOR())
					// .cACCIONMEDIDOR(xmlMedidores.get(x).getACCIONMEDIDOR())
					// .cPROPIEDADMEDIDOR(xmlMedidores.get(x).getPROPIEDADMEDIDOR())
					// .fechaInstalacion(xmlMedidores.get(x).getFechaInstalacion())
					// .build());

					medidoresInst.add(SMedidorXmlBean.builder().cNUMEROMEDIDOR(xmlMedidores.get(x).getNUMEROMEDIDOR())
							.cMARCAMEDIDOR(xmlMedidores.get(x).getMARCAMEDIDOR())
							.cMODELOMEDIDOR(xmlMedidores.get(x).getMODELOMEDIDOR())
							.cACCIONMEDIDOR(xmlMedidores.get(x).getACCIONMEDIDOR())
							.cPROPIEDADMEDIDOR(xmlMedidores.get(x).getPROPIEDADMEDIDOR())
							.fechaInstalacion(xmlMedidores.get(x).getFechaInstalacion())
							.cFACTOR(xmlMedidores.get(x).getFACTORMEDIDOR()).build());

					if (medidoresInst.get(intResultInstalado).getCPROPIEDADMEDIDOR() == null
							|| medidoresInst.get(intResultInstalado).getCPROPIEDADMEDIDOR().equals("")) {
						vObservacion = String.format(
								"Error: El valor de la Propíedad del Medidor Instalado(%s) es NULL(Posicion %d).",
								xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (medidoresInst.get(intResultInstalado).getFechaInstalacion() == null
							|| medidoresInst.get(intResultInstalado).getFechaInstalacion().equals("")) {
						vObservacion = String.format(
								"Error: El valor de fecha de instalacion del Medidor Instalado(%s) es NULL(Posicion %d).",
								xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1);
						vCodOperacion = "ASY045";
						logger.info("Validando : {}", vObservacion);
						return false;
					}

					if (medidoresInst.get(intResultInstalado).getCFACTOR() != null
							&& !medidoresInst.get(intResultInstalado).getCFACTOR().equals("")) {
						if (!validarFactor(medidoresInst.get(intResultInstalado).getCNUMEROMEDIDOR(),
								medidoresInst.get(intResultInstalado).getCMODELOMEDIDOR(),
								medidoresInst.get(intResultInstalado).getCMARCAMEDIDOR(),
								medidoresInst.get(intResultInstalado).getCFACTOR())) {
							vObservacion = "Error: El valor del factor de facturación del medidor no es válido.";
							vCodOperacion = "ASY045";
							logger.info("Validando : {}", vObservacion);
							return false;
						}
					}

					intResultInstalado = intResultInstalado + 1;
				}
			}

			if (xmlMedidores.get(x).getMARCAMEDIDOR() == null || xmlMedidores.get(x).getMARCAMEDIDOR().equals("")) {
				vObservacion = String.format("Error: La Marca de Componente del Medidor %s es NULL(Posicion %d).",
						xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1);
				vCodOperacion = "ASY045";
				logger.info("Validando : {}", vObservacion);
				return false;
			}

			if (xmlMedidores.get(x).getMODELOMEDIDOR() == null || xmlMedidores.get(x).getMODELOMEDIDOR().equals("")) {
				vObservacion = String.format("Error: El Modelo de Componente del Medidor %s es NULL(Posicion %d).",
						xmlMedidores.get(x).getNUMEROMEDIDOR(), x + 1);
				vCodOperacion = "ASY045";
				logger.info("Validando : {}", vObservacion);
				return false;
			}

			/*
			 * //INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización)
			 * - JEGALARZA MedidoresXML medidoresxml = new MedidoresXML();
			 * 
			 * medidoresxml.setAccionMedidor(xmlMedidores.get(x).getACCIONMEDIDOR());
			 * medidoresxml.setMedidor(xmlMedidores.get(x).getNUMEROMEDIDOR());
			 * //numero_componente
			 * medidoresxml.setFactorMedidor(xmlMedidores.get(x).getFACTORMEDIDOR());
			 * medidoresxml.setModeloMedidor(xmlMedidores.get(x).getMODELOMEDIDOR());
			 * medidoresxml.setMarcaMedidor(xmlMedidores.get(x).getMARCAMEDIDOR());
			 * 
			 * listaMedidoresXML.add(medidoresxml);
			 */

		}
		/*
		 * int contador_list = listaMedidoresXML.size();
		 * 
		 * try {
		 * 
		 * if( contador_list == 2 ) {
		 * 
		 * String v_accionMedidor_xml_encontrado =
		 * xmlMedidores.get(0).getACCIONMEDIDOR(); String v_medidor_xml_encontrado = "";
		 * String v_medidor_xml_instalado = ""; String v_factorMedidor_xml_encontrado =
		 * ""; String v_factorMedidor_xml_instalado = ""; String
		 * v_modeloMedidor_xml_encontrado = ""; String v_modeloMedidor_xml_instalado =
		 * ""; String v_marcaMedidor_xml_encontrado = ""; String
		 * v_marcaMedidor_xml_instalado = ""; int validar_factor = 0;
		 * 
		 * if( v_accionMedidor_xml_encontrado.equals("Encontrado") ) {
		 * 
		 * v_medidor_xml_encontrado = xmlMedidores.get(0).getNUMEROMEDIDOR();
		 * v_medidor_xml_instalado = xmlMedidores.get(1).getNUMEROMEDIDOR();
		 * v_factorMedidor_xml_encontrado = xmlMedidores.get(0).getFACTORMEDIDOR();
		 * v_factorMedidor_xml_instalado = xmlMedidores.get(1).getFACTORMEDIDOR();
		 * v_modeloMedidor_xml_encontrado = xmlMedidores.get(0).getMODELOMEDIDOR();
		 * v_modeloMedidor_xml_instalado = xmlMedidores.get(1).getMODELOMEDIDOR();
		 * v_marcaMedidor_xml_encontrado = xmlMedidores.get(0).getMARCAMEDIDOR();
		 * v_marcaMedidor_xml_instalado = xmlMedidores.get(1).getMARCAMEDIDOR();
		 * 
		 * }else {
		 * 
		 * v_medidor_xml_encontrado = xmlMedidores.get(1).getNUMEROMEDIDOR();
		 * v_medidor_xml_instalado = xmlMedidores.get(0).getNUMEROMEDIDOR();
		 * v_factorMedidor_xml_encontrado = xmlMedidores.get(1).getFACTORMEDIDOR();
		 * v_factorMedidor_xml_instalado = xmlMedidores.get(0).getFACTORMEDIDOR();
		 * v_modeloMedidor_xml_encontrado = xmlMedidores.get(1).getMODELOMEDIDOR();
		 * v_modeloMedidor_xml_instalado = xmlMedidores.get(0).getMODELOMEDIDOR();
		 * v_marcaMedidor_xml_encontrado = xmlMedidores.get(1).getMARCAMEDIDOR();
		 * v_marcaMedidor_xml_instalado = xmlMedidores.get(0).getMARCAMEDIDOR();
		 * 
		 * }
		 * 
		 * if( v_factorMedidor_xml_encontrado != null ||
		 * !v_factorMedidor_xml_encontrado.equals("") ) { try {
		 * 
		 * if( v_factorMedidor_xml_encontrado == null ||
		 * v_factorMedidor_xml_encontrado.equals("")) { v_factorMedidor_xml_encontrado =
		 * medidorXmlDao.selectMedidorEncontrado(v_modeloMedidor_xml_encontrado,
		 * v_marcaMedidor_xml_encontrado, v_medidor_xml_encontrado ); }
		 * 
		 * 
		 * if( v_medidor_xml_encontrado.equals(v_medidor_xml_instalado) &&
		 * !v_factorMedidor_xml_encontrado.equals(v_factorMedidor_xml_instalado) ) { //
		 * debe ser equals && !.equeals //UPDATE try {
		 * medidorXmlDao.actualizarMedidores(v_medidor_xml_instalado,
		 * v_modeloMedidor_xml_instalado, v_marcaMedidor_xml_instalado,
		 * v_factorMedidor_xml_instalado); } catch (Exception e) {
		 * logger.error("Error: no se pudo actualizar el query ... : "+e); } }
		 * 
		 * } catch (Exception e) { logger.error("Error: " + e); }
		 * 
		 * }
		 * 
		 * 
		 * }else { logger.info("Solo existe un fac en el TAG Medidor"); }
		 * 
		 * } catch (Exception e) {
		 * logger.error("Error, no ingreso al query modificar medidores: "+e); }
		 */

		// FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) -
		// JEGALARZA

		logger.info("Medidores ({}): {}", xmlMedidores.size(), xmlMedidores);
		logger.info("Medidores inst({}): {}", medidoresInst.size(), medidoresInst);
		logger.info("Medidores ret({}): {}", medidoresRet.size(), medidoresRet);
		return true;
	}

	public boolean validarFactor(String numMedidor, String modeloMedidor, String marcaMedidor, String factor) {
		int countFactoresValidos = medidorXmlDao.obtenerCountFactoresValidos(numMedidor, modeloMedidor, marcaMedidor,
				factor);
		logger.info("countFactoresValidos: {}", countFactoresValidos);
		return countFactoresValidos != 0;
	}

	// INCIIO - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) -
	// JEGALARZA
	private boolean cambioDeFactor() {
		logger.info("Metodo CambioDeFator inicio.");
		for (SMedidorXmlBean medidorXmlInstalado : medidoresInst) {
			int contador_list_instalado = medidoresInst.size();
			if (contador_list_instalado == 1) {
				String v_medidor_xml_instalado = medidorXmlInstalado.getCNUMEROMEDIDOR();
				String v_factorMedidor_xml_instalado = medidorXmlInstalado.getCFACTOR();
				String v_modeloMedidor_xml_instalado = medidorXmlInstalado.getCMODELOMEDIDOR();
				String v_marcaMedidor_xml_instalado = medidorXmlInstalado.getCMARCAMEDIDOR();
				if (v_factorMedidor_xml_instalado != null && !v_factorMedidor_xml_instalado.equals("")) {
					try {
						String factorInstaladoActual = medidorXmlDao.selectMedidorEncontrado(
								v_modeloMedidor_xml_instalado, v_marcaMedidor_xml_instalado, v_medidor_xml_instalado);
						if (!factorInstaladoActual.equals(v_factorMedidor_xml_instalado)) {
							try {
								medidorXmlDao.actualizarMedidores(v_medidor_xml_instalado,
										v_modeloMedidor_xml_instalado, v_marcaMedidor_xml_instalado,
										v_factorMedidor_xml_instalado);
							} catch (Exception e) {
								logger.error("Error: no se pudo actualizar el query ... : " + e);
								return false;
							}
						} else {
							logger.info("El factor del medidor instalado actual es igual al nuevo factor");
						}

					} catch (Exception e) {
						logger.error("Error: " + e);
						return false;
					}
				} else {
					logger.info("No existe el factor del medidor instalado");
				}
			} else {
				logger.info("Hay más de un medidor instalado");
			}
		}
		return true;
	}
	// FIN - REQ # 10 - Normalizaciones (Recepción de Ordenes de Normalización) -
	// JEGALARZA

	private void ParametrosOrden() {
		NumeroOrden = vNroOrden;

		CodResultadoCausal = xmlDatos.get(0).getCODIGO_CAUSAL_RESULTADO();
		CodResultado = xmlDatos.get(0).getCODIGO_RESULTADO();
		LLave_Secreta = xmlDatos.get(0).getLLAVE_SECRETA();
		CodProceso = xmlDatos.get(0).getCODIGO_PROCESO();
		CodSubProceso = xmlDatos.get(0).getCODIGO_SUBPROCESO();
		CodActComercial = xmlGPNormalizacion.get(0).getCodActComercial();
		// Observacion_NotOpe[1200] = '\0';
		Observacion = Observacion_NotOpe;
		NumOTFisica = xmlGPNormalizacion.get(0).getNroNotificacion();

		FechaIniEjec = xmlDatos.get(0).getINICIO_TDC();
		FechaFinEjec = xmlDatos.get(0).getFIN_TDC();

		NumeroOrdenInsp = xmlGPNormalizacion.get(0).getNroOrdenInspeccion();
		logger.info("Numero Orden Insp: {}", NumeroOrdenInsp);

		TieneCambioMed = xmlGPNormalizacion.get(0).getTieneCambioMed();
		if (TieneCambioMed.equals("SI")) {
			TieneCambioMed = "S";
		} else {
			TieneCambioMed = "N";
		}

		if (FechaIniEjec.length() > 10) {
			StringBuilder string1 = new StringBuilder(FechaIniEjec);
			logger.info("Fecha Inic: {}", string1);
			string1.setCharAt(10, ' ');
			FechaIniEjec = string1.substring(0, 19);
			logger.info("Fecha Inic: {}", FechaFinEjec);
		}

		if (FechaFinEjec.length() > 10) {
			StringBuilder string2 = new StringBuilder(FechaFinEjec);
			logger.info("Fecha Fin: {}", string2);
			string2.setCharAt(10, ' ');
			FechaFinEjec = string2.substring(0, 19);
			logger.info("Fecha Fin: {}", FechaFinEjec);
		}

		FechaNormalizacion = FechaFinEjec;
		dIdOrden = 0L;
		dIdOrden_insp = 0L;
		dIdWorkflowOrd = 0L;
		dIdServicio = 0L;
		dIdWorkflowSeg = 0;
		dIdCuadrilla = 0;
		dIdProducto = 0;
		dIdJefeProducto = 0;
		dIdCordCuadrilla = 0;
		dIdEjecutor = 0L;
		dIdResultado = 0;
		dIdParentesco = 0L;
		dIdActComercial = 0L;

		NumOTFisica = NumOTFisica == null ? NumOTFisica : NumOTFisica.trim();

	}

	private void ObtenerGPNormalizacion() {
		xmlGPNormalizacion = gpNormalizacionXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);
		logger.info("GPNormalizacion: {}", xmlGPNormalizacion.get(0));
	}

	private void ObtenerSellos() {
		xmlSellos = selloXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);

	}

	// private void PrepareCursoresProceso() {
	// PrepareObtenerDatos();
	// PrepareObtenerOperaciones();
	// //PrepareObtenerAnexos();
	// PrepareObtenerMedidores();
	// PrepareObtenerLecturas();
	// PrepareObtenerMedidores4J();//agregado por AVC 20150820 - EPT_PER_02 -
	// Solicitado por FVERA
	// PrepareObtenerSellos();
	// PrepareObtenerAnormalidades();
	// PrepareObtenerTareas();
	// PrepareObtenerGPNormalizacion();
	// }

	private void ObtenerOperaciones() {

		xmlOperaciones = operacionXmlDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);
		// records the number of rows processed
		for (int x = 0; x < xmlOperaciones.size(); x++) {
			if (x == 0)
				Observacion_NotOpe = xmlOperaciones.get(x).getNOTAS_OPERACION();
			else {
				Observacion_NotOpe += " ";
				Observacion_NotOpe += xmlOperaciones.get(x).getNOTAS_OPERACION();
			}
		}

		logger.info("Operacion: {}", Observacion_NotOpe);

	}

	private boolean validarContratista() {
		cntcont = 0;
		cCod_Contratista = eorOrdTransferDetDao.getCodigoContratistaByIdOrdTransferAndNroEvento(vIdOrdTransfer,
				vNroRecepciones);
		if (cCod_Contratista == null) {
			logger.info("Error: No se pudo obtener el codigo de contratista para <{}>", vIdOrdTransfer);
			escribirArchivo(arch_error,
					String.format("No se pudo obtener el codigo de contratista para vIdOrdTransfer = %ld%s",
							vIdOrdTransfer, System.lineSeparator()));
			vCodOperacion = vCodErrASY099;
			vObservacion = "No se pudo obtener el codigo de contatista (";
			vObservacion += cCod_Contratista;
			vObservacion += ")";

			return false;
		}
		cCod_Contratista = cCod_Contratista == null ? "" : cCod_Contratista.trim();
		if (cCod_Contratista.equals("")) {
			logger.info("Error: Codigo de contratista ({}) Vacio o Nulo", cCod_Contratista);
			escribirArchivo(arch_error,
					String.format("Codigo de contratista (%s) Vacío o Nulo%s", vIdOrdTransfer, System.lineSeparator()));
			// int_ordenes_error++;
			vCodOperacion = vCodErrASY099;
			vObservacion = "Codigo de contratista Vacío o Nulo";
			return false;
		}

		if (comContratistaDao.getCountByCodContratista(cCod_Contratista) == 0) {
			logger.info("Error: El contratista [{}] no esta configurado en COM_CONTRATISTA", cCod_Contratista);
			escribirArchivo(arch_error, String.format("El contratista [{}] no esta configurado en COM_CONTRATISTA%n",
					cCod_Contratista, System.lineSeparator()));
			vCodOperacion = vCodErrASY099;
			vObservacion = "El contratista (";
			vObservacion += cCod_Contratista;
			vObservacion += ") no esta configurado en COM_CONTRATISTA";
			logger.info(vObservacion);
			return false;
		}

		logger.info("Cod Contratista: {}", cCod_Contratista);

		ComParametroBean comParametroBeanCodCuadrilla = comParametroDao
				.findBySistemaAndEntidadAndCodigoAndActivo("EORDER", "CUADRILLA_UNICA", cCod_Contratista, "S");
		if (comParametroBeanCodCuadrilla == null) {
			logger.info("Error: No se pudo obtener el codigo de cuadrilla para {}", vIdOrdTransfer);
			escribirArchivo(arch_error,
					String.format("No se pudo obtener el codigo de cuadrilla para vIdOrdTransfer = %ld%s",
							vIdOrdTransfer, System.lineSeparator()));
			vCodOperacion = vCodErrASY099;
			vObservacion = "No se pudo obtener el codigo de cuadrilla para Contratista(";
			vObservacion = cCod_Contratista;
			vObservacion = ")";
			return false;
		}
		CodCuadrilla = comParametroBeanCodCuadrilla.getValor_alf();
		CodCuadrilla = CodCuadrilla.trim();

		return true;
	}

	private void ObtenerDatos() {
		xmlDatos = eorOrdTransferDetDao.findByIdOrdTransferAndNroEvento(vIdOrdTransfer, vNroRecepciones);
		Tdc_Creada_Campo = xmlDatos.get(0).getTDC_CREADO_EN_EORDER();
		logger.info("TDC Creada en campo: {}", Tdc_Creada_Campo);
	}

	private boolean ObtenerTipoTDC() {
		ComParametroBean comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "TIPO_TDC",
				V_VARIABLE);
		if (comParametroBean == null) {
			logger.info("Error: No se encuentra configurado Tipo de TDC.");
			return false;
		}
		vCodTipoTDC = comParametroBean.getValor_alf();
		logger.info("Se obtuvo tipo TDC: {}", vCodTipoTDC);
		return true;
	}

	private boolean crearArchivo(String archivo) {
		Path rutaArchivo = Paths.get(archivo);
		try {

			Files.createFile(rutaArchivo);
			logger.info("Archivo {} creado.", rutaArchivo);
		} catch (IOException e) {
			logger.error("No se pudo crear el archivo {}.", rutaArchivo);
			logger.error(e);
			return false;
		}
		return true;
	}

	private boolean crearArchivoRandomAccess(String archivo) {

		try (RandomAccessFile fError1 = new RandomAccessFile(archivo, "rw")) {
			logger.info("Archivo {} creado.", archivo);
		} catch (IOException e) {
			logger.error("No se pudo crear el archivo {}.", archivo);
			logger.error(e);
		}

		return true;
	}

	private boolean ObtenerRutas() {
		// bzero(path_error,sizeof(path_error));
		// bzero(path_salida,sizeof(path_salida));

		// logger.info("path_error: {}.", path_error);
		// logger.info("path_salida: {}.", path_salida);
		// Arrays.fill(path_error.getBytes(), (byte)0);
		// Arrays.fill(path_salida.getBytes(), (byte)0);
		// logger.info("Arrays.fill...");
		// logger.info("path_error: {}.", path_error);
		// logger.info("path_salida: {}.", path_salida);

		ComParametroBean comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "PATH_PER",
				"PER_LGC_OU");
		if (comParametroBean == null) {
			logger.error("Error: No se encuentra configurada Ruta de Salida.");
			return false;
		}
		db_ruta_salida = comParametroBean.getValor_alf();
		logger.info("db_ruta_salida: {}.", db_ruta_salida);
		path_error = db_ruta_salida;

		// borrar/comentar
		// path_error = RUTA_LINUX_DES_LOCAL.concat(path_error);

		return true;
	}

	private boolean ObtenerErroresTransferencia() {
		ComParametroBean comParametroBean;

		comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "ESTADO_ERRSYN", "ASY000");
		if (comParametroBean == null) {
			logger.error("Error: No se encuentra configurado Error de Transferencia (ASY000).");
			return false;
		}
		vCodErrASY000 = comParametroBean.getValor_num().toString();
		vDesErrASY000 = comParametroBean.getDescripcion();
		logger.info("vCodErrASY000: {}.", vCodErrASY000);
		logger.info("vDesErrASY000: {}.", vDesErrASY000);

		comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "ESTADO_ERRSYN", "ASY013");
		if (comParametroBean == null) {
			logger.error("Error: No se encuentra configurado Error de Transferencia (ASY013).");
			return false;
		}
		vCodErrASY013 = comParametroBean.getValor_num().toString();
		vDesErrASY013 = comParametroBean.getDescripcion();
		logger.info("vCodErrASY013: {}.", vCodErrASY013);
		logger.info("vDesErrASY013: {}.", vDesErrASY013);

		comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "ESTADO_ERRSYN", "ASY024");
		if (comParametroBean == null) {
			logger.error("Error: No se encuentra configurado Error de Transferencia (ASY024).");
			return false;
		}
		vCodErrASY024 = comParametroBean.getValor_num().toString();
		vDesErrASY024 = comParametroBean.getDescripcion();
		logger.info("vCodErrASY024: {}.", vCodErrASY024);
		logger.info("vDesErrASY024: {}.", vDesErrASY024);

		comParametroBean = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER", "ESTADO_ERRSYN", "ASY099");
		if (comParametroBean == null) {
			logger.error("Error: No se encuentra configurado Error de Transferencia (ASY099).");
			return false;
		}
		vCodErrASY099 = comParametroBean.getValor_alf();
		vDesErrASY099 = comParametroBean.getDescripcion();
		logger.info("vCodErrASY099: {}.", vCodErrASY099);
		logger.info("vDesErrASY099: {}.", vDesErrASY099);

		return true;
	}

	private boolean ObtenerEstadosTransferencia() {
		ComParametroBean comParametroBean1 = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER",
				"ESTADO_TRANSFERENCIA", "PRECE");
		if (comParametroBean1 == null) {
			logger.error("Error: No se encuentra configurado Estados de Transferencia (Pendiente de Recepcion).");
			return false;
		}
		vCodEstPendRecep = comParametroBean1.getValor_num();
		logger.info("vCodEstPendRecep: {}.", vCodEstPendRecep);

		ComParametroBean comParametroBean2 = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER",
				"ESTADO_TRANSFERENCIA", "PROCE");
		if (comParametroBean2 == null) {
			logger.error("Error: No se encuentra configurado Estados de Transferencia (En Proceso).");
			return false;
		}
		vCodEstEnProceso = comParametroBean2.getValor_num();
		logger.info("vCodEstEnProceso: {}.", vCodEstEnProceso);

		ComParametroBean comParametroBean3 = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER",
				"ESTADO_TRANSFERENCIA", "RECEP");
		if (comParametroBean3 == null) {
			logger.error("Error: No se encuentra configurado Estados de Transferencia (Recepcionado).");
			return false;
		}
		vCodEstRecep = comParametroBean3.getValor_num();
		logger.info("vCodEstRecep: {}.", vCodEstRecep);

		ComParametroBean comParametroBean4 = comParametroDao.findBySistemaAndEntidadAndCodigo("EORDER",
				"ESTADO_TRANSFERENCIA", "RECER");
		if (comParametroBean4 == null) {
			logger.error("Error: No se encuentra configurado Estados de Transferencia (Recepcionad con Error).");
			return false;
		}
		vCodEstRecepError = comParametroBean4.getValor_num();
		logger.info("vCodEstRecepError: {}.", vCodEstRecepError);

		return true;
	}

	private boolean ObtenerIDEmpresa_Usuario() {

		NucEmpresaBean empresaBean = nucEmpresaDao.findByCodPartition(db_cod_empresa);
		if (empresaBean == null) {
			logger.error("Error: No existe ID_EMPRESA para el parametro <Empresa> ingresado.");
			return false;
		}
		db_id_empresa = empresaBean.getId_empresa();

		UsuarioBean usuarioBean = usuarioDao.findByUsername(db_cod_usuario);
		if (usuarioBean == null) {
			logger.error("Error: No existe ID_USUARIO para el parametro <Cod_Usuario> ingresado.");
			return false;
		}
		db_id_usuario = usuarioBean.getId();

		return true;
	}

	private boolean escribirArchivo(String ruta, String cadena) {

		Path path = Paths.get(ruta);
		byte[] strToBytes = cadena.getBytes();

		try {
			Files.write(path, strToBytes, StandardOpenOption.APPEND);
			logger.info("Archivo {} escrito.", path);

		} catch (IOException e) {
			logger.error("No se pudo escribir el archivo {}.", ruta);

			logger.error(e.getMessage());

			logger.error(e);

			return false;

		}

		return true;

	}

	private int strcmp(String str1, String str2) {

		if (str1 == null) {
			str1 = "";
		}
		int l1 = str1.length();
		int l2 = str2.length();
		int lmin = Math.min(l1, l2);

		for (int i = 0; i < lmin; i++) {
			int str1_ch = (int) str1.charAt(i);
			int str2_ch = (int) str2.charAt(i);

			if (str1_ch != str2_ch) {
				return str1_ch - str2_ch;
			}
		}

		// Edge case for strings like
		// String 1="Geeks" and String 2="Geeksforgeeks"
		if (l1 != l2) {
			return l1 - l2;
		}

		// If none of the above conditions is true,
		// it implies both the strings are equal
		else {
			return 0;
		}
	}

	public void probarRichard1() {

		/*
		 * int rows = medComponenteDao.updateComponenteFuenteCliente(32167410660999L,
		 * 7L); logger.info("rows: {}.", rows);
		 * 
		 * int rows = medComponenteDao.updateComponenteFuenteEjecutor(32167410660999L,
		 * 5009187L); logger.info("rows: {}.", rows);
		 * 
		 * int rows = medComponenteDao.updateComponenteInstalar(32167410660999L,
		 * CodActComercial, NumeroServicio, NumeroOrden); logger.info("rows: {}.",
		 * rows);
		 */
		/*
		 * Long idComponenteElectrico = medComponenteDao.
		 * getIdComponenteElectricoByIdServicioAndNroComponenteAndMarcaMedidorAndModeloMedidor
		 * (5706207L, "23265013", "CMP", "LAA01");
		 * logger.info("idComponenteElectrico: {}.", idComponenteElectrico);
		 * 
		 * Long idComponenteDisponible = medComponenteDao.
		 * getIdComponenteDisponibleByNroComponenteAndIdModeloAndIdEmpresa("437093",
		 * 6410101213126566L); logger.info("idComponenteDisponible: {}.",
		 * idComponenteDisponible);
		 * 
		 * 
		 * MedidorMedidaDatoBean medidorMedidaDatoBean =
		 * medidorMedidaDatoDao.findByIdComponenteRetirar(idComponenteDisponible);
		 * logger.info(medidorMedidaDatoBean.toString());
		 * 
		 * medidorMedidaDatoBean =
		 * medidorMedidaDatoDao.findByIdComponenteInstalar(idComponenteDisponible);
		 * logger.info(medidorMedidaDatoBean.toString());
		 * 
		 * OrdInspLecturaBean ordInspLecturaBean = OrdInspLecturaBean.builder()
		 * .idEmpresa(3L) .idMediInsp(8L) .idLectura(4L) .build();
		 * 
		 * int rows = ordInspLecturaDao.insert(ordInspLecturaBean);
		 * logger.info("rows: {}.", rows);
		 * 
		 * Long id = ordInspLecturaDao.nextVal(); logger.info("id: {}.", id);
		 * 
		 * OrdMediCambioBean ordMediCambioBean = OrdMediCambioBean.builder()
		 * .idOrden(dIdOrden) .idComponente(NumeroOrden) .build();
		 * 
		 * 
		 * int rows = ordMediCambioDao.insert(ordMediCambioBean);
		 * logger.info("rows: {}.", rows);
		 */
		/*
		 * Long id = ordMediInspDao.nextVal(); logger.info("id: {}.", id);
		 * 
		 * OrdMediInspBean ordMediInspBean = OrdMediInspBean.builder() .idOrden(id)
		 * .idComponente(id) .idEmpresa(id) .idMediInsp(id) .build();
		 * 
		 * int rows = ordMediInspDao.insert(ordMediInspBean); logger.info("rows: {}.",
		 * rows);
		 * 
		 * System.exit(0);
		 */

	}

	public void probar() {

		// String cadena = "Hola como estas %s.";
		// String nombre = "Leona";
		// cadena = String.format(cadena, nombre);
		// logger.info(cadena);
		// int a = 2;
		// long aa = 2;
		// double aaa = 2;
		// boolean aaaa = true;

		// String var1 = "";
		// Long numeroOrden = 2332L;
		// var1 = String.format("Error al insertar Estado de la Orden(Workflow). Nro.
		// Orden: %s .", numeroOrden);
		// logger.info(var1);

		// Comercial 4J
		// Long idServicio = 86L;
		// Long secMagnitud;
		// secMagnitud = srvElectricoDao.getSecMagnitudByIdServicio(idServicio);
		// logger.info("secMagnitud: {}.", secMagnitud);
		// logger.info("Mayor a cero: {}.", secMagnitud > 0);
		// logger.info("Diferente de null: {}.", secMagnitud != null);

		// secMagnitud = srvElectricoDao.getSecMagnitudByIdServicio(idServicio);
		// logger.info("secMagnitud: {}.", secMagnitud);
		// logger.info("Mayor a cero: {}.", secMagnitud > 0);
		// logger.info("Diferente de null: {}.", secMagnitud != null);

		// IWkfWorkflowDao 3
		logger.info("wkfWorkflowDao.updateIdStateByIdWorkflow: {}.",
				wkfWorkflowDao.updateIdStateByIdWorkflow(21791289286081L, "Recibida ccc"));
		logger.info("wkfWorkflowDao.updateIdStateByIdWorkflowAndIdState: {}.",
				wkfWorkflowDao.updateIdStateByIdWorkflowAndIdState(21791289286081L, "Recibida ccc", "Recibida"));
		logger.info("wkfWorkflowDao.findById: {}.", wkfWorkflowDao.findById(21791289286081L));
		// IUsuarioDao 1
		logger.info("Usuario: {}.", usuarioDao.findByUsername("MVENEGAS"));
		// ITareaXmlDao 1
		logger.info("tareasXmlBean: {}.", tareaXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L));
		// ISrvElectricoDao 2
		logger.info("tareasXmlBean: {}.", srvElectricoDao.updateSecMagnitudByIdServicio(86L, 5L));
		logger.info("tareasXmlBean: {}.", srvElectricoDao.getSecMagnitudByIdServicio(86L));
		// ISrvActComercialDao 1
		logger.info("SrvActComercial: {}.", srvActComercialDao.findByCodActComercialAndIdEmpresa("Q0", 3L));
		// SelloXmlDaoImpl 0 IGNORAR
		// IOrdTipoOrdenDao 1
		logger.info("Cantidad: {}.", ordTipoOrdenDao.getCountByCodInternoAndCodTipoOrden("OrdenNormalizacion", "NORM"));
		// IOrdOrdenDao 4
		logger.info("updateFechaFinalizacionById: {}.",
				ordOrdenDao.updateFechaFinalizacionById(1520196808198L, "2021-03-21 15:39:45"));
		logger.info("findByIdOrdenAndDiscriminador: {}.",
				ordOrdenDao.findByIdOrdenAndDiscriminador(1520196215402L, "INSPECCION"));
		logger.info("findAaByNroOrden: {}.", ordOrdenDao.findAaByNroOrden("77444283"));
		logger.info("findAbByNroOrden: {}.", ordOrdenDao.findAbByNroOrden("1000016", 3L));
		// ILecturaXmlDao
		logger.info("lecturasXmlBean: {}.",
				lecturaXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L, "111264", "Encontrado"));

	}

	public void probarRichard2() {

		cCod_Contratista = "13006";
		Long count = comContratistaDao.getCountByCodContratista(cCod_Contratista);
		logger.info("count: {}.", count);

		CodCuadrilla = "N135E";
		Long countCuadrilla = comCuadrillaDao.getCountByCodCuadrillaAndIdEmpresa(CodCuadrilla);
		logger.info("countCuadrilla: {}.", countCuadrilla);

		ComCuadrillaAaBean comCuadrillaAaBean = comCuadrillaDao.findAaByCodCuadrillaAndIdEmpresa(CodCuadrilla);
		logger.info("idCuadrilla: {}.", comCuadrillaAaBean.getIdCuadrilla());
		logger.info("idEjecutor: {}.", comCuadrillaAaBean.getIdEjecutor());

		CodParentesco = "6";
		Long idParentesco = comParentescoDao.findByCodParentescoAndIdEmpresa(CodParentesco, 3L);
		logger.info("idParentesco: {}.", idParentesco);

		CodAnormalidad = "46";
		Long idAnormalidad = disAnormalidadDao.findByCodAnormalidadAndIdEmpresa(CodAnormalidad);
		logger.info("idAnormalidad: {}.", idAnormalidad);

		dIdOrden = 1520196111391L;
		Long idOrdInsp = disOrdNormDao.findByIdOrden(dIdOrden);
		logger.info("idOrdInsp: {}.", idOrdInsp);

		/*
		 * DisOrdNormBean disOrdNormBean = DisOrdNormBean .builder()
		 * .idEjecutorEje(comCuadrillaAaBean.getIdEjecutor()) .fecHoraIniEje(new
		 * java.util.Date()) .fecHoraFinEje(new java.util.Date()) .idResultado(5L)
		 * .idOrden(dIdOrden) .build(); int rows = disOrdNormDao.update(disOrdNormBean);
		 * logger.info("rows: {}.", rows);
		 */

		Long idTarea = disOrdTareaDao.getIdOrdTareaByIdEmpresaAndIdOrdenAndIdIrregularidadAndIdTarea(1520216848432L,
				240L, 0L);
		logger.info("idTarea: {}.", idTarea);

		Long idOrdTarea = disOrdTareaDao.nextVal();
		logger.info("idOrdTarea: {}.", idOrdTarea);
		DisOrdTareaBean disOrdTareaBean = DisOrdTareaBean.builder().id(idOrdTarea).idOrden(dIdOrden)
				.idAnormalidad(idAnormalidad).idTarea(idTarea).build();

		/*
		 * int rows = disOrdTareaDao.insert(disOrdTareaBean); logger.info("rows: {}.",
		 * rows);
		 * 
		 * rows = disOrdTareaDao.updateTareaEstadoByIdOrdTarea(idOrdTarea);
		 * logger.info("rows: {}.", rows);
		 * 
		 * rows = disOrdTareaDao.updateTareaEstadoByIdOrden(dIdOrden);
		 * logger.info("rows: {}.", rows);
		 */

		DisResultadoInspAaBean disResultadoInspAaBean = disResultadoInspDao.findByCodResultadoAndIdEmpresa("1");
		logger.info("idResultado: {}.", disResultadoInspAaBean.getId());
		logger.info("codInterno: {}.", disResultadoInspAaBean.getCodInterno());

		idTarea = disTareaDao.findByCodTareaAndIdEmpresa("41");
		logger.info("idTarea: {}.", idTarea);

		int countInterfaz = eorInterfazCnrDao.getCountByNumeroInspeccionAndTipoOrden(77463525L);
		logger.info("countInterfaz: {}.", countInterfaz);

		Long idInterfaz = eorInterfazCnrDao.nextVal();
		logger.info("idInterfaz: {}.", idInterfaz);

		EorInterfazCnrBean eorInterfazCnrBean = EorInterfazCnrBean.builder().idOrdInterfcnr(idInterfaz)
				.numeroServicio(NumeroServicio).numeroInspeccion(5L).numeroCuenta(vNumeroCuenta).tipoOrden(vTipoOrden)
				.motivo("motivo").subTipoOrden(vTipoOrden).build();

		// int rows = eorInterfazCnrDao.insert(eorInterfazCnrBean);
		// logger.info("rows: {}.", rows);

		/*
		 * FwkAuditeventBean fwkAuditeventBean = FwkAuditeventBean.builder()
		 * .usecase("usecase") .idFk(5L) .idUser(7L) .build(); int rows =
		 * fwkAuditeventDao.insert(fwkAuditeventBean); logger.info("rows: {}.", rows);
		 */

		/*
		 * int rows = medHisComponenteDao.updateFecHastaByIdComponete(46L);
		 * logger.info("rows: {}.", rows);
		 * 
		 * int rows = medHisComponenteDao.insertMedidorRetiro(idOrdInsp, idTarea,
		 * CodActComercial, idOrdTarea, idInterfaz); logger.info("rows: {}.", rows);
		 * 
		 * int rows = medHisComponenteDao.insertMedidorInstalacion(idOrdInsp, idTarea,
		 * idOrdTarea); logger.info("rows: {}.", rows);
		 */
		System.exit(0);
	}

	public void probarChris() {

		// // ILecturaXmlDao 1 cfdiaze
		// logger.info("lecturasXmlBean: {}.",
		// lecturaXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L, "111264",
		// "Encontrado"));
		// // IOrdObservacionDao
		// logger.info("ordObservacionDao.nextVal(): {}.", ordObservacionDao.nextVal());
		// logger.info("ordObservacionDao.insert: {}.",
		// ordObservacionDao.insert(OrdObservacionBean.builder()
		// .idObservacion(ordObservacionDao.nextVal())
		// .idOrden(1L)
		// .texto("Normalizacion desde eOrder")
		// .fechaObservacion(new java.util.Date())
		// .idUsuario(3353L)
		// .stateName("SFinalizada")
		// .discriminator("ObeservacionOrden")
		// .build()));

		// // IAnormalidadXmlDao
		// logger.info("findByIdOrdenInsp: {}.",
		// anormalidadXmlDao.findByIdOrdenInsp(1520216848432L));

		// // IComParametroDao
		// logger.info("comParametroDao.getCountBySistemaAndEntidadAndCodigoAndValorAlf:
		// {}.",
		// comParametroDao.getCountBySistemaAndEntidadAndCodigoAndValorAlf("EORDER",
		// "DOMINIOS", "RESULTADO", "REA"));

		// // IOperacionXmlDao
		// logger.info("findByIdOrdTransferAndNroEvento: {}.",
		// operacionXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L));

		// // IMedidorXmlDao
		// logger.info("findByIdOrdTransferAndNroEvento: {}.",
		// medidorXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L));

		// // IMedidorXmlDao
		// logger.info("findByIdOrdTransferAndNroEvento: {}.",
		// gpNormalizacionXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L));

		// // INucClienteDao
		// logger.info("getIdClienteByIdServicio: {}.",
		// nucClienteDao.getIdClienteByIdServicio(1969979L));

		// // IEorOrdTransferDao
		// logger.info("eorOrdTransferDao.findByNroOrdenLegacy: {}.",
		// eorOrdTransferDao.findByNroOrdenLegacy("2039370008"));
		// logger.info("eorOrdTransferDao.findByCodEstadoOrdenAndCodTipoOrdenEorder:
		// {}.", eorOrdTransferDao.findByCodEstadoOrdenAndCodTipoOrdenEorder(5L,
		// "GPN"));
		// logger.info("eorOrdTransferDao.updateTranferProcesoRecepcionaOrdenNormalizacion:
		// {}.",
		// eorOrdTransferDao.updateTranferProcesoRecepcionaOrdenNormalizacion(EorOrdTransferBean.builder()
		// .codOperacion("aaaa ch")
		// .nroOrdenEorder(1L)
		// .nroOrdenLegacy("bbbbb ch")
		// .codEstadoOrden(1L)
		// .observaciones("obsevandolos asda ch ddddyyyyy")
		// .codTipoOrdenLegacy("ccc ch")
		// .nroCuenta(1L)
		// .codEstadoOrden(1L)
		// .codTipoOrdenEorder("NCX.01")
		// .idOrdTransfer(20930071L)
		// .build()));

		// // IEorOrdTransferDetDao
		// logger.info("eorOrdTransferDetDao.getCodigoContratistaByIdOrdTransferAndNroEvento:
		// {}.",
		// eorOrdTransferDetDao.getCodigoContratistaByIdOrdTransferAndNroEvento(116704L,
		// 1L));
		// logger.info("eorOrdTransferDetDao.findByIdOrdTransferAndNroEvento: {}.",
		// eorOrdTransferDetDao.findByIdOrdTransferAndNroEvento(116704L, 1L));
		// logger.info("eorOrdTransferDetDao.update: {}.",
		// eorOrdTransferDetDao.update("Bleh aaa", 116704L, 1L));

		// INucEmpresaDao
		// logger.info("nucEmpresaDao.findByCodPartition: {}.",
		// nucEmpresaDao.findByCodPartition("EDEL"));

		// // IMedPropiedadDao
		// logger.info("nucEmpresaDao.findByCodPartition: {}.",
		// medPropiedadDao.findByCodPropiedadAndIdEmpresa("CLIE", 3L));

		// // IMedModeloDao
		// logger.info("medModeloDao.getIdModelByCodModelAndCodMarca: {}.",
		// medModeloDao.getIdModelByCodModelAndCodMarca("AAA01", "FAE"));

		// // IClienteDao
		// logger.info("medModeloDao.findByNroServicio: {}.",
		// clienteDao.findByNroServicio(1564127L));

		// // IClienteDao
		// logger.info("medidor4jDao.findByIdUbicacion: {}.",
		// medidor4jDao.findByIdUbicacion(10870111L));

		// DateFormat fomatoFecha = new SimpleDateFormat("yyyy-MM-dd hh24:mi:ss");

		// // IMedMagnitudDao
		// logger.info("aca 2");
		// logger.info("medMagnitudDao.nextVal: {}.", medMagnitudDao.nextVal());
		// logger.info("medMagnitudDao.insertMagnitudLectura: {}.",
		// medMagnitudDao.insertMagnitudLectura(
		// MedMagnitudBean
		// .builder()
		// .idMagnitud(medMagnitudDao.nextVal())
		// .idEmpresa(3L)
		// .idComponente(1L)
		// .idServicio(1L)
		// .idMedida(1L)
		// .enteros(1L)
		// .decimales(1L)
		// .factor(new BigDecimal(1))
		// .valor(new BigDecimal(1))
		// .tipo("LECTURA")
		// .idTipMagnitud(1L)
		// .observaciones("aaaaa")
		// .fecLecTerreno(new java.util.Date())
		// .secMagnitud(1L)
		// .idEstMagnitud(1L)
		// .idTipCalculo(1L)
		// .idTipoCons(1L)
		// .build()));

		// // IOrdOrdenDao 4 cfdiaze
		// logger.info("updateFechaFinalizacionById: {}.",
		// ordOrdenDao.updateFechaFinalizacionById(1520196808198L, "2021-03-21
		// 15:39:45"));
		// logger.info("findByIdOrdenAndDiscriminador: {}.",
		// ordOrdenDao.findByIdOrdenAndDiscriminador(1520196215402L, "INSPECCION"));
		// logger.info("findAaByNroOrden: {}.",
		// ordOrdenDao.findAaByNroOrden("77444283"));
		// logger.info("findAbByNroOrden: {}.", ordOrdenDao.findAbByNroOrden("1000016",
		// 3L));
		// // IOrdTipoOrdenDao 1 cfdiaze
		// logger.info("Cantidad: {}.",
		// ordTipoOrdenDao.getCountByCodInternoAndCodTipoOrden("OrdenNormalizacion",
		// "NORM"));
		// // SelloXmlDaoImpl 0 IGNORAR
		// // ISrvActComercialDao 1 cfdiaze
		// logger.info("SrvActComercial: {}.",
		// srvActComercialDao.findByCodActComercialAndIdEmpresa("Q0", 3L));
		// // ISrvElectricoDao 2 cfdiaze
		// logger.info("tareasXmlBean: {}.",
		// srvElectricoDao.updateSecMagnitudByIdServicio(86L, 5L));
		// logger.info("tareasXmlBean: {}.",
		// srvElectricoDao.getSecMagnitudByIdServicio(86L));
		// // ITareaXmlDao 1 cfdiaze
		// logger.info("tareasXmlBean: {}.",
		// tareaXmlDao.findByIdOrdTransferAndNroEvento(116704L, 1L));
		// // IUsuarioDao 1 cfdiaze
		// logger.info("Usuario: {}.", usuarioDao.findByUsername("MVENEGAS"));
		// // IWkfWorkflowDao 3 cfdiaze
		// logger.info("wkfWorkflowDao.updateIdStateByIdWorkflow: {}.",
		// wkfWorkflowDao.updateIdStateByIdWorkflow(21791289286081L, "Recibida ccc"));
		// logger.info("wkfWorkflowDao.updateIdStateByIdWorkflowAndIdState: {}.",
		// wkfWorkflowDao.updateIdStateByIdWorkflowAndIdState(21791289286081L, "Recibida
		// ccc", "Recibida"));
		// logger.info("wkfWorkflowDao.findById: {}.",
		// wkfWorkflowDao.findById(21791289286081L));

	}

	private boolean probarActualizar(Long idServicio) {
		try {
			srvElectricoDao.updateSecMagnitudByIdServicio(idServicio, 77L);
			int numero = 1 / 0;
			logger.info("Actualizado.");
		} catch (Exception e) {
			logger.error("Error al actualizar.");
			// return false;
			throw e;

		}
		return true;
	}

	public void probarActualizar2(Long idServicio, Long secMagnitud) {

		srvElectricoDao.updateSecMagnitudByIdServicio(idServicio, secMagnitud);
		int numero = 1 / 0;
		logger.info("Actualizado.");

	}
}
